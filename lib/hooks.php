<?php

// Stop register action if we find that the user is spammer via Stop Forum Spam
elgg_register_plugin_hook_handler('action', 'register', 'hj_approve_registration_gatekeeper', 100);

// Queue new users for manual approval
elgg_register_plugin_hook_handler('register', 'user', 'hj_approve_apply_user_approval_policies', 999);

// Override editing permissions
elgg_register_plugin_hook_handler('permissions_check', 'all', 'hj_approve_permissions_check', 999);
elgg_register_plugin_hook_handler('container_permissions_check', 'all', 'hj_approve_container_permissions_check', 999);

elgg_register_plugin_hook_handler('access:collections:read', 'user', 'hj_approve_access_collections_read');

// Apply exemption policies to messages
elgg_register_plugin_hook_handler('exempt', 'framework:approve', 'hj_approve_exempt_messages');

// Apply user page restrictions
if (elgg_is_logged_in()) {
	$user = elgg_get_logged_in_user_entity();
	if (isset($user->approval_status) && $user->approval_status != HYPEAPPROVE_STATUS_APPROVED) {
		$restrictions = elgg_get_config('policy_user_restrictions');
		$page_restrictions = elgg_extract('pages', $restrictions, array());
		$user_restrictions = elgg_extract($user->approval_status, $page_restrictions, array());
		foreach ($user_restrictions as $restr) {
			elgg_register_plugin_hook_handler('route', $restr, 'hj_approve_apply_user_page_restrictions', 1);
		}
		$action_restrictions = elgg_extract('actions', $restrictions, array());
		$user_restrictions = elgg_extract($user->approval_status, $action_restrictions, array());
		foreach ($user_restrictions as $restr) {
			elgg_register_plugin_hook_handler('action', $restr, 'hj_approve_apply_user_action_restrictions', 1);
		}
	}
}

function hj_approve_registration_gatekeeper($hook, $type, $return, $params) {

	if (!elgg_get_config('policy_user_spamcheck')) {
		return $return;
	}

	if (!HYPEAPPROVE_STOPFORUMSPAM_KEY) {
		if (!elgg_admin_notice_exists('hypeapprove:stopforumspamkey')) {
			elgg_add_admin_notice('hypeapprove:stopforumspamkey', elgg_echo('hj:approve:admin:spam:stopforumspam_key_notice'));
		}
		return $return;
	}

	elgg_load_library('approve:vendor:stopforumspam');

	elgg_make_sticky_form('register');

	$sfs = new StopForumSpam(HYPEAPPROVE_STOPFORUMSPAM_KEY);
	$is_spammer = $sfs->is_spammer(array(
		'email' => get_input('email'),
		'ip' => $_SERVER['REMOTE_ADDR']
			));
	if ($is_spammer) {
		register_error(elgg_echo('hj:approve:register:stopforumspam:spamemail'));
		return false;
	}
	$is_spammer = $sfs->is_spammer(array(
		'username' => get_input('username'),
		'ip' => $_SERVER['REMOTE_ADDR']
			));
	if ($is_spammer) {
		register_error(elgg_echo('hj:approve:register:stopforumspam:spamusername'));
		return false;
	}

	elgg_clear_sticky_form('register');

	return $return;
}

/**
 * Queue new users for approval
 */
function hj_approve_apply_user_approval_policies($hook, $type, $return, $params) {

	if (!elgg_get_config('policy_user_approval')) {
		return $return;
	}

	$user = elgg_extract('user', $params);

	if (!elgg_instanceof($user, 'user')) {
		return $return;
	}

	$ha = access_get_show_hidden_status();
	access_show_hidden_entities(true);

	hj_approve_set_approval_status($user, HYPEAPPROVE_STATUS_PENDING, HYPEAPPROVE_REASON_SITEPOLICY, HYPEAPPROVE_TRIGGER_SYSTEM);

	if (HYPEAPPROVE_NOTIFY_USER_QUEUE) {
		hj_approve_user_queue_notify($user);
	}

	access_show_hidden_entities($ha);

	return $return;
}

function hj_approve_permissions_check($hook, $type, $return, $params) {

	$entity = elgg_extract('entity', $params);
	$user = elgg_extract('user', $params);

	if (hj_approve_can_edit($user, $entity)) {
		return true;
	}

	return $return;
}

function hj_approve_container_permissions_check($hook, $type, $return, $params) {

	$container = elgg_extract('container', $params);
	$user = elgg_extract('user', $params);
	$subtype = elgg_extract('subtype', $params);

	// a little hack to fix the problem with blogs
	if ($type == 'all' && $subtype == 'all') {
		if (get_input('action') == 'blog/save') {
			$type = 'object';
			$subtype = 'blog';
		}
	}

	if (hj_approve_can_edit($user, null, $type, $subtype)
			|| hj_approve_can_edit($user, $container)) {
		return true;
	}

	return $return;
}

function hj_approve_apply_user_page_restrictions($hook, $type, $return, $params) {

	$title = elgg_echo('hj:approve:user:restrictions:page');

	$layout = elgg_view_layout('error', array(
		'content' => elgg_view_title($title)
			));

	echo elgg_view_page($title, $layout);

	return false;
}

function hj_approve_apply_user_action_restrictions($hook, $type, $return, $params) {

	register_error(elgg_echo('hj:approve:user:restrictions:action'));
	return false;
}

function hj_approve_access_collections_read($hook, $type, $return, $params) {

	// using this global variable to prevent this hook from calling itself
	global $POLICY_IGNORE_SQL_SUFFIX;

	if ($POLICY_IGNORE_SQL_SUFFIX)
		return $return;

	$user_guid = elgg_extract('user_id', $params);

	$POLICY_IGNORE_SQL_SUFFIX = true;
	$user = get_entity($user_guid);
	$POLICY_IGNORE_SQL_SUFFIX = false;

	if (!elgg_instanceof($user, 'user'))
		return $return;

	$is_admin = elgg_is_admin_user($user->guid);
	$is_editor = hj_approve_is_editor($user);
	$is_supervisor = hj_approve_is_supervisor($user);

	if ($is_admin) {
		$return[] = ACCESS_APPROVAL_WORKFLOW;
		$return[] = ACCESS_EDITORS;
		$return[] = ACCESS_SUPERVISORS;
	}

	if ($is_editor) {
		$return[] = ACCESS_APPROVAL_WORKFLOW;
		$return[] = ACCESS_EDITORS;
		$return[] = ACCESS_PRIVATE;
	}

	if ($is_supervisor) {
		$return[] = ACCESS_APPROVAL_WORKFLOW;
		$return[] = ACCESS_SUPERVISORS;
	}

	return $return;
}


/**
 * Apply exemptions to user-to-user messaging
 */
function hj_approve_exempt_messages($hook, $type, $return, $params) {

	$entity = elgg_extract('entity', $params);

	if (!elgg_instanceof($entity, 'object', 'messages')) {
		return $return;
	}

	if (!get_input('recipient_guid', false)) {
		// this is an internal site notification
		return true;
	}
	if ((int) $entity->owner_guid == (int) $entity->fromId) {
		// this is a copy of a message saved in the sent folder
		return true;
	}

	if ((int) $entity->fromId == (int) $entity->toId) {
		// this is a message sent to self
		return true;
	}

	$from = get_entity($entity->fromId);

	if (elgg_instanceof($from, 'site')) {
		// message is sent by the site
		return true;
	}

	if (elgg_instanceof($from, 'user')) {
		if (elgg_is_admin_user($from->guid)
				|| hj_approve_is_editor($from)
				|| hj_approve_is_supervisor($from)
				|| elgg_get_plugin_user_setting('exempt', $from->guid, 'hypeApprove')
		) {
			// sender is in a supervisory role, no need to queue
			return true;
		}
	}

	$to = get_entity($entity->toId);

	if (elgg_instanceof($to, 'site')) {
		// message is received by the site
		return true;
	}

	if (elgg_instanceof($to, 'user')) {
		if (elgg_is_admin_user($to->guid)
				|| hj_approve_is_editor($to)
				|| hj_approve_is_supervisor($to)
				//|| elgg_get_plugin_user_setting('exempt', $to->guid, 'hypeApprove')
		) {
			// recipient is in a supervisory role, no need to queue
			return true;
		}
	}

	return $return;
}