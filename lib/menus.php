<?php

if (elgg_is_admin_logged_in()) {

	elgg_register_menu_item('page', array(
		'name' => 'approve:policy',
		'text' => elgg_echo('hj:approve:admin:policy'),
		'href' => 'admin/approve/policy',
		'priority' => 100,
		'contexts' => array('admin'),
		'section' => 'approve'
	));

	elgg_register_menu_item('page', array(
		'name' => 'approve:restrictions',
		'text' => elgg_echo('hj:approve:admin:restrictions'),
		'href' => 'admin/approve/restrictions',
		'priority' => 100,
		'contexts' => array('admin'),
		'section' => 'approve'
	));

	elgg_register_menu_item('page', array(
		'name' => 'approve:notifications',
		'text' => elgg_echo('hj:approve:admin:notifications'),
		'href' => 'admin/approve/notifications',
		'priority' => 100,
		'contexts' => array('admin'),
		'section' => 'approve'
	));

	elgg_register_menu_item('page', array(
		'name' => 'approve:spam',
		'text' => elgg_echo('hj:approve:admin:spam'),
		'href' => 'admin/approve/spam',
		'priority' => 200,
		'contexts' => array('admin'),
		'section' => 'approve'
	));

	elgg_register_menu_item('page', array(
		'name' => 'approve:editors',
		'text' => elgg_echo('hj:approve:admin:editors'),
		'href' => 'admin/approve/editors',
		'priority' => 300,
		'contexts' => array('admin'),
		'section' => 'approve'
	));

	elgg_register_menu_item('page', array(
		'name' => 'approve:supervisors',
		'text' => elgg_echo('hj:approve:admin:supervisors'),
		'href' => 'admin/approve/supervisors',
		'priority' => 400,
		'contexts' => array('admin'),
		'section' => 'approve'
	));
}

elgg_register_plugin_hook_handler('register', 'menu:entity', 'hj_approve_entity_menu_setup');
elgg_register_plugin_hook_handler('register', 'menu:annotation', 'hj_approve_annotation_menu_setup');
elgg_register_plugin_hook_handler('register', 'menu:user_hover', 'hj_approve_user_hover_menu_setup');
elgg_register_plugin_hook_handler('register', 'menu:topbar', 'hj_approve_topbar_menu_setup');

function hj_approve_entity_menu_setup($hook, $type, $return, $params) {

	$entity = elgg_extract('entity', $params);

	if (!elgg_instanceof($entity)) {
		return $return;
	}

	if (isset($entity->approval_status) && $entity->approval_status != HYPEAPPROVE_STATUS_APPROVED && $entity->approval_status != HYPEAPPROVE_STATUS_FLAGGED && $entity->approval_status != HYPEAPPROVE_STATUS_EXEMPT
	) {

		$return['approval_status'] = ElggMenuItem::factory(array(
					'name' => 'approval_status',
					'text' => elgg_echo("hj:approve:status:$entity->approval_status"),
					'title' => elgg_echo('hj:approve:status'),
					'href' => '#'
		));
	}

	if (HYPEAPPROVE_SPAM_REPORTING && !isset($entity->approval_status) && $entity->owner_guid != elgg_get_logged_in_user_guid() && !elgg_instanceof($entity, 'user')) {
		elgg_load_js('approve.base.js');
		$return['report_spam'] = ElggMenuItem::factory(array(
					'name' => 'report_spam',
					'text' => elgg_echo('hj:approve:report:spam'),
					'href' => "action/approve/content/report?guid=$entity->guid",
					'is_action' => true
		));
	}

	if (elgg_instanceof($entity, 'user')) {
		if (hj_approve_is_editor($entity)) {
			$return['is_editor'] = ElggMenuItem::factory(array(
						'name' => 'is_editor',
						'text' => elgg_echo("hj:approve:editor"),
						'href' => false
			));
		} else if (hj_approve_is_superviser_of($entity, elgg_get_logged_in_user_entity())) {
			$return['is_supervisor'] = ElggMenuItem::factory(array(
						'name' => 'is_supervisor',
						'text' => elgg_echo("hj:approve:supervisor"),
						'href' => false
			));
		}
	}

	return $return;
}

function hj_approve_annotation_menu_setup($hook, $type, $return, $params) {

	$annotation = elgg_extract('annotation', $params);

	if (!$annotation instanceof ElggAnnotation) {
		return $return;
	}

	$entity = $annotation->getEntity();
	$owner = $annotation->getOwnerEntity();

	$approval_status = (isset($owner->annotation_approval_status)) ? unserialize($owner->annotation_approval_status) : array();
	$current_status = elgg_extract($annotation->id, $approval_status, null);

	if ($current_status && $current_status != HYPEAPPROVE_STATUS_APPROVED && $current_status != HYPEAPPROVE_STATUS_FLAGGED && $current_status != HYPEAPPROVE_STATUS_EXEMPT) {

		$return['approval_status'] = ElggMenuItem::factory(array(
					'name' => 'approval_status',
					'text' => elgg_echo("hj:approve:status:$current_status"),
					'title' => elgg_echo('hj:approve:status'),
					'href' => '#'
		));
	}

	if (HYPEAPPROVE_SPAM_REPORTING && !$current_status && $annotation->owner_guid != elgg_get_logged_in_user_guid()) {
		elgg_load_js('approve.base.js');
		$return['report_spam'] = ElggMenuItem::factory(array(
					'name' => 'report_spam',
					'text' => elgg_echo('hj:approve:report:spam'),
					'href' => "action/approve/content/report?annotation_id=$annotation->id",
					'is_action' => true
		));
	}

	return $return;
}

function hj_approve_user_hover_menu_setup($hook, $type, $return, $params) {

	$entity = elgg_extract('entity', $params);

	if (HYPEAPPROVE_SPAM_REPORTING && !isset($entity->approval_status) && $entity->owner_guid != elgg_get_logged_in_user_guid()) {
		elgg_load_js('approve.base.js');
		$return['report_spam'] = ElggMenuItem::factory(array(
					'name' => 'report_spam',
					'text' => elgg_echo('hj:approve:report:user'),
					'href' => "action/approve/content/report?guid=$entity->guid",
					'is_action' => true,
					'section' => 'actions'
		));
	}

	if (elgg_is_admin_logged_in()) {
		if (!hj_approve_is_editor($entity)) {
			$return['editor'] = ElggMenuItem::factory(array(
						'name' => 'editor',
						'text' => elgg_echo('hj:approve:make_editor'),
						'href' => "action/approve/admin/make_editor?guid=$entity->guid",
						'is_action' => true,
						'section' => 'admin'
			));
			$return['supervisor'] = ElggMenuItem::factory(array(
						'name' => 'supervisor',
						'text' => elgg_echo('hj:approve:supervisor:manage'),
						'href' => "approve/supervisor/$entity->guid",
						'section' => 'admin'
			));
			if (hj_approve_is_supervisor($entity)) {
				$return['supervisor_revoke'] = ElggMenuItem::factory(array(
						'name' => 'supervisor_revoke',
						'text' => elgg_echo('hj:approve:supervisor:revoke'),
						'href' => "action/approve/supervisor/revoke?guid=$entity->guid",
						'is_action' => true,
						'section' => 'admin'
			));
			}
		} else {
			$return['editor'] = ElggMenuItem::factory(array(
						'name' => 'editor',
						'text' => elgg_echo('hj:approve:revoke_editor'),
						'href' => "action/approve/admin/revoke_editor?guid=$entity->guid",
						'is_action' => true,
						'section' => 'admin'
			));
		}
	}

	return $return;
}

$user = elgg_get_logged_in_user_entity();
if ($user && (hj_approve_is_editor($user) || hj_approve_is_supervisor($user) || elgg_is_admin_user($user->guid)
		)) {
	elgg_register_menu_item('topbar', array(
		'name' => 'approve',
		'href' => 'approve/dashboard',
		'text' => elgg_view_icon('settings') . elgg_echo('hj:approve:topbar:approve'),
		'priority' => 200,
		'section' => 'alt',
	));
}