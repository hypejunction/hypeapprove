<?php

$policy_object_approval = elgg_get_plugin_setting('policy_object_approval', 'hypeApprove');
elgg_set_config('policy_object_approval', ($policy_object_approval) ? unserialize($policy_object_approval) : array());

$policy_object_spamcheck = elgg_get_plugin_setting('policy_object_spamcheck', 'hypeApprove');
elgg_set_config('policy_object_spamcheck', ($policy_object_spamcheck) ? unserialize($policy_object_spamcheck) : array());

$policy_user_approval = elgg_get_plugin_setting('policy_user_approval', 'hypeApprove');
elgg_set_config('policy_user_approval', ($policy_user_approval) ? $policy_user_approval : false);

$policy_user_spamcheck = elgg_get_plugin_setting('policy_user_spamcheck', 'hypeApprove');
elgg_set_config('policy_user_spamcheck', ($policy_user_spamcheck) ? $policy_user_spamcheck : false);

$policy_user_restrictions = elgg_get_plugin_setting('policy_user_restrictions', 'hypeApprove');
elgg_set_config('policy_user_restrictions', ($policy_user_restrictions) ? unserialize($policy_user_restrictions) : array());

$policy_group_approval = elgg_get_plugin_setting('policy_group_approval', 'hypeApprove');
elgg_set_config('policy_group_approval', ($policy_group_approval) ? $policy_group_approval : false);

$policy_group_spamcheck = elgg_get_plugin_setting('policy_group_spamcheck', 'hypeApprove');
elgg_set_config('policy_group_spamcheck', ($policy_group_spamcheck) ? $policy_group_spamcheck : false);

$policy_annotation_approval = elgg_get_plugin_setting('policy_annotation_approval', 'hypeApprove');
elgg_set_config('policy_annotation_approval', ($policy_annotation_approval) ? unserialize($policy_annotation_approval) : array());

$policy_annotation_spamcheck = elgg_get_plugin_setting('policy_annotation_spamcheck', 'hypeApprove');
elgg_set_config('policy_annotation_spamcheck', ($policy_annotation_spamcheck) ? unserialize($policy_annotation_spamcheck) : array());

$policy_object_editor_can_edit = elgg_get_plugin_setting('policy_object_editor_can_edit', 'hypeApprove');
elgg_set_config('policy_object_editor_can_edit', ($policy_object_editor_can_edit) ? unserialize($policy_object_editor_can_edit) : array());

$policy_object_editor_can_approve = elgg_get_plugin_setting('policy_object_editor_can_approve', 'hypeApprove');
elgg_set_config('policy_object_editor_can_approve', ($policy_object_editor_can_approve) ? unserialize($policy_object_editor_can_approve) : array());

$policy_user_editor_can_edit = elgg_get_plugin_setting('policy_user_editor_can_edit', 'hypeApprove');
elgg_set_config('policy_user_editor_can_edit', ($policy_user_editor_can_edit) ? $policy_user_editor_can_edit : false);

$policy_user_editor_can_approve = elgg_get_plugin_setting('policy_user_editor_can_approve', 'hypeApprove');
elgg_set_config('policy_user_editor_can_approve', ($policy_user_editor_can_approve) ? $policy_user_editor_can_approve : false);

$policy_group_editor_can_edit = elgg_get_plugin_setting('policy_group_editor_can_edit', 'hypeApprove');
elgg_set_config('policy_group_editor_can_edit', ($policy_group_editor_can_edit) ? $policy_group_editor_can_edit : false);

$policy_group_editor_can_approve = elgg_get_plugin_setting('policy_group_editor_can_approve', 'hypeApprove');
elgg_set_config('policy_group_editor_can_approve', ($policy_group_editor_can_approve) ? $policy_group_editor_can_approve : false);

$policy_annotation_editor_can_edit = elgg_get_plugin_setting('policy_annotation_editor_can_edit', 'hypeApprove');
elgg_set_config('policy_annotation_editor_can_edit', ($policy_annotation_editor_can_edit) ? unserialize($policy_annotation_editor_can_edit) : array());

$policy_annotation_editor_can_approve = elgg_get_plugin_setting('policy_annotation_editor_can_approve', 'hypeApprove');
elgg_set_config('policy_annotation_editor_can_approve', ($policy_annotation_editor_can_approve) ? unserialize($policy_annotation_editor_can_approve) : array());

$policy_object_supervisor_can_edit = elgg_get_plugin_setting('policy_object_supervisor_can_edit', 'hypeApprove');
elgg_set_config('policy_object_supervisor_can_edit', ($policy_object_supervisor_can_edit) ? unserialize($policy_object_supervisor_can_edit) : array());

$policy_object_supervisor_can_approve = elgg_get_plugin_setting('policy_object_supervisor_can_approve', 'hypeApprove');
elgg_set_config('policy_object_supervisor_can_approve', ($policy_object_supervisor_can_approve) ? unserialize($policy_object_supervisor_can_approve) : array());

$policy_user_supervisor_can_edit = elgg_get_plugin_setting('policy_user_supervisor_can_edit', 'hypeApprove');
elgg_set_config('policy_user_supervisor_can_edit', ($policy_user_supervisor_can_edit) ? $policy_user_supervisor_can_edit : false);

$policy_user_supervisor_can_approve = elgg_get_plugin_setting('policy_user_supervisor_can_approve', 'hypeApprove');
elgg_set_config('policy_user_supervisor_can_approve', ($policy_user_supervisor_can_approve) ? $policy_user_supervisor_can_approve : false);

$policy_group_supervisor_can_edit = elgg_get_plugin_setting('policy_group_supervisor_can_edit', 'hypeApprove');
elgg_set_config('policy_group_supervisor_can_edit', ($policy_group_supervisor_can_edit) ? $policy_group_supervisor_can_edit : false);

$policy_group_supervisor_can_approve = elgg_get_plugin_setting('policy_group_supervisor_can_approve', 'hypeApprove');
elgg_set_config('policy_group_supervisor_can_approve', ($policy_group_supervisor_can_approve) ? $policy_group_supervisor_can_approve : false);

$policy_annotation_supervisor_can_edit = elgg_get_plugin_setting('policy_annotation_supervisor_can_edit', 'hypeApprove');
elgg_set_config('policy_annotation_supervisor_can_edit', ($policy_annotation_supervisor_can_edit) ? unserialize($policy_annotation_supervisor_can_edit) : array());

$policy_annotation_supervisor_can_approve = elgg_get_plugin_setting('policy_annotation_supervisor_can_approve', 'hypeApprove');
elgg_set_config('policy_annotation_supervisor_can_approve', ($policy_annotation_supervisor_can_approve) ? unserialize($policy_annotation_supervisor_can_approve) : array());

elgg_set_config('policy_object_disable_owner_access', array(get_subtype_id('object', 'messages')));

$policy_messages = elgg_get_plugin_setting('policy_messages', 'hypeApprove');
elgg_set_config('policy_messages', ($policy_messages) ? unserialize($policy_messages) : array());

elgg_set_config('policy_annotations', array('generic_comment', 'messageboard', 'group_topic_post'));

$policy_notifications = elgg_get_plugin_setting('policy_notifications', 'hypeApprove');
if (!$policy_notifications) {
	$policy_notifications = array();
} else {
	$policy_notifications = unserialize($policy_notifications);
}
elgg_set_config('policy_notifications', $policy_notifications);

define('HYPEAPPROVE_NOTIFY_QUEUE', isset($policy_notifications['queue']));
define('HYPEAPPROVE_NOTIFY_USER_QUEUE', isset($policy_notifications['user_queue']));
define('HYPEAPPROVE_NOTIFY_USER_REPORT', isset($policy_notifications['user_report']));
define('HYPEAPPROVE_NOTIFY_AKISMET', isset($policy_notifications['akismet']));
define('HYPEAPPROVE_NOTIFY_WORKFLOW', isset($policy_notifications['workflow']));

/**
 * Check to see if the user is an editor
 * 
 * @param ElggUser $user
 * @return boolean
 */
function hj_approve_is_editor($user) {

	if (!elgg_instanceof($user, 'user')) {
		return false;
	}

	global $POLICY_ROLES_CACHE;

	if (!isset($POLICY_ROLES_CACHE['editors'])) {
		hj_approve_cache_editors();
	}

	return in_array($user->guid, $POLICY_ROLES_CACHE['editors']);
}

function hj_approve_cache_editors() {
	global $POLICY_ROLES_CACHE, $POLICY_IGNORE_SQL_SUFFIX;

	$POLICY_ROLES_CACHE['editors'] = array();

	$POLICY_IGNORE_SQL_SUFFIX = true;

	$site = elgg_get_site_entity();
	$editors = elgg_get_entities_from_relationship(array(
		'types' => 'user',
		'relationship' => 'editor',
		'relationship_guid' => $site->guid,
		'inverse_relationship' => true,
		'limit' => 0
	));

	$POLICY_IGNORE_SQL_SUFFIX = false;

	foreach ($editors as $editor) {
		if (!in_array($editor->guid, $POLICY_ROLES_CACHE['editors'])) {
			$POLICY_ROLES_CACHE['editors'][] = $editor->guid;
		}
	}

	return;
}

/**
 * Get site editors
 */
function hj_approve_get_editors() {
	global $POLICY_ROLES_CACHE, $POLICY_IGNORE_SQL_SUUFIX;

	if (!isset($POLICY_ROLES_CACHE['editors'])) {
		hj_approve_cache_editors();
	}
	$editor_guids = $POLICY_ROLES_CACHE['editors'];

	$POLICY_IGNORE_SQL_SUUFIX = true;
	foreach ($editor_guids as $guid) {
		$editors[] = get_entity($guid);
	}
	$POLICY_IGNORE_SQL_SUUFIX = false;
	return ($editors) ? $editors : false;
}

/**
 * Check to see if the user is a supervisor
 *
 * @param ElggUser $user
 * @return boolean
 */
function hj_approve_is_supervisor($user) {

	if (!elgg_instanceof($user, 'user')) {
		return false;
	}

	global $POLICY_ROLES_CACHE, $POLICY_IGNORE_SQL_SUFFIX;

	if (isset($POLICY_ROLES_CACHE['supervisors']) && in_array($user->guid, $POLICY_ROLES_CACHE['supervisors'])) {
		return true;
	} else {

		$POLICY_IGNORE_SQL_SUFFIX = true;

		$count = elgg_get_entities_from_relationship(array(
			'types' => array('user', 'group'),
			'relationship' => 'supervisor',
			'relationship_guid' => $user->guid,
			'inverse_relationship' => false,
			'count' => true
		));


		$POLICY_IGNORE_SQL_SUFFIX = false;

		if ($count) {
			$POLICY_ROLES_CACHE['supervisors'][] = $user->guid;
			return true;
		}
	}

	return false;
}

/**
 * Check to see if the user is a supervised user
 *
 * @param ElggUser $user
 * @return boolean
 */
function hj_approve_is_supervised_user($user) {

	if (!elgg_instanceof($user, 'user')) {
		return false;
	}

	if (elgg_is_admin_user($user->guid) || hj_approve_is_editor($user) || hj_approve_is_supervisor($user)) {
		return false;
	}

	global $POLICY_ROLES_CACHE;

	if (isset($POLICY_ROLES_CACHE['supervised_users']) && in_array($user->guid, $POLICY_ROLES_CACHE['supervised_users'])) {
		return true;
	} else {

		$count = elgg_get_entities_from_relationship(array(
			'types' => 'user',
			'relationship' => 'supervisor',
			'relationship_guid' => $user->guid,
			'inverse_relationship' => true,
			'count' => true
		));

		if ($count) {
			$POLICY_ROLES_CACHE['supervised_users'][] = $user->guid;
			return true;
		}

		$offset = 0;
		while ($user_groups = $user->getGroups(null, 1, $offset)) {
			$group = $user_groups[0];
			if (hj_approve_is_supervised_group($group)) {
				$POLICY_ROLES_CACHE['supervised_users'][] = $user->guid;
				return true;
			}
			$offset++;
		}
	}

	return false;
}

/**
 * Check to see if the group is a supervised group
 *
 * @param ElggGroup $user
 * @return boolean
 */
function hj_approve_is_supervised_group($group) {

	if (!elgg_instanceof($group, 'group')) {
		return false;
	}

	global $POLICY_ROLES_CACHE;

	if (isset($POLICY_ROLES_CACHE['supervised_groups']) && in_array($group->guid, $POLICY_ROLES_CACHE['supervised_groups'])) {
		return true;
	} else {

		$count = elgg_get_entities_from_relationship(array(
			'types' => 'group',
			'relationship' => 'supervisor',
			'relationship_guid' => $group->guid,
			'inverse_relationship' => true,
			'count' => true
		));

		if ($count) {
			$POLICY_ROLES_CACHE['supervised_groups'][] = $group->guid;
			return true;
		}
	}

	return false;
}

/**
 * Check to see if the supervisor is in charge of this user
 *
 * @param ElggUser $supervisor
 * @param ElggUser $user
 * @return boolean
 */
function hj_approve_is_superviser_of($supervisor, $user) {
	if (!elgg_instanceof($supervisor, 'user') || !elgg_instanceof($user, 'user')) {
		return false;
	}

	if (elgg_is_admin_user($user->guid) || hj_approve_is_editor($user) || hj_approve_is_supervisor($user)) {
		return false;
	}

	if ((check_entity_relationship($supervisor->guid, 'supervisor', $user->guid))) {
		return true;
	}
	$groups = hj_approve_get_supervised_groups($supervisor);
	if ($groups) {
		foreach ($groups as $group) {
			if ($group->isMember($user))
				return true;
		}
	}
	return false;
}

/**
 * Get supervisors
 */
function hj_approve_get_supervisors($user = null) {

	global $POLICY_ROLES_CACHE;

	$supervisors = elgg_get_entities_from_relationship(array(
		'types' => array('user', 'group'),
		'relationship' => 'supervisor',
		'relationship_guid' => (elgg_instanceof($user, 'user')) ? $user->guid : null,
		'inverse_relationship' => true,
		'limit' => 9999
	));

	$return = array();
	if ($supervisors) {
		if (!isset($POLICY_ROLES_CACHE['supervisors'])) {
			$POLICY_ROLES_CACHE['supervisors'] = array();
		}
		foreach ($supervisors as $supervisor) {
			if (!elgg_instanceof($supervisor, 'user')) {
				continue;
			}
			if (elgg_is_admin_user($supervisor->guid) || hj_approve_is_editor($supervisor)) {
				continue;
			}
			if (!array_key_exists($supervisor->guid, $return)) {
				$return[$supervisor->guid] = $supervisor;
			}
			if (!in_array($supervisor->guid, $POLICY_ROLES_CACHE['supervisors'])) {
				$POLICY_ROLES_CACHE['supervisors'][] = $supervisor->guid;
			}
		}
	}

	return (sizeof($return)) ? $return : false;
}

/**
 * Get supervised users
 */
function hj_approve_get_supervised_users($supervisor = null) {

	global $POLICY_ROLES_CACHE;

	$supervised = elgg_get_entities_from_relationship(array(
		'types' => 'user',
		'relationship' => 'supervisor',
		'relationship_guid' => (elgg_instanceof($supervisor, 'user')) ? $supervisor->guid : null,
		'inverse_relationship' => false,
		'limit' => 9999
	));

	$groups = hj_approve_get_supervised_groups($supervisor);
	if ($groups) {
		if (!$supervised) {
			$supervised = array();
		}
		foreach ($groups as $group) {
			$members = $group->getMembers(9999);
			if ($members) {
				$supervised = array_merge($supervised, $members);
			}
		}
	}

	$return = array();
	if ($supervised) {
		if (!isset($POLICY_ROLES_CACHE['supervised_users'])) {
			$POLICY_ROLES_CACHE['supervised_users'] = array();
		}
		foreach ($supervised as $sup) {
			if (!elgg_instanceof($sup, 'user')) {
				continue;
			}
			if (elgg_is_admin_user($sup->guid) || hj_approve_is_editor($sup) || hj_approve_is_supervisor($sup)) {
				continue;
			}
			if (!array_key_exists($sup->guid, $return)) {
				$return[$sup->guid] = $sup;
			}
			if (!in_array($sup->guid, $POLICY_ROLES_CACHE['supervised_users'])) {
				$POLICY_ROLES_CACHE['supervised_users'][] = $sup->guid;
			}
		}
	}

	return (sizeof($return)) ? $return : false;
}

/**
 * Get supervised groups
 */
function hj_approve_get_supervised_groups($supervisor = null) {

	global $POLICY_ROLES_CACHE;

	$supervised = elgg_get_entities_from_relationship(array(
		'types' => 'group',
		'relationship' => 'supervisor',
		'relationship_guid' => (elgg_instanceof($supervisor, 'user')) ? $supervisor->guid : null,
		'inverse_relationship' => false,
		'limit' => 9999
	));

	$return = array();
	if ($supervised) {
		if (!isset($POLICY_ROLES_CACHE['supervised_groups'])) {
			$POLICY_ROLES_CACHE['supervised_groups'] = array();
		}
		foreach ($supervised as $sup) {
			if (!array_key_exists($sup->guid, $return)) {
				$return[$sup->guid] = $sup;
			}
			if (!in_array($sup->guid, $POLICY_ROLES_CACHE['supervised_groups'])) {
				$POLICY_ROLES_CACHE['supervised_groups'][] = $sup->guid;
			}
		}
	}
	return (sizeof($return)) ? $return : false;
}

/**
 * Check to see if the user can edit an entity of type/subtype
 * 
 * @param ElggUser $user			User to perform an operation
 * @param ElggEntity $entity		Entity an operation to be performed on
 * @param string $entity_type		Entity type
 * @param string $entity_subtype	Entity subtype
 * @param ElggUser $owner			Owner of the entity	
 */
function hj_approve_can_edit($user = null, $entity = null, $entity_type = null, $entity_subtype = null, $owner = null) {

	if (!$user) {
		$user = elgg_get_logged_in_user_entity();
		if (!$user) {
			return false;
		}
	}

	$is_admin = elgg_is_admin_user($user->guid);
	$is_editor = hj_approve_is_editor($user);
	$is_supervisor = hj_approve_is_supervisor($user);

	if ($entity) {
		$entity_type = $entity->getType();
		$entity_subtype = $entity->getSubtype();
		$owner = $entity->getOwnerEntity();
	}


	if (!$is_admin && !$is_editor && !$is_supervisor) {
		return false;
	}

	if ($is_admin) {
		return true;
	}

	switch ($entity_type) {

		default :
			return false;
			break;

		case 'object' :
			$subtype_id = get_subtype_id($entity_type, $entity_subtype);

			if (!$subtype_id) {
				return false;
			}

			if ($is_editor) {
				return in_array($subtype_id, elgg_get_config('policy_object_editor_can_edit'));
			} else if ($is_supervisor) {
				if (elgg_instanceof($owner) && hj_approve_is_superviser_of($user, $owner)) {
					return in_array($subtype_id, elgg_get_config('policy_object_supervisor_can_edit'));
				} else if (!elgg_instanceof($owner)) {
					return in_array($subtype_id, elgg_get_config('policy_object_supervisor_can_edit'));
				} else {
					return false;
				}
			}
			break;


		case 'user' :

			if (elgg_instanceof($entity, 'user')) {
				// Only other admins can edit admin users
				if (elgg_is_admin_user($entity->guid) && !$is_admin) {
					return false;
				}

				// Only admins can edit editors
				if (hj_approve_is_editor($entity) && !$is_admin) {
					return false;
				}

				// Only admins and editors can edit supervisors
				if (hj_approve_is_supervisor($entity) && !$is_admin && !$is_editor) {
					return false;
				}
			}

			if ($is_editor) {
				return elgg_get_config('policy_user_editor_can_edit');
			} else if ($is_supervisor) {
				if (elgg_instanceof($entity) && hj_approve_is_superviser_of($user, $entity)) {
					return elgg_get_config('policy_user_supervisor_can_edit');
				} else if (!elgg_instanceof($entity)) {
					return elgg_get_config('policy_user_supervisor_can_edit');
				} else {
					return false;
				}
			}
			break;


		case 'group' :
			if ($is_editor) {
				return elgg_get_config('policy_group_editor_can_edit');
			} else if ($is_supervisor) {
				if (elgg_instanceof($owner) && hj_approve_is_superviser_of($user, $owner)) {
					return elgg_get_config('policy_group_supervisor_can_edit');
				} else if (!elgg_instanceof($owner)) {
					return elgg_get_config('policy_user_supervisor_can_edit');
				} else {
					return false;
				}
			}
			break;

		case 'object' :
			if ($is_editor) {
				return in_array($entity->name, elgg_get_config('policy_annotation_editor_can_edit'));
			} else if ($is_supervisor) {
				if (elgg_instanceof($owner) && hj_approve_is_superviser_of($user, $owner)) {
					return in_array($entity->name, elgg_get_config('policy_annotation_supervisor_can_edit'));
				} else if (!elgg_instanceof($owner)) {
					return in_array($entity->name, elgg_get_config('policy_annotation_supervisor_can_edit'));
				} else {
					return false;
				}
			}
			break;
	}
}

/**
 * Check to see if the user can approve an entity of type/subtype
 *
 * @param ElggUser $user			User to perform an operation
 * @param ElggEntity $entity		Entity an operation to be performed on
 * @param string $entity_type		Entity type
 * @param string $entity_subtype	Entity subtype
 * @param ElggUser $owner			Owner of the entity
 */
function hj_approve_can_approve($user = null, $entity = null, $entity_type = null, $entity_subtype = null, $owner = null) {

	if (!$user) {
		$user = elgg_get_logged_in_user_entity();
		if (!$user) {
			return false;
		}
	}

	$is_admin = elgg_is_admin_user($user->guid);
	$is_editor = hj_approve_is_editor($user);
	$is_supervisor = hj_approve_is_supervisor($user);

	if ($entity) {
		$entity_type = $entity->getType();
		$entity_subtype = $entity->getSubtype();
		$owner = $entity->getOwnerEntity();
	}

	if (!$is_admin && !$is_editor && !$is_supervisor) {
		return false;
	}

	if ($is_admin) {
		return true;
	}

	switch ($entity_type) {

		default :
			return false;
			break;

		case 'object' :
			$subtype_id = get_subtype_id($entity_type, $entity_subtype);

			if (!$subtype_id) {
				return false;
			}

			if ($is_editor) {
				return in_array($subtype_id, elgg_get_config('policy_object_editor_can_approve'));
			} else if ($is_supervisor) {
				if (elgg_instanceof($owner) && hj_approve_is_superviser_of($user, $owner)) {
					return in_array($subtype_id, elgg_get_config('policy_object_supervisor_can_approve'));
				} else if (!elgg_instanceof($owner)) {
					return in_array($subtype_id, elgg_get_config('policy_object_supervisor_can_approve'));
				} else {
					return false;
				}
			}
			break;


		case 'user' :
			if ($is_editor) {
				return elgg_get_config('policy_user_editor_can_approve');
			} else if ($is_supervisor) {
				if (hj_approve_is_superviser_of($user, $entity)) {
					return elgg_get_config('policy_user_supervisor_can_approve');
				} else {
					return false;
				}
			}
			break;


		case 'group' :
			if ($is_editor) {
				return elgg_get_config('policy_group_editor_can_approve');
			} else if ($is_supervisor) {
				if (elgg_instanceof($owner) && hj_approve_is_superviser_of($user, $owner)) {
					return elgg_get_config('policy_group_supervisor_can_approve');
				} else if (!elgg_instanceof($owner)) {
					return elgg_get_config('policy_user_supervisor_can_approve');
				} else {
					return false;
				}
			}
			break;

		case 'annotation' :
			if ($is_editor) {
				return in_array($entity->name, elgg_get_config('policy_annotation_editor_can_approve'));
			} else if ($is_supervisor) {
				if (elgg_instanceof($owner) && hj_approve_is_superviser_of($user, $owner)) {
					return in_array($entity->name, elgg_get_config('policy_annotation_supervisor_can_approve'));
				} else if (!elgg_instanceof($owner)) {
					return in_array($entity->name, elgg_get_config('policy_annotation_supervisor_can_approve'));
				} else {
					return false;
				}
			}
			break;
	}
}

/**
 * Check if entity and its owner are exempt from approval queue
 *
 * @param ElggEntity $entity
 */
function hj_approve_queue_is_exempt($entity) {

	$exempt = false;

	if ($entity->approval_status == HYPEAPPROVE_STATUS_APPROVED) {
		$exempt = true;
	}

	if (elgg_instanceof($entity, 'user')) {
		$owner = $entity;
	} else {
		$owner = $entity->getOwnerEntity();
		$container = $entity->getContainerEntity();
	}

	if (!$exempt && elgg_instanceof($owner, 'user')) {
		if (elgg_is_admin_user($owner->guid) || hj_approve_is_editor($owner) || hj_approve_is_supervisor($owner) || elgg_get_plugin_user_setting('exempt', $owner->guid, 'hypeApprove')
		) {
			$exempt = true;
		}
	}

	if (!$exempt && elgg_instanceof($container, 'user')) {
		if (elgg_is_admin_user($container->guid) || hj_approve_is_editor($container) || hj_approve_is_supervisor($container) || elgg_get_plugin_user_setting('exempt', $container->guid, 'hypeApprove')
		) {
			$exempt = true;
		}
	}

	return elgg_trigger_plugin_hook('exempt', 'framework:approve', array('entity' => $entity), $exempt);
}

/**
 * Notify admins/editors about an item queued for approval
 *
 * @param ElggEntity $entity
 */
function hj_approve_queue_notify(ElggEntity $entity) {

	$policy_notifications = elgg_get_config('policy_notifications');
	$subtype_id = get_subtype_id($entity->getType(), $entity->getSubtype());
	$owner = $entity->getOwnerEntity();

	if ($policy_notifications['queue']['admin']) {
		$admins = elgg_get_admins();
		foreach ($admins as $admin) {
			$to[] = $admin->guid;
		}
	}

	if ($policy_notifications['queue']['editor']) {
		if (in_array($subtype_id, elgg_get_config('policy_object_editor_can_edit')) || in_array($subtype_id, elgg_get_config('policy_object_editor_can_approve'))) {
			$editors = hj_approve_get_editors();
			if ($editors) {
				foreach ($editors as $editor) {
					$to[] = $editor->guid;
				}
				$to = array_unique($to);
			}
		}
	}

	if ($policy_notifications['queue']['supervisor']) {
		if (in_array($subtype_id, elgg_get_config('policy_object_supervisor_can_edit')) || in_array($subtype_id, elgg_get_config('policy_object_supervisor_can_approve'))) {
			$supervisors = hj_approve_get_supervisors($owner);
			if ($supervisors) {
				foreach ($supervisors as $supervisor) {
					$to[] = $supervisor->guid;
				}
			}
			$to = array_unique($to);
		}
	}

	$site = elgg_get_site_entity();
	$from = $site->guid;

	$subject = elgg_echo("hj:approve:pendingapproval:email:subject");
	$body = elgg_view('framework/approve/notifications/pendingapproval', array(
		'entity' => $entity
	));

	return notify_user($to, $from, $subject, $body);
}

/**
 * Notify admins/editors about new user registrations queued for approval
 *
 * @param ElggUser $user
 */
function hj_approve_user_queue_notify(ElggUser $user) {

	$policy_notifications = elgg_get_config('policy_notifications');

	if ($policy_notifications['user_queue']['admin']) {
		$admins = elgg_get_admins();
		foreach ($admins as $admin) {
			$to[] = $admin->guid;
		}
	}

	if ($policy_notifications['user_queue']['editor']) {
		if (elgg_get_config('policy_user_editor_can_edit') || elgg_get_config('policy_user_editor_can_approve')) {
			$editors = hj_approve_get_editors();
			if ($editors) {
				foreach ($editors as $editor) {
					$to[] = $editor->guid;
				}
				$to = array_unique($to);
			}
		}
	}

	$site = elgg_get_site_entity();
	$from = $site->guid;

	$subject = elgg_echo("hj:approve:userpendingapproval:email:subject");
	$body = elgg_view('framework/approve/notifications/userpendingapproval', array(
		'entity' => $user
	));

	return notify_user($to, $from, $subject, $body);
}

/**
 * Notify admins/editors about an annotation queued for approval
 *
 * @param ElggAnnotation $entity
 */
function hj_approve_annotation_queue_notify(ElggAnnotation $annotation) {

	$policy_notifications = elgg_get_config('policy_notifications');
	$owner = $annotation->getOwnerEntity();

	if ($policy_notifications['queue']['admin']) {
		$admins = elgg_get_admins();
		foreach ($admins as $admin) {
			$to[] = $admin->guid;
		}
	}

	if ($policy_notifications['queue']['editor']) {
		if (in_array($annotation->name, elgg_get_config('policy_annotation_editor_can_edit')) || in_array($annotation->name, elgg_get_config('policy_annotation_editor_can_approve'))) {
			$editors = hj_approve_get_editors();
			if ($editors) {
				foreach ($editors as $editor) {
					$to[] = $editor->guid;
				}
				$to = array_unique($to);
			}
		}
	}

	if ($policy_notifications['queue']['supervisor']) {
		if (in_array($annotation->name, elgg_get_config('policy_annotation_supervisor_can_edit')) || in_array($annotation->name, elgg_get_config('policy_annotation_supervisor_can_approve'))) {
			$supervisors = hj_approve_get_supervisors($owner);
			if ($supervisors) {
				foreach ($supervisors as $supervisor) {
					$to[] = $supervisor->guid;
				}
			}
			$to = array_unique($to);
		}
	}

	$site = elgg_get_site_entity();
	$from = $site->guid;

	$subject = elgg_echo("hj:approve:pendingapproval:email:subject");
	$body = elgg_view('framework/approve/notifications/annotation_pendingapproval', array(
		'annotation' => $annotation
	));

	return notify_user($to, $from, $subject, $body);
}

/**
 * Prepare Akismet class to submit a report
 *
 * @param ElggUser $user		Content owner
 * @param ElggEntity $entity	Entity being checked against spam
 * @return boolean|\Akismet
 */
function hj_approve_spamcheck_prepare_akismet(ElggUser $user, ElggEntity $entity) {

	if (!HYPEAPPROVE_AKISMET_KEY || !elgg_instanceof($user, 'user') || !elgg_instanceof($entity, 'object') || !($entity instanceof ElggAnnotation)) {
		return false;
	}

	elgg_load_library('approve:vendor:akismet');

	$akismet = new Akismet(elgg_get_site_url(), HYPEAPPROVE_AKISMET_KEY);

	$akismet->setCommentAuthor($user->username);
	$akismet->setCommentAuthorEmail($user->email);

	if (!$user_url = $user->website) {
		if (!$user_url = $user->www) {
			$user_url = $user->getURL();
		}
	}
	$akismet->setCommentAuthorURL($user_url);

	$metadata = elgg_trigger_plugin_hook('spamcheck:content', 'framework:approve', array('entity' => $entity), array('title', 'description', 'value'));

	if (is_array($metadata)) {
		foreach ($metadata as $meta) {
			if (!empty($entity->$meta)) {
				$content_meta[] = $entity->$meta;
			}
		}
	}

	$content = implode(', ', $content_meta);

	$akismet->setCommentContent($content);
	$akismet->setPermalink($entity->getURL());

	return $akismet;
}

/**
 * Check if entity and its owner are exempt from spamcheck
 * 
 * @param ElggEntity $entity
 */
function hj_approve_spamcheck_is_exempt($entity) {

	$exempt = false;

	if ($entity->approval_status == HYPEAPPROVE_STATUS_APPROVED) {
		$exempt = true;
	}

	if (elgg_instanceof($entity, 'user')) {
		$owner = $entity;
	} else {
		$owner = $entity->getOwnerEntity();
		$container = $entity->getContainerEntity();
	}

	if (!$exempt && elgg_instanceof($owner, 'user')) {
		if (elgg_is_admin_user($owner->guid) || hj_approve_is_editor($owner) || hj_approve_is_supervisor($owner) || elgg_get_plugin_user_setting('whitelist', $owner->guid, 'hypeApprove')
		) {
			$exempt = true;
		}
	}

	if (!$exempt && elgg_instanceof($container, 'user')) {
		if (elgg_is_admin_user($container->guid) || hj_approve_is_editor($container) || hj_approve_is_supervisor($container) || elgg_get_plugin_user_setting('exempt', $container->guid, 'hypeApprove')
		) {
			$exempt = true;
		}
	}

	return elgg_trigger_plugin_hook('whitelist', 'framework:approve', array('entity' => $entity), $exempt);
}

/**
 * Notify admins/editors about an item flagged as spam
 *
 * @param ElggEntity $entity
 * @param string $spamreport_source
 */
function hj_approve_spamcheck_notify(ElggEntity $entity, $spamreport_source = 'default') {

	$subtype_id = get_subtype_id($entity->getType(), $entity->getSubtype());
	$policy_notifications = elgg_get_config('policy_notifications');
	$owner = $entity->getOwnerEntity();

	if ($policy_notifications[$spamreport_source]['admin']) {
		$admins = elgg_get_admins();
		foreach ($admins as $admin) {
			$to[] = $admin->guid;
		}
	}

	if ($policy_notifications[$spamreport_source]['editor']) {
		if (in_array($subtype_id, elgg_get_config('policy_object_editor_can_edit')) || in_array($subtype_id, elgg_get_config('policy_object_editor_can_approve'))) {
			$editors = hj_approve_get_editors();
			if ($editors) {
				foreach ($editors as $editor) {
					$to[] = $editor->guid;
				}
				$to = array_unique($to);
			}
		}
	}

	if ($policy_notifications[$spamreport_source]['supervisor']) {
		if (in_array($subtype_id, elgg_get_config('policy_object_supervisor_can_edit')) || in_array($subtype_id, elgg_get_config('policy_object_supervisor_can_approve'))) {
			$supervisors = hj_approve_get_supervisors($owner);
			if ($supervisors) {
				foreach ($supervisors as $supervisor) {
					$to[] = $supervisor->guid;
				}
			}
			$to = array_unique($to);
		}
	}

	$site = elgg_get_site_entity();
	$from = $site->guid;

	$subject = elgg_echo("hj:approve:suspectedspam:email:subject", array(elgg_echo("hj:approve:suspectedspam:source:$spamreport_source")));
	$body = elgg_view('framework/approve/notifications/suspectedspam', array(
		'entity' => $entity,
		'spamreport_source' => $spamreport_source
	));

	return notify_user($to, $from, $subject, $body);
}

/**
 * Notify admins/editors about an annotation flagged as spam
 *
 * @param ElggAnnotation $annotation
 * @param string $spamreport_source
 */
function hj_approve_annotation_spamcheck_notify(ElggAnnotation $annotation, $spamreport_source = 'default') {

	$annotation_name = $annotation->name;
	$policy_notifications = elgg_get_config('policy_notifications');
	$owner = $annotation->getOwnerEntity();

	if ($policy_notifications[$spamreport_source]['admin']) {
		$admins = elgg_get_admins();
		foreach ($admins as $admin) {
			$to[] = $admin->guid;
		}
	}

	if ($policy_notifications[$spamreport_source]['editor']) {
		if (in_array($annotation_name, elgg_get_config('policy_annotation_editor_can_edit')) || in_array($subtype_id, elgg_get_config('policy_annotation_editor_can_approve'))) {
			$editors = hj_approve_get_editors();
			if ($editors) {
				foreach ($editors as $editor) {
					$to[] = $editor->guid;
				}
				$to = array_unique($to);
			}
		}
	}

	if ($policy_notifications[$spamreport_source]['supervisor']) {
		if (in_array($annotation_name, elgg_get_config('policy_annotation_supervisor_can_edit')) || in_array($subtype_id, elgg_get_config('policy_annotation_supervisor_can_approve'))) {
			$supervisors = hj_approve_get_supervisors($owner);
			if ($supervisors) {
				foreach ($supervisors as $supervisor) {
					$to[] = $supervisor->guid;
				}
			}
			$to = array_unique($to);
		}
	}

	$site = elgg_get_site_entity();
	$from = $site->guid;

	$subject = elgg_echo("hj:approve:suspectedspam:email:subject", array(elgg_echo("hj:approve:suspectedspam:source:$spamreport_source")));
	$body = elgg_view('framework/approve/notifications/annotation_suspectedspam', array(
		'annotation' => $annotation,
		'spamreport_source' => $spamreport_source
	));

	return notify_user($to, $from, $subject, $body);
}

/**
 * Set an approval status
 * Create an annotation to log history
 *
 * @param ElggEntity $entity	Entity in the approval workflow
 * @param str $status			Status to be set
 * @param str $reason			Reason for which the entity was subjected to approval workflow
 * @param str $trigger			What triggered the approval workflow
 * @param str $msg				Additional note
 */
function hj_approve_set_approval_status($entity, $status, $reason = null, $trigger = null, $msg = null) {

	$current_status = $entity->approval_status;
	$current_reason = $entity->approval_reason;
	$current_trigger = $entity->approval_trigger;

	if (!elgg_instanceof($entity)) {
		return false;
	}

	if ($status == HYPEAPPROVE_STATUS_FLAGGED && in_array($current_status, array(HYPEAPPROVE_STATUS_REJECTED, HYPEAPPROVE_STATUS_PENDING, HYPEAPPROVE_STATUS_APPROVED))) {
		return false;
	}


	if ($entity->canEdit()) {
		$setter = elgg_get_logged_in_user_entity();
	}

	if (!elgg_instanceof($setter, 'user') || !elgg_is_admin_user($setter->guid) || !hj_approve_is_editor($setter) || !hj_approve_is_supervisor($setter)
	) {
		$setter = elgg_get_site_entity();
	}

	$setter_guid = $setter->guid;

	if ($status != $current_status) {
		$entity->approval_status = $status;
		create_metadata($entity->guid, 'approval_status', $status, '', $entity->owner_guid, ACCESS_PUBLIC, false);
		create_annotation($entity->guid, 'approval_status', $status, '', $setter_guid, ACCESS_PUBLIC);

		// enable entities after approving them
		if ($status == HYPEAPPROVE_STATUS_APPROVED && !$entity->isEnabled()) {
			$entity->enable();
			create_annotation($entity->guid, 'disable', false, '', $setter_guid, ACCESS_PUBLIC);
		}

		if ($status == HYPEAPPROVE_STATUS_APPROVED && isset($entity->original_access_id) && !elgg_instanceof($entity, 'user')) {
			$ia = elgg_set_ignore_access(true);

			$entity->access_id = $entity->original_access_id;
			$entity->original_access_id = null;
			$entity->setURL(null);
			$entity->save();
			elgg_set_ignore_access($ia);
		}

		if (($status == HYPEAPPROVE_STATUS_REJECTED || $status == HYPEAPPROVE_STATUS_PENDING) && $entity->access_id != ACCESS_APPROVAL_WORKFLOW && !elgg_instanceof($entity, 'user')) {
			$ia = elgg_set_ignore_access(true);
			if (!isset($entity->original_access_id)) {
				$entity->original_access_id = $entity->access_id; // create_metadata doesnt update cache
				create_metadata($entity->guid, 'original_access_id', $entity->access_id, '', $entity->owner_guid, ACCESS_PUBLIC, false);

				// Override the URL so that notifications are sent out pointing to the /approve page handler
				$type = $entity->getType();
				$subtype = $entity->getSubtype();
				$guid = $entity->getGUID();
				$title = ($entity->title) ? $entity->title : $entity->name;
				$friendly_title = elgg_get_friendly_title($title);
				$url = elgg_normalize_url("approve/$status/$type/$subtype/$guid/$friendly_title");
				$entity->setURL($url);
			}
			$entity->access_id = ACCESS_APPROVAL_WORKFLOW;
			$entity->save();

			elgg_set_ignore_access($ia);
		}

		if ($current_status == HYPEAPPROVE_STATUS_PENDING && $status == HYPEAPPROVE_STATUS_APPROVED && $current_reason == HYPEAPPROVE_REASON_SPAMCHECK && $current_trigger == HYPEAPPROVE_TRIGGER_AKISMET) {
			// spam report not confirmed
			// report ham back to akismet

			$akismet = hj_approve_spamcheck_prepare_akismet($entity->getOwnerEntity(), $entity);
			if ($akismet instanceof Akismet) {
				$akismet->submitHam();
			}
		}

		if (HYPEAPPROVE_NOTIFY_WORKFLOW && $status != HYPEAPPROVE_STATUS_EXEMPT && $status != HYPEAPPROVE_STATUS_FLAGGED) {

			$policy_notifications = elgg_get_config('policy_notifications');
			$subtype_id = get_subtype_id($entity->getType(), $entity->getSubtype());
			$owner = $entity->getOwnerEntity();

			if ($policy_notifications['workflow']['admin']) {
				$admins = elgg_get_admins();
				foreach ($admins as $admin) {
					$to[] = $admin->guid;
				}
			}

			if ($policy_notifications['workflow']['editor']) {
				if (in_array($subtype_id, elgg_get_config('policy_object_editor_can_edit')) || in_array($subtype_id, elgg_get_config('policy_object_editor_can_approve'))) {
					$editors = hj_approve_get_editors();
					if ($editors) {
						foreach ($editors as $editor) {
							$to[] = $editor->guid;
						}
						$to = array_unique($to);
					}
				}
			}

			if ($policy_notifications['workflow']['supervisor']) {
				if (in_array($subtype_id, elgg_get_config('policy_object_supervisor_can_edit')) || in_array($subtype_id, elgg_get_config('policy_object_supervisor_can_approve'))) {
					$supervisors = hj_approve_get_supervisors($owner);
					if ($supervisors) {
						foreach ($supervisors as $supervisor) {
							$to[] = $supervisor->guid;
						}
					}
					$to = array_unique($to);
				}
			}

			$subject = elgg_echo("hj:approve:contentapprovalstatus:email:subject");
			$body = elgg_view('framework/approve/notifications/contentapprovalstatus', array(
				'entity' => $entity,
				'previous_status' => $current_status,
				'status' => $status,
				'setter' => $setter,
				'note' => $msg
			));

			if (($key = array_search($setter->guid, $to)) !== false) {
				unset($to[$key]);
			}
			notify_user($to, $setter->guid, $subject, $body);
		}
	}

	if ($reason && $reason != $current_reason) {
		$entity->approval_reason = $reason;
		create_metadata($entity->guid, 'approval_reason', $reason, '', $entity->owner_guid, ACCESS_PUBLIC, false);
		create_annotation($entity->guid, 'approval_reason', $reason, '', $setter_guid, ACCESS_PUBLIC);
	}

	if ($trigger && $trigger != $current_trigger) {
		$entity->approval_trigger = $trigger;
		create_metadata($entity->guid, 'approval_trigger', $trigger, '', $entity->owner_guid, ACCESS_PUBLIC, false);
		create_annotation($entity->guid, 'approval_trigger', $trigger, '', $setter_guid, ACCESS_PUBLIC);
	}

	if ($msg) {
		create_annotation($entity->guid, 'approval_message', $msg, '', elgg_get_logged_in_user_guid(), ACCESS_PUBLIC);
	}

	return true;
}

/**
 * Set an annotation approval status
 * Create an annotation to log history
 *
 * @param ElggAnnotation $annotation Annotation in the approval workflow
 * @param str $status			Status to be set
 * @param str $reason			Reason for which the annotation was subjected to approval workflow
 * @param str $trigger			What triggered the approval workflow
 * @param str $msg				Additional note
 */
function hj_approve_set_annotation_approval_status($annotation, $status, $reason = null, $trigger = null, $msg = null) {

	$entity = $annotation->getEntity();
	$owner = $annotation->getOwnerEntity();

	$approval_status = (isset($owner->annotation_approval_status)) ? unserialize($owner->annotation_approval_status) : array();
	$current_status = elgg_extract($annotation->id, $approval_status, null);

	$approval_reason = (isset($owner->annotation_approval_reason)) ? unserialize($owner->annotation_approval_reason) : array();
	$current_reason = elgg_extract($annotation->id, $approval_reason, null);

	$approval_trigger = (isset($owner->annotation_approval_trigger)) ? unserialize($owner->annotation_approval_trigger) : array();
	$current_trigger = elgg_extract($annotation->id, $approval_trigger, null);

	$original_access = (isset($owner->annotation_original_access)) ? unserialize($owner->annotation_original_access) : array();
	$original_access_id = elgg_extract($annotation->id, $original_access, false);


	if ($status == HYPEAPPROVE_STATUS_FLAGGED && in_array($current_status, array(HYPEAPPROVE_STATUS_REJECTED, HYPEAPPROVE_STATUS_PENDING, HYPEAPPROVE_STATUS_APPROVED))) {
		return false;
	}

	$setter = elgg_get_logged_in_user_entity();

	if (!elgg_instanceof($setter, 'user') || !elgg_is_admin_user($setter->guid) || !hj_approve_is_editor($setter) || !hj_approve_is_supervisor($setter)
	) {
		$setter = elgg_get_site_entity();
	}

	if ($status != $current_status) {
		$approval_status[$annotation->id] = $status;
		$owner->annotation_approval_status = serialize($approval_status);
		create_metadata($owner->guid, 'annotation_approval_status', serialize($approval_status), '', $owner->guid, ACCESS_PUBLIC, false);

		// enable annotations after approving them
		if ($status == HYPEAPPROVE_STATUS_APPROVED && $annotation->enabled == 'no') {
			$annotation->enable();
		}

		if ($status == HYPEAPPROVE_STATUS_APPROVED) {

			$ia = elgg_set_ignore_access(true);

			if ($original_access_id !== false) {
				$annotation->access_id = $original_access_id;
				unset($original_access[$annotation->id]);
				$owner->annotation_original_access = serialize($original_access);
				$annotation->save();
				$river = elgg_get_river(array(
					'annotation_ids' => $annotation->id,
					'limit' => 0
				));
				$dbprefix = elgg_get_config('dbprefix');
				foreach ($river as $r) {
					update_data("UPDATE {$dbprefix}river
									set access_id='{$original_access_id}'
										WHERE annotation_id={$annotation->id}");
				}
			}

			unset($approval_status[$annotation->id]);
			if (count($approval_status)) {
				$owner->annotation_approval_status = serialize($approval_status);
				create_metadata($owner->guid, 'annotation_approval_status', serialize($approval_status), '', $owner->guid, ACCESS_PUBLIC, false);
			} else {
				$owner->annotation_approval_status = null;
				elgg_delete_metadata(array(
					'entity_guid' => $owner->guid,
					'metadata_name' => 'annotation_approval_status'
				));
			}
			elgg_set_ignore_access($ia);
		}

		if (($status == HYPEAPPROVE_STATUS_REJECTED || $status == HYPEAPPROVE_STATUS_PENDING) && $annotation->access_id != ACCESS_APPROVAL_WORKFLOW) {
			$ia = elgg_set_ignore_access(true);
			if ($original_access_id === false) {
				$original_access[$annotation->id] = $annotation->access_id;
				$owner->annotation_original_access = serialize($original_access); // create_metadata doesnt update cache
				create_metadata($owner->guid, 'annotation_original_access', serialize($original_access), '', $owner->guid, ACCESS_PUBLIC, false);
			}
			$annotation->access_id = ACCESS_APPROVAL_WORKFLOW;
			$annotation->save();
			elgg_set_ignore_access($ia);
		}

		if ($current_status == HYPEAPPROVE_STATUS_PENDING && $status == HYPEAPPROVE_STATUS_APPROVED && $current_reason == HYPEAPPROVE_REASON_SPAMCHECK && $current_trigger == HYPEAPPROVE_TRIGGER_AKISMET) {
			// spam report not confirmed
			// report ham back to akismet

			$akismet = hj_approve_spamcheck_prepare_akismet($owner, $annotation);
			if ($akismet instanceof Akismet) {
				$akismet->submitHam();
			}
		}

		if (HYPEAPPROVE_NOTIFY_WORKFLOW && $status != HYPEAPPROVE_STATUS_EXEMPT && $status != HYPEAPPROVE_STATUS_FLAGGED) {

			$policy_notifications = elgg_get_config('policy_notifications');
			$annotation_name = $annotation->name;

			if ($policy_notifications['workflow']['admin']) {
				$admins = elgg_get_admins();
				foreach ($admins as $admin) {
					$to[] = $admin->guid;
				}
			}

			if ($policy_notifications['workflow']['editor']) {
				if (in_array($annotation_name, elgg_get_config('policy_annotation_editor_can_edit')) || in_array($annotation_name, elgg_get_config('policy_annotation_editor_can_approve'))) {
					$editors = hj_approve_get_editors();
					if ($editors) {
						foreach ($editors as $editor) {
							$to[] = $editor->guid;
						}
						$to = array_unique($to);
					}
				}
			}

			if ($policy_notifications['workflow']['supervisor']) {
				if (in_array($annotation_name, elgg_get_config('policy_annotation_supervisor_can_edit')) || in_array($annotation_name, elgg_get_config('policy_annotation_supervisor_can_approve'))) {
					$supervisors = hj_approve_get_supervisors($owner);
					if ($supervisors) {
						foreach ($supervisors as $supervisor) {
							$to[] = $supervisor->guid;
						}
					}
					$to = array_unique($to);
				}
			}

			$subject = elgg_echo("hj:approve:contentapprovalstatus:email:subject");
			$body = elgg_view('framework/approve/notifications/annotationapprovalstatus', array(
				'annotation' => $annotation,
				'previous_status' => $current_status,
				'status' => $status,
				'setter' => $setter,
				'note' => $msg
			));

			if (($key = array_search($setter->guid, $to)) !== false) {
				unset($to[$key]);
			}
			notify_user($to, $setter->guid, $subject, $body);
		}
	}

	if ($reason && $reason != $current_reason) {
		$approval_reason[$annotation->id] = $reason;
		$owner->annotation_approval_reason = serialize($approval_reason); // create_metadata doesnt update cache
		create_metadata($owner->guid, 'annotation_approval_reason', serialize($approval_reason), '', $owner->guid, ACCESS_PUBLIC, false);
	}

	if ($trigger && $trigger != $current_trigger) {
		$approval_trigger[$annotation->id] = $trigger;
		$owner->annotation_approval_trigger = serialize($approval_trigger); // create_metadata doesnt update cache
		create_metadata($entity->guid, 'annotation_approval_trigger', serialize($approval_trigger), '', $owner->guid, ACCESS_PUBLIC, false);
	}

	/** @todo: figure out how to store 'msg' */
	return true;
}

/**
 * Apply search criteria to getter options
 *
 * @param array $options Current options
 * @return array Update options array
 */
function hj_approve_apply_user_search_values($options) {

	$search_values = get_input('search_values', array());

	$dbprefix = elgg_get_config('dbprefix');

	$types = elgg_extract('types', $search_values, 'user');
	$options['types'] = $types;

	$keyword = elgg_extract('keyword', $search_values, false);
	if ($keyword) {
		$keyword = sanitize_string($keyword);
	}
	switch ($types) {

		case 'user' :
		default :
			$options['joins'][] = "JOIN {$dbprefix}users_entity ue ON
											ue.guid = e.guid";
			if ($keyword) {
				$options['wheres'][] = "ue.username LIKE '%$keyword%' OR ue.name LIKE '%$keyword%' OR ue.email LIKE '%$keyword%'";
			}

			$validated = elgg_extract('validated', $search_values, false);
			if ($validated) {
				if ($validated == 'no') {
					$options['metadata_name_value_pairs'][] = array('name' => 'validated', 'value' => false);
				} else if ($validated == 'yes') {
					$options['metadata_name_value_pairs'][] = array('name' => 'validated', 'value' => true);
				}
			}

			break;

		case 'object' :
			$options['joins'][] = "JOIN {$dbprefix}objects_entity oe ON
											oe.guid = e.guid";
			if ($keyword) {
				$options['wheres'][] = "oe.title LIKE '%$keyword%' OR oe.description LIKE '%$keyword%'";
			}

			$subtype_ids = elgg_extract('subtypes', $search_values);
			if (!$subtype_ids) {
				$registered = elgg_get_config('registered_entities');
				foreach ($registered as $type => $subtypes) {
					if ($type == 'object') {
						foreach ($subtypes as $subtype) {
							$id = get_subtype_id($type, $subtype);
							if ($id) {
								$subtype_ids[] = $id;
							}
						}
					}
				}
			}
			if ($subtype_ids) {
				$subtype_ids = implode(',', $subtype_ids);
				$options['wheres'][] = "e.subtype IN ($subtype_ids)";
			}
			break;

		case 'group' :
			$options['joins'][] = "JOIN {$dbprefix}groups_entity ge ON
											ge.guid = e.guid";
			if ($keyword) {
				$options['wheres'][] = "ge.name LIKE '%$keyword%' OR ge.description LIKE '%$keyword%'";
			}
			break;
	}

	$status = elgg_extract('status', $search_values);

	if ($status && !is_array($status)) {
		$status = array(HYPEAPPROVE_STATUS_PENDING, HYPEAPPROVE_STATUS_APPROVED, HYPEAPPROVE_STATUS_FLAGGED, HYPEAPPROVE_STATUS_REJECTED);
	}

	if ($status) {
		foreach ($status as $s) {
			$status_in[] = "'$s'";
		}
		$status_in = implode(',', $status_in);
		$options['metadata_name_value_pairs'][] = array('name' => 'approval_status', 'value' => $status_in, 'operand' => 'IN');
	}

	$reason = elgg_extract('reason', $search_values);

	if ($reason && !is_array($reason)) {
		$reason = array(HYPEAPPROVE_REASON_SITEPOLICY, HYPEAPPROVE_REASON_SPAMCHECK);
	}

	if ($reason) {
		foreach ($reason as $r) {
			$reason_in[] = "'$r'";
		}
		$reason_in = implode(',', $reason_in);
		$options['metadata_name_value_pairs'][] = array('name' => 'approval_reason', 'value' => $reason_in, 'operand' => 'IN');
	}


	$trigger = elgg_extract('trigger', $search_values);

	if ($trigger && !is_array($trigger)) {
		$trigger = array(HYPEAPPROVE_TRIGGER_ADMIN, HYPEAPPROVE_TRIGGER_AKISMET, HYPEAPPROVE_TRIGGER_SYSTEM, HYPEAPPROVE_TRIGGER_USERREPORT);
	}

	if ($trigger) {
		foreach ($trigger as $t) {
			$trigger_in[] = "'$t'";
		}
		$trigger_in = implode(',', $trigger_in);
		$options['metadata_name_value_pairs'][] = array('name' => 'approval_trigger', 'value' => $trigger_in, 'operand' => 'IN');
	}

	$enabled = elgg_extract('enabled', $search_values);

	if ($enabled && !is_array($enabled)) {
		$enabled = array('yes', 'no');
	}

	if ($enabled) {
		foreach ($enabled as $e) {
			$enabled_in[] = "'$e'";
		}
		$enabled_in = implode(',', $enabled_in);
		$options['wheres'][] = "e.enabled IN ($enabled_in)";
	}

	return $options;
}