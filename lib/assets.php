<?php

elgg_register_simplecache_view('css/framework/approve/base');
elgg_register_css('approve.base.css', elgg_get_simplecache_url('css', 'framework/approve/base'));


elgg_register_simplecache_view('js/framework/approve/base');
elgg_register_js('approve.base.js', elgg_get_simplecache_url('js', 'framework/approve/base'));

elgg_register_simplecache_view('js/framework/approve/dashboard');
elgg_register_js('approve.dashboard.js', elgg_get_simplecache_url('js', 'framework/approve/dashboard'));

elgg_register_simplecache_view('js/framework/approve/admin');
elgg_register_js('approve.admin.js', elgg_get_simplecache_url('js', 'framework/approve/admin'));