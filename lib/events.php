<?php

elgg_register_event_handler('create', 'object', 'hj_approve_apply_entity_spamcheck_policies', 900);
elgg_register_event_handler('create', 'group', 'hj_approve_apply_entity_spamcheck_policies', 900);
elgg_register_event_handler('create', 'annotation', 'hj_approve_apply_annotation_spamcheck_policies', 900);

elgg_register_event_handler('create', 'object', 'hj_approve_apply_entity_approval_policies', 999);
elgg_register_event_handler('update', 'object', 'hj_approve_apply_entity_approval_policies', 999);

elgg_register_event_handler('create', 'group', 'hj_approve_apply_entity_approval_policies', 999);
elgg_register_event_handler('update', 'group', 'hj_approve_apply_entity_approval_policies', 999);

elgg_register_event_handler('create', 'annotation', 'hj_approve_apply_annotation_approval_policies', 999);
elgg_register_event_handler('update', 'annotation', 'hj_approve_apply_annotation_approval_policies', 999);

elgg_register_event_handler('login', 'user', 'hj_approve_on_login');

elgg_register_event_handler('create', 'supervisor', 'hj_approve_supervisor_relationship_exceptions');
elgg_register_event_handler('create', 'editor', 'hj_approve_editor_relationship_create', 999);

// Apply user event restrictions
if (elgg_is_logged_in()) {
	$user = elgg_get_logged_in_user_entity();
	if (isset($user->approval_status) && $user->approval_status != HYPEAPPROVE_STATUS_APPROVED) {
		$restrictions = elgg_get_config('policy_user_restrictions');
		$event_restrictions = elgg_extract('events', $restrictions, array());
		$user_restrictions = elgg_extract($user->approval_status, $event_restrictions, array());
		foreach ($user_restrictions as $restr) {
			list($event, $type) = explode('::', $restr);
			elgg_register_event_handler($event, $type, 'hj_approve_apply_user_event_restrictions', 1);
		}
	}
}

/**
 * Check new objects and groups for spam
 */
function hj_approve_apply_entity_spamcheck_policies($event, $entity_type, $entity) {

	if (elgg_instanceof($entity, 'object')) {
		$subtype_id = get_subtype_id($entity->getType(), $entity->getSubtype());

		if (!in_array($subtype_id, elgg_get_config('policy_object_spamcheck'))) {
			return true;
		}
	} else if (elgg_instanceof($entity, 'group')) {
		if (!elgg_get_config('policy_group_spamcheck')) {
			return true;
		}
	} else {
		return true;
	}

	$user = $entity->getOwnerEntity();

	if (!HYPEAPPROVE_AKISMET_KEY) {
		if (!elgg_admin_notice_exists('hypeapprove:akismetkey')) {
			elgg_add_admin_notice('hypeapprove:akismetkey', elgg_echo('hj:approve:admin:spam:akismet_key_notice'));
		}
		return true;
	}

	// User is exempt from spam checks
	if (hj_approve_spamcheck_is_exempt($entity)) {
		if (!$entity->approval_status) {
			hj_approve_set_approval_status($entity, HYPEAPPROVE_STATUS_EXEMPT, HYPEAPPROVE_REASON_SPAMCHECK, HYPEAPPROVE_TRIGGER_AKISMET);
		}
		return true;
	}

	elgg_load_library('approve:vendor:akismet');

	$akismet = hj_approve_spamcheck_prepare_akismet($user, $entity);

	if ($akismet instanceof Akismet) {
		if ($akismet->isCommentSpam()) {

			if ($entity->access_id != ACCESS_APPROVAL_WORKFLOW) {
				if (!isset($entity->original_access_id)) {
					$entity->original_access_id = $entity->access_id; // create_metadata doesnt update cache
					create_metadata($entity->guid, 'original_access_id', $entity->access_id, '', $entity->owner_guid, ACCESS_PUBLIC, false);
				}
				$entity->access_id = ACCESS_APPROVAL_WORKFLOW;
			}

			hj_approve_set_approval_status($entity, HYPEAPPROVE_STATUS_PENDING, HYPEAPPROVE_REASON_SPAMCHECK, HYPEAPPROVE_TRIGGER_AKISMET);

			// Override the URL so that notifications are sent out pointing to the /approve page handler
			$type = $entity->getType();
			$subtype = $entity->getSubtype();
			$guid = $entity->getGUID();
			$title = ($entity->title) ? $entity->title : $entity->name;
			$friendly_title = elgg_get_friendly_title($title);
			$status = HYPEAPPROVE_STATUS_PENDING;
			$url = elgg_normalize_url("approve/$status/$type/$subtype/$guid/$friendly_title");
			$entity->setURL($url);

			system_message(elgg_echo('hj:approve:markedasspam'));

			if (HYPEAPPROVE_NOTIFY_AKISMET) {
				hj_approve_spamcheck_notify($entity, HYPEAPPROVE_TRIGGER_AKISMET);
			}

			$fail_count = elgg_get_plugin_user_setting('akismet_fail_count', $user->guid, 'hypeApprove');
			$fail_count++;
			elgg_set_plugin_user_setting('akismet_fail_count', $fail_count, $user->guid, 'hypeApprove');
			$fail_count_policy = elgg_get_plugin_setting('akismet_fail_count', 'hypeApprove');
			if ($fail_count_policy && $fail_count >= $fail_count_policy) {
				$ia = elgg_set_ignore_access(true);
				switch (HYPEAPPROVE_AKISMET_FAIL_COUNT_ACTION) {
					default:
					case 'ban' :
						$user->ban('akismet_fail_count');
						break;

					case 'disable' :
						$user->disable('akismet_fail_count');
						break;

					case 'delete' :
						$user->delete(true);
						break;
				}
				elgg_set_ignore_access($ia);
			}
		} else {
			hj_approve_set_approval_status($entity, HYPEAPPROVE_STATUS_APPROVED, HYPEAPPROVE_REASON_SPAMCHECK, HYPEAPPROVE_TRIGGER_AKISMET);
		}
	}

	return true;
}


/**
 * Check new objects and groups for spam
 */
function hj_approve_apply_annotation_spamcheck_policies($event, $entity_type, $annotation) {

	$annotation_name = $annotation->name;
	if (!in_array($annotation_name, elgg_get_config('policy_annotation_spamcheck'))) {
			return true;
	}

	$entity = get_entity($annotation->entity_guid);
	$user = get_entity($annotation->owner_guid);

	if (!HYPEAPPROVE_AKISMET_KEY) {
		if (!elgg_admin_notice_exists('hypeapprove:akismetkey')) {
			elgg_add_admin_notice('hypeapprove:akismetkey', elgg_echo('hj:approve:admin:spam:akismet_key_notice'));
		}
		return true;
	}

	// User is exempt from spam checks
	if (hj_approve_spamcheck_is_exempt($user)) {
		$approval_status = (isset($user->annotation_approval_status)) ? unserialize($user->annotation_approval_status) : array();
		if (!array_key_exists($annotation->id, $approval_status)) {
			hj_approve_set_annotation_approval_status($annotation, HYPEAPPROVE_STATUS_EXEMPT, HYPEAPPROVE_REASON_SPAMCHECK, HYPEAPPROVE_TRIGGER_AKISMET);
		}
		return true;
	}

	elgg_load_library('approve:vendor:akismet');

	$akismet = hj_approve_spamcheck_prepare_akismet($user, $annotation);

	if ($akismet instanceof Akismet) {
		if ($akismet->isCommentSpam()) {

			$original_access = (isset($user->annotation_original_access)) ? unserialize($user->annotation_original_access) : array();
			$original_access_id = elgg_extract($annotation->id, $original_access, false);

			if ($annotation->access_id != ACCESS_APPROVAL_WORKFLOW) {
				if ($original_access_id !== false) {
					$original_access[$annotation->id] = $annotation->access_id;
					$user->annotation_original_access = serialize($original_access); // create_metadata doesnt update cache
					create_metadata($user->guid, 'annotation_original_access', serialize($original_access), '', $user->guid, ACCESS_PUBLIC, false);
				}
				$annotation->access_id = ACCESS_APPROVAL_WORKFLOW;
			}

			hj_approve_set_annotation_approval_status($annotation, HYPEAPPROVE_STATUS_PENDING, HYPEAPPROVE_REASON_SPAMCHECK, HYPEAPPROVE_TRIGGER_AKISMET);

			system_message(elgg_echo('hj:approve:markedasspam'));

			if (HYPEAPPROVE_NOTIFY_AKISMET) {
				hj_approve_annotation_spamcheck_notify($entity, HYPEAPPROVE_TRIGGER_AKISMET);
			}

			$fail_count = elgg_get_plugin_user_setting('akismet_fail_count', $user->guid, 'hypeApprove');
			$fail_count++;
			elgg_set_plugin_user_setting('akismet_fail_count', $fail_count, $user->guid, 'hypeApprove');
			$fail_count_policy = elgg_get_plugin_setting('akismet_fail_count', 'hypeApprove');
			if ($fail_count_policy && $fail_count >= $fail_count_policy) {
				$ia = elgg_set_ignore_access(true);
				switch (HYPEAPPROVE_AKISMET_FAIL_COUNT_ACTION) {
					default:
					case 'ban' :
						$user->ban('akismet_fail_count');
						break;

					case 'disable' :
						$user->disable('akismet_fail_count');
						break;

					case 'delete' :
						$user->delete(true);
						break;
				}
				elgg_set_ignore_access($ia);
			}
		} else {
			hj_approve_set_annotation_approval_status($annotation, HYPEAPPROVE_STATUS_APPROVED, HYPEAPPROVE_REASON_SPAMCHECK, HYPEAPPROVE_TRIGGER_AKISMET);
		}
	}

	return true;
}

/**
 * Queue objects for approval
 */
function hj_approve_apply_entity_approval_policies($event, $entity_type, $entity) {

	if (elgg_instanceof($entity, 'object')) {
		$subtype_id = get_subtype_id($entity->getType(), $entity->getSubtype());

		if (!in_array($subtype_id, elgg_get_config('policy_object_approval'))) {
			return true;
		}
	} else if (elgg_instanceof($entity, 'group')) {
		if (!elgg_get_config('policy_group_approval')) {
			return true;
		}
	} else {
		return true;
	}


	// User is exempt from approval workflow
	if (hj_approve_queue_is_exempt($entity)) {
		if (!$entity->approval_status) {
			hj_approve_set_approval_status($entity, HYPEAPPROVE_STATUS_EXEMPT, HYPEAPPROVE_REASON_SITEPOLICY, HYPEAPPROVE_TRIGGER_SYSTEM);
		}
		return true;
	}

	if (!$entity->approval_status) {
		if (hj_approve_set_approval_status($entity, HYPEAPPROVE_STATUS_PENDING, HYPEAPPROVE_REASON_SITEPOLICY, HYPEAPPROVE_TRIGGER_SYSTEM)) {
			system_message(elgg_echo('hj:approve:pendingapproval'));
		}

		if (HYPEAPPROVE_NOTIFY_QUEUE) {
			hj_approve_queue_notify($entity);
		}
	} else {
		if ($entity->access_id != ACCESS_APPROVAL_WORKFLOW) {
			switch ($entity->approval_status) {

				default :
					return true;
					break;

				case HYPEAPPROVE_STATUS_PENDING :
				case HYPEAPPROVE_STATUS_REJECTED :


					// Override the URL so that notifications are sent out pointing to the /approve page handler
					$type = $entity->getType();
					$subtype = $entity->getSubtype();
					$title = ($entity->title) ? $entity->title : $entity->name;
					$friendly_title = elgg_get_friendly_title($title);
					$status = $entity->approval_status;
					$url = elgg_normalize_url("approve/$status/$type/$subtype/$entity->guid/$friendly_title");
					$entity->setURL($url);

					if ($entity->access_id !== ACCESS_APPROVAL_WORKFLOW) {

						$entity->original_access_id = $entity->access_id; // create_metadata doesnt update cache
						create_metadata($entity->guid, 'original_access_id', $entity->access_id, '', $entity->owner_guid, ACCESS_PUBLIC, false);

						$guid = $entity->getGUID();

						system_message(elgg_echo('hj:approve:access_id_private'));

						$entity->access_id = ACCESS_APPROVAL_WORKFLOW;

						$dbprefix = elgg_get_config('dbprefix');
						$access_id = ACCESS_APPROVAL_WORKFLOW;

						// @hack Since 'update','object' event handler does not let us update the access_id
						// we shall do it the hardcore way
						update_data("UPDATE {$dbprefix}entities
									set access_id='{$access_id}'
										WHERE guid={$guid}");

						update_river_access_by_object($guid, $access_id);

						// If memcache is available then delete this entry from the cache
						static $newentity_cache;
						if ((!$newentity_cache) && (is_memcache_available())) {
							$newentity_cache = new ElggMemcache('new_entity_cache');
						}
						if ($newentity_cache) {
							$newentity_cache->delete($guid);
						}
					}

					// disable entities so that they can't be seen by owners
					if (in_array($entity->subtype, elgg_get_config('policy_object_disable_owner_access'))
							&& $entity->isEnabled()) {
						create_metadata($entity->guid, 'original_access_id', ACCESS_PRIVATE, '', $entity->owner_guid, ACCESS_PUBLIC, false);
						$entity->disable('disable_owner_access');
					}

					return true;
					break;
			}
		}
	}
	return true;
}


/**
 * Queue annotations for approval
 */
function hj_approve_apply_annotation_approval_policies($event, $entity_type, $annotation) {

	$annotation_name = $annotation->name;
	if (!in_array($annotation_name, elgg_get_config('policy_annotation_approval'))) {
			return true;
	}

	$entity = $annotation->getEntity();
	$user = $annotation->getOwnerEntity();

	// User is exempt from approval workflow
	if (hj_approve_queue_is_exempt($user)) {
		$approval_status = (isset($user->annotation_approval_status)) ? unserialize($user->annotation_approval_status) : array();
		if (!array_key_exists($annotation->id, $approval_status)) {
			hj_approve_set_annotation_approval_status($annotation, HYPEAPPROVE_STATUS_EXEMPT, HYPEAPPROVE_REASON_SPAMCHECK, HYPEAPPROVE_TRIGGER_AKISMET);
		}
		return true;
	}

	$approval_status = (isset($user->annotation_approval_status)) ? unserialize($user->annotation_approval_status) : array();
	$current_status = elgg_extract($annotation->id, $approval_status, null);

	$original_access = (isset($user->annotation_original_access)) ? unserialize($user->annotation_original_access) : array();

	if (!$current_status) {
		if (hj_approve_set_annotation_approval_status($annotation, HYPEAPPROVE_STATUS_PENDING, HYPEAPPROVE_REASON_SITEPOLICY, HYPEAPPROVE_TRIGGER_SYSTEM)) {
			system_message(elgg_echo('hj:approve:pendingapproval'));
		}

		if (HYPEAPPROVE_NOTIFY_QUEUE) {
			hj_approve_annotation_queue_notify($entity);
		}
	} else {
		if ($annotation->access_id != ACCESS_APPROVAL_WORKFLOW) {
			switch ($approval_status) {

				default :
					return true;
					break;

				case HYPEAPPROVE_STATUS_PENDING :
				case HYPEAPPROVE_STATUS_REJECTED :

					if ($annotation->access_id !== ACCESS_APPROVAL_WORKFLOW) {

						$original_access[$annotation->id] = $annotation->access_id;
						$user->annotation_original_access = serialize($original_access); // create_metadata doesnt update cache
						create_metadata($user->guid, 'annotation_original_access', serialize($original_access), '', $user->guid, ACCESS_PUBLIC, false);

						$id = $annotation->id;

						system_message(elgg_echo('hj:approve:access_id_private'));

						$annotation->access_id = ACCESS_APPROVAL_WORKFLOW;

						$dbprefix = elgg_get_config('dbprefix');
						$access_id = ACCESS_APPROVAL_WORKFLOW;
						$access_private = ACCESS_PRIVATE;

						update_data("UPDATE {$dbprefix}annotations
									set access_id='{$access_id}'
										WHERE id={$id}");

						$river = elgg_get_river(array(
							'annotation_ids' => $id,
							'limit' => 0
						));

						foreach ($river as $r) {
							update_data("UPDATE {$dbprefix}river
									set access_id='{$access_private}'
										WHERE annotation_id={$id}");
						}
					}

					return true;
					break;
			}
		}
	}
	return true;
}

/**
 * Perform actions on logged in users
 */
function hj_approve_on_login($event, $type, $user) {
	if ($user) {
		elgg_set_plugin_user_setting('last_ip', $_SERVER['REMOTE_ADDR'], $user->guid, 'hypeApprove');
		if ($user->approval_status == HYPEAPPROVE_STATUS_PENDING) {
			system_message(elgg_echo('hj:approve:userpendingapproval'));
		}
		if ($user->approval_status == HYPEAPPROVE_STATUS_REJECTED) {
			register_error(elgg_echo('hj:approve:userrejected'));
		}
		if ($user->approval_status == HYPEAPPROVE_STATUS_FLAGGED
				&& $user->approval_trigger != HYPEAPPROVE_TRIGGER_USERREPORT) {
			register_error(elgg_echo('hj:approve:userflagged'));
		}
	}
	return true;
}

/**
 * Apply event restrictions
 */
function hj_approve_apply_user_event_restrictions($event, $type, $param) {

	register_error(elgg_echo('hj:approve:user:restrictions:event'));
	return false;
}

/**
 * Don't add admins, editors and other supervisors to the supervised list
 */
function hj_approve_supervisor_relationship_exceptions($event, $type, $relationship) {

	if (!$relationship instanceof ElggRelationship) {
		return true;
	}

	$user_one = get_entity($relationship->guid_one);
	$user_two = get_entity($relationship->guid_two);

	if (elgg_instanceof($user_one, 'user')) {
		if (elgg_is_admin_user($user_one->guid)
				|| hj_approve_is_editor($user_one)) {
			if (!elgg_admin_notice_exists('approve:supervisor_relationship_reverse')) {
				elgg_add_admin_notice('approve:supervisor_relationship_reverse', elgg_echo('hj:approve:admin_notice:supervisor_relationship_reverse'));
			}
			return false;
		}
	}

	if (elgg_instanceof($user_two, 'user')) {
		if (elgg_is_admin_user($user_two->guid)
				|| hj_approve_is_editor($user_two)
				|| hj_approve_is_supervisor($user_two)) {
			if (!elgg_admin_notice_exists('approve:supervisor_relationship')) {
				elgg_add_admin_notice('approve:supervisor_relationship', elgg_echo('hj:approve:admin_notice:supervisor_relationship'));
			}
			return false;
		}
	}

	return true;
}

/**
 * Clean up supervisor relationships when the editor is created
 */
function hj_approve_editor_relationship_create($event, $type, $relationship) {

	if (!$relationship instanceof ElggRelationship) {
		return true;
	}

	return remove_entity_relationships($relationship->guid_one, 'supervisor');
}