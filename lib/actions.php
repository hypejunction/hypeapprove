<?php

$path = elgg_get_plugins_path() . 'hypeApprove/actions/';

elgg_register_action('hypeApprove/settings/save', $path . 'settings/save.php', 'admin');

elgg_register_action('approve/admin/import/sf_objects', $path . 'import/sf_objects.php', 'admin');
elgg_register_action('approve/admin/import/sf_users', $path . 'import/sf_users.php', 'admin');
elgg_register_action('approve/admin/scan/spam', $path . 'scan/spam.php', 'admin');

elgg_register_action('approve/admin/make_editor', $path . 'admin/make_editor.php', 'admin');
elgg_register_action('approve/admin/revoke_editor', $path . 'admin/revoke_editor.php', 'admin');

elgg_register_action('approve/supervisor/manage', $path . 'admin/manage_supervisor.php', 'admin');
elgg_register_action('approve/supervisor/assign', $path . 'admin/assign_supervisor.php', 'admin');
elgg_register_action('approve/supervisor/revoke', $path . 'admin/revoke_supervisor.php', 'admin');

elgg_register_action('approve/bulk', $path . 'bulk.php');

elgg_register_action('approve/user/exempt', $path . 'user/exempt.php');
elgg_register_action('approve/user/exempt_revoke', $path . 'user/exempt_revoke.php');
elgg_register_action('approve/user/whitelist', $path . 'user/whitelist.php');
elgg_register_action('approve/user/whitelist_revoke', $path . 'user/whitelist_revoke.php');
elgg_register_action('approve/user/validate', $path . 'user/validate.php');
elgg_register_action('approve/user/set_status', $path . 'user/set_status.php');
elgg_register_action('approve/user/ban', $path . 'user/ban.php');
elgg_register_action('approve/user/unban', $path . 'user/unban.php');
elgg_register_action('approve/user/enable', $path . 'user/enable.php');
elgg_register_action('approve/user/disable', $path . 'user/disable.php');
elgg_register_action('approve/user/delete', $path . 'user/delete.php');

elgg_register_action('approve/content/set_status', $path . 'content/set_status.php');
elgg_register_action('approve/content/enable', $path . 'content/enable.php');
elgg_register_action('approve/content/disable', $path . 'content/disable.php');
elgg_register_action('approve/content/delete', $path . 'content/delete.php');

elgg_register_action('approve/content/report', $path . 'content/report.php');


