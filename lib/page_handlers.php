<?php

elgg_register_page_handler('approve', 'hj_approve_page_handler');

function hj_approve_page_handler($page) {

	$user = elgg_get_logged_in_user_entity();

	switch ($page[0]) {

		case 'dashboard' :

			gatekeeper();

			$is_admin = elgg_is_admin_logged_in();
			$is_editor = hj_approve_is_editor($user);
			$is_supervisor = hj_approve_is_supervisor($user);

			if (!$is_admin && !$is_editor && !$is_supervisor) {
				return false;
			}

			$filter_context = elgg_extract(1, $page, 'pending');

			$title = elgg_echo("hj:approve:dashboard:$filter_context");

			elgg_push_breadcrumb(elgg_echo('hj:approve:dashboard'), 'approve/dashboard');
			elgg_push_breadcrumb($title);

			if ($is_admin) {
				$content = elgg_view('framework/approve/dashboard/admin', array(
					'filter_context' => $filter_context
				));
			} else if ($is_editor) {
				$content = elgg_view('framework/approve/dashboard/editor', array(
					'filter_context' => $filter_context
				));
			} else if ($is_supervisor) {
				$content = elgg_view('framework/approve/dashboard/supervisor', array(
					'filter_context' => $filter_context
				));
			}

			$filter = elgg_view('framework/approve/dashboard/filter', array(
				'filter_context' => $filter_context
			));

			$sidebar = elgg_view('framework/approve/dashboard/sidebar', array(
				'filter_context' => $filter_context
			));

			$layout = elgg_view_layout('content', array(
				'title' => $title,
				'content' => $content,
				'filter' => $filter,
				'sidebar' => $sidebar
			));

			echo elgg_view_page($title, $layout);
			return true;
			break;

		case 'review' :

			gatekeeper();

			$is_admin = elgg_is_admin_logged_in();
			$is_editor = hj_approve_is_editor($user);
			$is_supervisor = hj_approve_is_supervisor($user);

			if (!$is_admin && !$is_editor && !$is_supervisor) {
				return false;
			}

//			$ia = elgg_set_ignore_access(true);
			$ha = access_get_show_hidden_status();
			access_show_hidden_entities(true);

			if ($page[1] == 'annotation') {

				$annotation = elgg_get_annotation_from_id($page[2]);

				if ($annotation instanceof ElggAnnotation
						&& (hj_approve_can_approve(elgg_get_logged_in_user_entity(), $annotation)
							|| hj_approve_can_edit(elgg_get_logged_in_user_entity(), $annotation))) {

					$title = elgg_echo("hj:approve:policy:annotation:$annotation->name");

					$content = elgg_view_annotation($annotation, array(
						'full_view' => true
					));

					$layout = elgg_view_layout('content', array(
						'title' => $title,
						'content' => $content,
						'filter' => false
					));

					echo elgg_view_page(elgg_echo('hj:approve:review'), $layout);
					return true;
				}

			} else {

				$entity = get_entity($page[1]);

				if (elgg_instanceof($entity)
						&& (hj_approve_can_approve(elgg_get_logged_in_user_entity(), $entity)
							|| hj_approve_can_edit(elgg_get_logged_in_user_entity(), $entity))) {

					$title = ($entity->title) ? $entity->title : (($entity->name) ? $entity->name : elgg_echo('hj:approve::notitle'));

					$content = elgg_view_entity($entity, array(
						'full_view' => true
					));

					$layout = elgg_view_layout('content', array(
						'title' => $title,
						'content' => $content,
						'filter' => false
					));

					echo elgg_view_page(elgg_echo('hj:approve:review'), $layout);
					return true;
				}
			}

			access_show_hidden_entities($ha);

			break;

		case 'supervisor' :

			admin_gatekeeper();

			$user = get_entity($page[1]);
			if (!elgg_instanceof($user, 'user')) {
				return false;
			}

			$title = elgg_echo('hj:approve:supervisor:manage');
			$content = elgg_view_form('approve/supervisor/manage', array(), array(
				'entity' => $user
			));

			$layout = elgg_view_layout('content', array(
				'title' => $title,
				'content' => $content,
				'filter' => false
			));

			echo elgg_view_page($title, $layout);
			return true;
			break;

		case HYPEAPPROVE_STATUS_APPROVED :
		case HYPEAPPROVE_STATUS_REJECTED :
		case HYPEAPPROVE_STATUS_PENDING :
		case HYPEAPPROVE_STATUS_FLAGGED :
		case HYPEAPPROVE_STATUS_EXEMPT :

			list($status, $type, $subtype, $guid) = $page;

			// entity is accessible so route it to the original url
			if ($entity = get_entity($guid)) {
				forward(get_entity_url($guid));
			}

			$title = elgg_echo("hj:approve:under_review");
			$content = elgg_view_title($title);

			$layout = elgg_view_layout('error', array(
				'title' => $title,
				'content' => $content,
			));
			echo elgg_view_page($title, $layout);
			return true;

			break;
	}

	return false;
}

