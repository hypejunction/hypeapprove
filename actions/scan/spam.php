<?php

$limit = get_input('limit', 10);
$offset = get_input('offset', 0);

$ia = elgg_set_ignore_access(true);
$ha = access_get_show_hidden_status();
access_show_hidden_entities(true);

$subtype_ids = elgg_get_config('policy_object_spamcheck');
if (count($subtype_ids)) {
	foreach ($subtype_ids as $id) {
		$subtypes[] = get_subtype_from_id($id);
	}
}

$unscanned = elgg_get_entities_from_metadata(array(
	'types' => 'object',
	'subtypes' => $subtypes,
	'metadata_name_value_pairs' => array(
		'name' => 'approval_status',
		'value' => null,
		'operand' => 'IS NULL'
	),
	'limit' => $limit,
	'offset' => $offset
		));


if (!$unscanned) {

	elgg_set_ignore_access($ia);
	access_show_hidden_entities($ha);

	print json_encode(array('complete' => true));
	forward(REFERER);
}

foreach ($unscanned as $entity) {
	hj_approve_apply_entity_spamcheck_policies('', '', $entity);
	if (!$entity->approval_status) {
		$offset++;
	}
}

print json_encode(array(
			'offset' => $offset
		));

elgg_set_ignore_access($ia);
access_show_hidden_entities($ha);

forward(REFERER);