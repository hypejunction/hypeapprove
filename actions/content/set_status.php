<?php

$guids = get_input('content_guids');

if ($guids) {
	$count = count($guids);
	$error_noentity = $error_canedit = $error = $success = $noflag = 0;

	foreach ($guids as $guid) {
		$entity = get_entity($guid);
		if (!elgg_instanceof($entity)) {
			$error_noentity++;
			continue;
		}

		if (!hj_approve_can_approve(elgg_get_logged_in_user_entity(), $entity)) {
			$error_canedit++;
			continue;
		}

		$new_status = get_input('status_content');
		if (!$new_status) {
			$error++;
		}
		$status = $entity->approval_status;
		if (hj_approve_set_approval_status($entity, $new_status, HYPEAPPROVE_REASON_SITEPOLICY, HYPEAPPROVE_TRIGGER_ADMIN, get_input('content_approval_message'))) {
			$subject = elgg_echo("hj:approve:contentapprovalstatus:email:subject");
			
			if (get_input('notify_owners', false)) {
				$body = elgg_view('framework/approve/notifications/contentapprovalstatus', array(
					'entity' => $entity,
					'previous_status' => $status,
					'status' => $new_status,
					'setter' => elgg_get_logged_in_user_entity(),
					'note' => get_input('notify_owners_message')
						));

				notify_user($entity->owner_guid, elgg_get_logged_in_user_guid(), $subject, $body);
			}
			$success++;
		} else {
			$error++;
		}
	}

}


$annotation_ids = get_input('annotation_ids');

if ($annotation_ids) {
	$count += count($annotation_ids);

	foreach ($annotation_ids as $id) {
		$annotation = elgg_get_annotation_from_id($id);
		if (!$annotation instanceof Elggannotation) {
			$error_noentity++;
			continue;
		}

		if (!hj_approve_can_approve(elgg_get_logged_in_user_entity(), $annotation)) {
			$error_canedit++;
			continue;
		}

		$new_status = get_input('status_content');
		if (!$new_status) {
			$error++;
		}

		$owner = $annotation->getOwnerEntity();
		$approval_status = (isset($owner->annotation_approval_status)) ? unserialize($owner->annotation_approval_status) : array();
		$status = elgg_extract($annotation->id, $approval_status, null);
		if (hj_approve_set_annotation_approval_status($annotation, $new_status, HYPEAPPROVE_REASON_SITEPOLICY, HYPEAPPROVE_TRIGGER_ADMIN, get_input('content_approval_message'))) {
			$subject = elgg_echo("hj:approve:contentapprovalstatus:email:subject");

			if (get_input('notify_owners', false)) {
				$body = elgg_view('framework/approve/notifications/annotationapprovalstatus', array(
					'annotation' => $annotation,
					'previous_status' => $status,
					'status' => $new_status,
					'setter' => elgg_get_logged_in_user_entity(),
					'note' => get_input('notify_owners_message')
						));

				notify_user($annotation->owner_guid, elgg_get_logged_in_user_guid(), $subject, $body);
			}
			$success++;
		} else {
			$error++;
		}
	}

}

$msg[] = elgg_echo('hj:approve:success:content:set_status', array((int) $success, $count));
	if ($error_noentity > 0)
		$msg[] = elgg_echo('hj:approve:error:noentity', array($error_noentity));
	if ($error_canedit > 0)
		$msg[] = elgg_echo('hj:approve:error:canedit', array($error_canedit));
	if ($error > 0)
		$msg[] = elgg_echo('hj:approve:error:unknown', array($error));


	system_message(implode('<br />', $msg));