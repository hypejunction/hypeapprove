<?php

$guids = get_input('content_guids');

if ($guids) {
	$count = count($guids);
	$error_noentity = $error_canedit = $error = $success = 0;

	foreach ($guids as $guid) {
		$entity = get_entity($guid);
		if (!elgg_instanceof($entity)) {
			$error_noentity++;
			continue;
		}

		if (!$entity->canEdit()) {
			$error_canedit++;
			continue;
		}
		$subject = elgg_echo("hj:approve:content:delete:email:subject");
		$body = elgg_view('framework/approve/notifications/contentdelete', array(
			'entity' => $entity,
			'setter' => elgg_get_logged_in_user_entity(),
			'note' => get_input('notify_owners_message')
		));

		if ($entity->delete(true)) {
			if (get_input('notify_owners', false)) {
				notify_user($entity->owner_guid, elgg_get_logged_in_user_guid(), $subject, $body);
			}
			$success++;
		} else {
			$error++;
		}
	}
}

$annotation_ids = get_input('annotation_ids');

if ($annotation_ids) {
	$count += count($annotation_ids);

	foreach ($annotation_ids as $id) {
		$annotation = elgg_get_annotation_from_id($id);
		if (!($annotation instanceof ElggAnnotation)) {
			$error_noentity++;
			continue;
		}

		if (!$annotation->canEdit()) {
			$error_canedit++;
			continue;
		}
		$subject = elgg_echo("hj:approve:content:delete:email:subject");
		$body = elgg_view('framework/approve/notifications/annotationdelete', array(
			'annotation' => $annotation,
			'setter' => elgg_get_logged_in_user_entity(),
			'note' => get_input('notify_owners_message')
		));

		if ($annotation->delete(true)) {
			if (get_input('notify_owners', false)) {
				notify_user($annotation->owner_guid, elgg_get_logged_in_user_guid(), $subject, $body);
			}
			$success++;
		} else {
			$error++;
		}
	}
}

$msg[] = elgg_echo('hj:approve:success:content:delete', array((int) $success, $count));
if ($error_noentity > 0)
	$msg[] = elgg_echo('hj:approve:error:noentity', array($error_noentity));
if ($error_canedit > 0)
	$msg[] = elgg_echo('hj:approve:error:canedit', array($error_canedit));
if ($error > 0)
	$msg[] = elgg_echo('hj:approve:error:unknown', array($error));


system_message(implode('<br />', $msg));

