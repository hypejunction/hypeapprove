<?php

$guid = get_input('guid');
$entity = get_entity($guid);
$status = $entity->approval_status;

if ($status) {
	system_message(elgg_echo('hj:approve:report:spam:in_workflow'));
	forward(REFERER);
}

if ($entity && hj_approve_set_approval_status($entity, HYPEAPPROVE_STATUS_FLAGGED, HYPEAPPROVE_REASON_SPAMCHECK, HYPEAPPROVE_TRIGGER_USERREPORT, get_input('message'))) {
	system_message(elgg_echo('hj:approve:report:spam:success'));
	if (HYPEAPPROVE_NOTIFY_USER_REPORT) {
		hj_approve_spamcheck_notify($entity, HYPEAPPROVE_TRIGGER_USERREPORT);
	}
} else if ($guid) {
	register_error(elgg_echo('hj:approve:report:spam:error'));
}

$annotation_id = get_input('annotation_id');
$annotation = elgg_get_annotation_from_id($annotation_id);

if ($annotation && hj_approve_set_annotation_approval_status($annotation, HYPEAPPROVE_STATUS_FLAGGED, HYPEAPPROVE_REASON_SPAMCHECK, HYPEAPPROVE_TRIGGER_USERREPORT, get_input('message'))) {
	system_message(elgg_echo('hj:approve:report:spam:success'));
	if (HYPEAPPROVE_NOTIFY_USER_REPORT) {
		hj_approve_annotation_spamcheck_notify($annotation, HYPEAPPROVE_TRIGGER_USERREPORT);
	}
} else if ($annotation_id) {
	register_error(elgg_echo('hj:approve:report:spam:error'));
}

forward(REFERER);