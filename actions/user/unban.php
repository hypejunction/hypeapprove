<?php

$guids = get_input('user_guids');

if ($guids) {
	$count = count($guids);
	$error_nouser = $error_canedit = $error = $success = $notbanned = 0;

	foreach ($guids as $guid) {
		$user = get_entity($guid);
		if (!elgg_instanceof($user, 'user')) {
			$error_nouser++;
			continue;
		}

		if (!$user->canEdit()) {
			$error_canedit++;
			continue;
		}

		if ($user->guid == elgg_get_logged_in_user_guid()) {
			$error++;
			continue;
		}

		if (!$user->isBanned()) {
			$notbanned++;
		} else {
			if ($user->unban()) {
				create_annotation($user->guid, 'ban', false, '', elgg_get_logged_in_user_guid(), ACCESS_PUBLIC);
				if (get_input('approval_message')) {
					create_annotation($user->guid, 'approval_message', get_input('approval_message'), '', elgg_get_logged_in_user_guid(), ACCESS_PUBLIC);
				}
				$subject = elgg_echo("hj:approve:unban:email:subject");
				if (get_input('notify_users', false)) {
					$body = elgg_view('framework/approve/notifications/unban', array(
						'entity' => $user,
						'setter' => elgg_get_logged_in_user_entity(),
						'note' => get_input('notify_users_message')
							));

					email_notify_handler(elgg_get_logged_in_user_entity(), $user, $subject, $body);
				}
				$success++;
			} else {
				$error++;
			}
		}
	}

	$msg[] = elgg_echo('hj:approve:success:ban', array((int) $success, $count));
	if ($notbanned > 0)
		$msg[] = elgg_echo('hj:approve:error:notbanned', array($notbanned));
	if ($error_nouser > 0)
		$msg[] = elgg_echo('hj:approve:error:nouser', array($error_nouser));
	if ($error_canedit > 0)
		$msg[] = elgg_echo('hj:approve:error:canedit', array($error_canedit));
	if ($error > 0)
		$msg[] = elgg_echo('hj:approve:error:unknown', array($error));


	system_message(implode('<br />', $msg));
}