<?php

$guids = get_input('user_guids');

if ($guids) {
	$count = count($guids);
	$error_nouser = $error_canedit = $error = $success = 0;
	
	foreach ($guids as $guid) {
		$user = get_entity($guid);
		if (!elgg_instanceof($user, 'user')) {
			$error_nouser++;
			continue;
		}

		if (!hj_approve_can_approve(elgg_get_logged_in_user_entity(), $user)) {
			$error_canedit++;
			continue;
		}

		if (elgg_unset_plugin_user_setting('exempt', $user->guid, 'hypeApprove')) {
			create_annotation($user->guid, 'exempt', false, '', elgg_get_logged_in_user_guid(), ACCESS_PUBLIC);
			if (get_input('approval_message')) {
				create_annotation($user->guid, 'approval_message', get_input('approval_message'), '', elgg_get_logged_in_user_guid(), ACCESS_PUBLIC);
			}
			$subject = elgg_echo("hj:approve:exempt_revoke:email:subject");

			if (get_input('notify_users', false)) {
				$body = elgg_view('framework/approve/notifications/exempt_revoke', array(
					'entity' => $user,
					'setter' => elgg_get_logged_in_user_entity(),
					'note' => get_input('notify_users_message')
						));

				notify_user($user->guid, elgg_get_logged_in_user_guid(), $subject, $body);
			}
			$success++;
		} else {
			$error++;
		}
	}

	$msg[] = elgg_echo('hj:approve:success:exempt_revoke', array((int) $success, $count));
	if ($error_nouser > 0)
		$msg[] = elgg_echo('hj:approve:error:nouser', array($error_nouser));
	if ($error_canedit > 0)
		$msg[] = elgg_echo('hj:approve:error:canedit', array($error_canedit));
	if ($error > 0)
		$msg[] = elgg_echo('hj:approve:error:unknown', array($error));


	system_message(implode('<br />', $msg));
}