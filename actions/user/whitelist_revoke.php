<?php

$guids = get_input('user_guids');

if ($guids) {
	$count = count($guids);
	$error_nouser = $error_canedit = $error = $success = 0;
	
	foreach ($guids as $guid) {
		$user = get_entity($guid);
		if (!elgg_instanceof($user, 'user')) {
			$error_nouser++;
			continue;
		}

		if (!hj_approve_can_approve(elgg_get_logged_in_user_entity(), $user)) {
			$error_canedit++;
			continue;
		}

		if (elgg_unset_plugin_user_setting('whitelist', $user->guid, 'hypeApprove')) {
			create_annotation($user->guid, 'exempt', false, '', elgg_get_logged_in_user_guid(), ACCESS_PUBLIC);
			$success++;
		} else {
			$error++;
		}
	}

	$msg[] = elgg_echo('hj:approve:success:whitelist_revoke', array((int) $success, $count));
	if ($error_nouser > 0)
		$msg[] = elgg_echo('hj:approve:error:nouser', array($error_nouser));
	if ($error_canedit > 0)
		$msg[] = elgg_echo('hj:approve:error:canedit', array($error_canedit));
	if ($error > 0)
		$msg[] = elgg_echo('hj:approve:error:unknown', array($error));


	system_message(implode('<br />', $msg));
}