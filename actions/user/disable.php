<?php

$guids = get_input('user_guids');

if ($guids) {
	$count = count($guids);
	$error_nouser = $error_canedit = $error = $success = $disabled = 0;

	foreach ($guids as $guid) {
		$user = get_entity($guid);
		if (!elgg_instanceof($user, 'user')) {
			$error_nouser++;
			continue;
		}

		if (!$user->canEdit()) {
			$error_canedit++;
			continue;
		}

		if ($user->guid == elgg_get_logged_in_user_guid()) {
			$error++;
			continue;
		}

		if (!$user->isEnabled()) {
			$disabled++;
		} else {
			if ($user->disable(get_input('approval_message', 'admin decision'))) {
				create_annotation($user->guid, 'disable', get_input('approval_message', true), '', elgg_get_logged_in_user_guid(), ACCESS_PUBLIC);
				$subject = elgg_echo("hj:approve:disable:email:subject");
				if (get_input('notify_users', false)) {
					$body = elgg_view('framework/approve/notifications/disable', array(
						'entity' => $user,
						'setter' => elgg_get_logged_in_user_entity(),
						'note' => get_input('notify_users_message')
							));

					email_notify_handler(elgg_get_logged_in_user_entity(), $user, $subject, $body);
				}
				$success++;
			} else {
				$error++;
			}
		}
	}

	$msg[] = elgg_echo('hj:approve:success:disable', array((int) $success, $count));
	if ($disabled > 0)
		$msg[] = elgg_echo('hj:approve:error:already_disabled', array($disabled));
	if ($error_nouser > 0)
		$msg[] = elgg_echo('hj:approve:error:nouser', array($error_nouser));
	if ($error_canedit > 0)
		$msg[] = elgg_echo('hj:approve:error:canedit', array($error_canedit));
	if ($error > 0)
		$msg[] = elgg_echo('hj:approve:error:unknown', array($error));


	system_message(implode('<br />', $msg));
}