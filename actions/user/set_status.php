<?php

$guids = get_input('user_guids');

if ($guids) {
	$count = count($guids);
	$error_nouser = $error_canedit = $error = $success = $noflag = 0;

	foreach ($guids as $guid) {
		$user = get_entity($guid);
		if (!elgg_instanceof($user, 'user')) {
			$error_nouser++;
			continue;
		}

		if (!hj_approve_can_approve(elgg_get_logged_in_user_entity(), $user)) {
			$error_canedit++;
			continue;
		}

		$new_status = get_input('status_users');
		if (!$new_status) {
			$error++;
		}
		$status = $user->approval_status;
		if (hj_approve_set_approval_status($user, $new_status, HYPEAPPROVE_REASON_SITEPOLICY, HYPEAPPROVE_TRIGGER_ADMIN, get_input('approval_message'))) {
			$subject = elgg_echo("hj:approve:userapprovalstatus:email:subject");
			
			if (get_input('notify_users', false)) {
				$body = elgg_view('framework/approve/notifications/userapprovalstatus', array(
					'entity' => $user,
					'previous_status' => $status,
					'status' => $new_status,
					'setter' => elgg_get_logged_in_user_entity(),
					'note' => get_input('notify_users_message')
						));

				notify_user($user->guid, elgg_get_logged_in_user_guid(), $subject, $body);
			}
			$success++;
		} else {
			$error++;
		}
	}

	$msg[] = elgg_echo('hj:approve:success:set_status', array((int) $success, $count));
	if ($error_nouser > 0)
		$msg[] = elgg_echo('hj:approve:error:nouser', array($error_nouser));
	if ($error_canedit > 0)
		$msg[] = elgg_echo('hj:approve:error:canedit', array($error_canedit));
	if ($error > 0)
		$msg[] = elgg_echo('hj:approve:error:unknown', array($error));


	system_message(implode('<br />', $msg));
}