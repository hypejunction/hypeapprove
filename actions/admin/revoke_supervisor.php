<?php

$guid = get_input('guid');
$user = get_entity($guid);

if (elgg_instanceof($user, 'user')) {
	if (!remove_entity_relationships($user->guid, 'supervisor')) {
		$error = true;
	}
} else {
	$error = true;
}

if (!$error) {
	system_message(elgg_echo('hj:approve:supervisor:revoke:success'));
} else {
	register_error(elgg_echo('hj:approve:supervisor:revoke:error'));
}

forward(REFERER);