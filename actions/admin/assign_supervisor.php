<?php

$guids = get_input('user_guids');
$supervisors = get_input('members');

if ($guids && $supervisors) {
	$count = count($guids);

	foreach ($supervisors as $supervisor_guid) {
		$error_nouser = $error_canedit = $success = $error = $already = 0;

		foreach ($guids as $guid) {
			$user = get_entity($guid);
			if (!elgg_instanceof($user, 'user')) {
				$error_nouser++;
				continue;
			}

			if (!$user->canEdit()) {
				$error_canedit++;
				continue;
			}

			$supervisor = get_entity($supervisor_guid);
			if (!check_entity_relationship($supervisor->guid, 'supervisor', $user->guid)) {
				if (add_entity_relationship($supervisor->guid, 'supervisor', $user->guid)) {
					$success++;

					create_annotation($user->guid, 'supervisor', $supervisor->guid, '', elgg_get_logged_in_user_guid(), ACCESS_PUBLIC);
					if (get_input('approval_message')) {
						create_annotation($user->guid, 'approval_message', get_input('approval_message'), '', elgg_get_logged_in_user_guid(), ACCESS_PUBLIC);
					}

					if (get_input('notify_users', false)) {
						$subject = elgg_echo("hj:approve:supervisor_assigned:email:subject");
						$body = elgg_view('framework/approve/notifications/supervisor_assigned', array(
							'entity' => $user,
							'supervisor' => $supervisor,
							'setter' => elgg_get_logged_in_user_entity(),
							'note' => get_input('notify_users_message')
								));

						notify_user($user->guid, elgg_get_logged_in_user_guid(), $subject, $body);

						$subject = elgg_echo("hj:approve:supervisor_assigned_user:email:subject");
						$body = elgg_view('framework/approve/notifications/supervisor_assigned_user', array(
							'entity' => $user,
							'supervisor' => $supervisor,
							'setter' => elgg_get_logged_in_user_entity(),
							'note' => get_input('notify_users_message')
								));

						notify_user($supervisor->guid, elgg_get_logged_in_user_guid(), $subject, $body);
					}
				}
			}
		}
		$msg[] = elgg_echo('hj:approve:success:assign_supervisor', array($supervisor->name, (int) $success, $count));
		if ($error_nouser > 0)
			$msg[] = elgg_echo('hj:approve:error:nouser', array($error_nouser));
		if ($error_canedit > 0)
			$msg[] = elgg_echo('hj:approve:error:canedit', array($error_canedit));
	}
} else {
	$msg[] = elgg_echo('error');
}

system_message(implode('<br />', $msg));