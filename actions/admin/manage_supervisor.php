<?php

$guid = get_input('guid');
$members = get_input('members');
$groups = get_input('groups');

// make sure members and groups are empty arrays if no value supplied
if (!$members) {
	$members = array();
}

if (!$groups) {
	$groups = array();
}

$supervisor = get_entity($guid);

if (!elgg_instanceof($supervisor)) {
	register_error('hj:approve:supervisor:notfound');
	forward(REFERER);
}

$current_supervised_users = elgg_get_entities_from_relationship(array(
	'types' => 'user',
	'relationship' => 'supervisor',
	'relationship_guid' => $supervisor->guid,
	'inverse_relationship' => false,
	'limit' => 9999
		));
$current_guids = array();
if ($current_supervised_users) {
	foreach ($current_supervised_users as $user) {
		$current_guids[] = $user->guid;
	}
}

$deleted_count = $added_count = $error_count = 0;

$to_delete = array_diff($current_guids, $members);
$to_add = array_diff($members, $current_guids);

if ($to_delete) {
	foreach ($to_delete as $d) {
		if (check_entity_relationship($supervisor->guid, 'supervisor', $d)) {
			if (remove_entity_relationship($supervisor->guid, 'supervisor', $d)) {
				$deleted_count++;
			} else {
				$error_count++;
			}
		}
	}
}

if ($to_add) {
	foreach ($to_add as $a) {
		if (!check_entity_relationship($supervisor->guid, 'supervisor', $a)) {
			if (add_entity_relationship($supervisor->guid, 'supervisor', $a)) {
				$added_count++;
			} else {
				$error_count++;
			}
		}
	}
}

if ($added_count)
	system_message(elgg_echo('hj:approve:supervisor:supervised_users:add', array($added_count)));

if ($deleted_count)
	system_message(elgg_echo('hj:approve:supervisor:supervised_users:delete', array($deleted_count)));



$current_supervised_groups = elgg_get_entities_from_relationship(array(
	'types' => 'group',
	'relationship' => 'supervisor',
	'relationship_guid' => $supervisor->guid,
	'inverse_relationship' => false,
	'limit' => 9999
		));
$current_group_guids = array();
if ($current_supervised_groups) {
	foreach ($current_supervised_groups as $user) {
		$current_group_guids[] = $user->guid;
	}
}

$deleted_count = $added_count = 0;

$to_delete = array_diff($current_group_guids, $groups);
$to_add = array_diff($groups, $current_group_guids);

if ($to_delete) {
	foreach ($to_delete as $d) {
		if (check_entity_relationship($supervisor->guid, 'supervisor', $d)) {
			if (remove_entity_relationship($supervisor->guid, 'supervisor', $d)) {
				$deleted_count++;
			} else {
				$error_count++;
			}
		}
	}
}

if ($to_add) {
	foreach ($to_add as $a) {
		if (!check_entity_relationship($supervisor->guid, 'supervisor', $a)) {
			if (add_entity_relationship($supervisor->guid, 'supervisor', $a)) {
				$added_count++;
			} else {
				$error_count++;
			}
		}
	}
}

if ($added_count)
	system_message(elgg_echo('hj:approve:supervisor:supervised_groups:add', array($added_count)));

if ($deleted_count)
	system_message(elgg_echo('hj:approve:supervisor:supervised_groups:delete', array($deleted_count)));


if ($error_count)
	system_message(elgg_echo('hj:approve:supervisor:supervised_users_groups:error', array($error_count)));

forward(REFERER);