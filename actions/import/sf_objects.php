<?php

/**
 * Import hypeSpamFighter metadata
 */
$ia = elgg_set_ignore_access(true);
$ha = access_get_show_hidden_status();
access_show_hidden_entities(true);

$limit = get_input('limit', 10);
$offset = get_input('offset', 0);

$spamfighter_entities = elgg_get_entities_from_metadata(array(
	'types' => 'object',
	'metadata_names' => array('spamreport', 'spamsafe', 'spamreport_source'),
	'limit' => $limit,
	'offset' => $offset
		));


if (!$spamfighter_entities) {
	print json_encode(array('complete' => true));
	forward(REFERER);
}

foreach ($spamfighter_entities as $entity) {

	if ($entity->spamreport_source == 'user') {
		if (isset($entity->spamreport)) {
			hj_approve_set_approval_status($entity, HYPEAPPROVE_STATUS_FLAGGED, HYPEAPPROVE_REASON_SPAMCHECK, HYPEAPPROVE_TRIGGER_USERREPORT);
		}
		if ($entity->spamsafe) {
			hj_approve_set_approval_status($entity, HYPEAPPROVE_STATUS_APPROVED);
		}
	} else if ($entity->spamreport_source == 'akismet') {
		if (isset($entity->spamreport)) {
			hj_approve_set_approval_status($entity, HYPEAPPROVE_STATUS_PENDING, HYPEAPPROVE_REASON_SPAMCHECK, HYPEAPPROVE_TRIGGER_AKISMET);
			if ($entity->spamreport) {
				$entity->access_id = ACCESS_APPROVAL_WORKFLOW;
			}
		}
		if ($entity->spamsafe) {
			hj_approve_set_approval_status($entity, HYPEAPPROVE_STATUS_APPROVED);
		}
	}

	if ($entity->save()) {
		elgg_delete_metadata(array(
			'guid' => $entity->guid,
			'metadata_names' => array('spamreport', 'spamsafe', 'spamreport_source'),
			'limit' => 0
		));
	} else {
		$offset++;
	}
}

print json_encode(array(
			'offset' => $offset
		));

elgg_set_ignore_access($ia);
access_show_hidden_entities($ha);

forward(REFERER);