<?php

/**
 * Import hypeSpamFighter metadata
 */
$ia = elgg_set_ignore_access(true);
$ha = access_get_show_hidden_status();
access_show_hidden_entities(true);

$limit = get_input('limit', 10);
$offset = get_input('offset', 0);

$spamfighter_entities = elgg_get_entities_from_metadata(array(
	'types' => 'user',
	'metadata_names' => array('spamcount', 'last_ip'),
	'limit' => $limit,
	'offset' => $offset
		));

if (!$spamfighter_entities) {
	print json_encode(array('complete' => true));
	forward(REFERER);
}

foreach ($spamfighter_entities as $entity) {

	elgg_set_plugin_user_setting('akismet_fail_count', $entity->spamcount, $entity->guid, 'hypeApprove');
	elgg_set_plugin_user_setting('last_ip', $entity->last_ip, $entity->guid, 'hypeApprove');

	if (!elgg_delete_metadata(array(
			'guid' => $entity->guid,
			'metadata_names' => array('spamcount', 'last_ip'),
			'limit' => 0
		))) {
		$offset++;
	}

}

print json_encode(array(
			'offset' => $offset
		));

elgg_set_ignore_access($ia);
access_show_hidden_entities($ha);

forward(REFERER);