hypeApprove
===========

Content approval workflow for Elgg

## Description ##

hypeApprove implements a content approval workflow with a granular control over
types of content to be queued for manual review, checked for spam, or subjected
to review by editors and/or supervisors.

This plugin sets and retains the access level at Private until the content is
Approved, thus giving the author an opportunity to continue updating the content.


## Features ##

* Granular controls over approval policies
* Assign editors and control what editing and/or approval privileges they should have over site content
* Assign supervisors over defined user groups and control what privileges they should have over user content
* Akismet and StopForumSpam integration to limit spam, including granular control over types of content should be checked against spam
* Granular controls over notifications
* Abuse reporting by users
* Annotated approval workflow history
* Restrict users from performing certain operations and accessing certain sites when their accounts are pending approval or have been rejected
* Administrative dashboard for performing bulk operations over user accounts:
** Exempt/Add users to the approval workflow
** Whitelist/Blacklist users for spam checks
** Change user status to Needs Review, Pending, Approved, or Rejected
** Assign supervisors to user accounts
** Validate user accounts
** Ban/Unban user accounts
** Enable/Disable user accounts
** Delete user accounts with all user content
* Administrative dashboard for performing bulk operations over content in the approval workflow:
** Change status to Needs Review, Pending, Approved, or Rejected
** Disable/Enable content
** Delete content
* Scan older site content for spam
* NEW in 1.9.2  -  Moderate annotations (messageboard posts, comments, group topic posts)

## Acknowledgements ##

* (re)development of this plugin was inspired and partially sponsored by Whitemoor School

## Notes ##

hypeFramework is not required to run this plugin.
hypeSpamFighter data can be imported into this plugin via the admin interface.

## Screenshots ##

![alt text](https://raw.github.com/hypeJunction/hypeApprove/master/screenshots/settings.png "Settings Page")
![alt text](https://raw.github.com/hypeJunction/hypeApprove/master/screenshots/settings2.png "Settings Page")
![alt text](https://raw.github.com/hypeJunction/hypeApprove/master/screenshots/settings3.png "Settings Page")
![alt text](https://raw.github.com/hypeJunction/hypeApprove/master/screenshots/interface.png "Approval Interface")