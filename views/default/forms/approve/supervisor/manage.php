<?php

$supervisor = elgg_extract('entity', $vars);

if (!elgg_instanceof($supervisor, 'user')) {
	return;
}

echo '<div>';
echo '<label>' . elgg_echo('hj:approve:supervisor') . '</label>';
echo elgg_view_entity($supervisor, array(
	'full_view' => false
));
echo '</div>';

$supervised_users = $supervised = elgg_get_entities_from_relationship(array(
	'types' => 'user',
	'relationship' => 'supervisor',
	'relationship_guid' => $supervisor->guid,
	'inverse_relationship' => false,
	'limit' => 9999
		));

$guids = array();

if ($supervised_users) {
	foreach ($supervised_users as $user) {
		$guids[] = $user->guid;
	}
}

echo '<div>';
echo '<label>' . elgg_echo('hj:approve:supervisor:supervised_users') . '</label>';
echo elgg_view('input/userpicker', array(
	'value' => $guids
));
echo '</div>';


$supervised_groups = hj_approve_get_supervised_groups($supervisor);

if ($supervised_groups) {
	foreach ($supervised_groups as $group) {
		$group_values[] = $group->guid;
	}
}

$groups = elgg_get_entities(array(
	'types' => 'group',
	'limit' => 9999
		));

$group_options = array();

if ($groups) {
	foreach ($groups as $group) {
		$str = "$group->name ({$group->getMembers(0, 0, true)})";
		$group_options[$str] = $group->guid;
	}
}

echo '<div>';
echo '<label>' . elgg_echo('hj:approve:supervisor:supervised_groups') . '</label>';
echo '<div class="elgg-text-help">' . elgg_echo('hj:approve:supervisor:supervised_groups:info') . '</div>';

echo elgg_view('input/checkboxes', array(
	'name' => 'groups',
	'value' => $group_values,
	'options' => $group_options
));
echo '</div>';


echo '<div class="elgg-foot">';
echo elgg_view('input/hidden', array(
	'name' => 'guid',
	'value' => $supervisor->guid
));
echo elgg_view('input/submit', array(
	'value' => elgg_echo('save')
));
echo '</div>';