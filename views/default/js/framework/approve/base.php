<?php if (FALSE) : ?>
	<script type="text/javascript">
<?php endif; ?>

elgg.provide('framework');
elgg.provide('framework.approve');

framework.approve.menu = function() {

	$('.elgg-menu-item-report-spam')
	.live('click', function(e) {
		e.preventDefault();

		var message = prompt(elgg.echo('hj:approve:report:message'), '');
		var action = $(this).find('a').eq(0).attr('href');
		elgg.action(action, {
			data : {
				message : message
			}
		});
	})
}

elgg.register_hook_handler('init', 'system', framework.approve.menu);

<?php if (FALSE) : ?></script><?php
endif;
?>
