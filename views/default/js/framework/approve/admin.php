<?php if (FALSE) : ?><script type="text/javascript"><?php endif; ?>

	elgg.provide('framework.approve');
	elgg.provide('framework.approve.admin');

	framework.approve.admin.init = function() {

		$('#hj-approve-admin-import-sf-objects')
		.click(function(e) {

			var confirmText = $(this).attr('rel');
			if (!confirm(confirmText)) {
				e.preventDefault();
				return false;
			}
	
			$('#import-sf-objects-progress').progressbar({
				value : 0
			});

			$(this).hide();

			var params = {
				count : $(this).data('count'),
				offset : 0,
				progress : 0,
				limit : 10
			}

			elgg.trigger_hook('import:sf_objects', 'framework:approve', params);
		})

		$('#hj-approve-admin-import-sf-users')
		.click(function(e) {

			var confirmText = $(this).attr('rel');
			if (!confirm(confirmText)) {
				e.preventDefault();
				return false;
			}

			$('#import-sf-users-progress').progressbar({
				value : 0
			});

			$(this).hide();

			var params = {
				count : $(this).data('count'),
				offset : 0,
				progress : 0,
				limit : 10
			}

			elgg.trigger_hook('import:sf_users', 'framework:approve', params);
		})

		$('#hj-approve-admin-scan-spam')
		.click(function(e) {

			$('#scan-spam-progress').progressbar({
				value : 0
			});

			$(this).hide();

			var params = {
				count : $(this).data('count'),
				offset : 0,
				progress : 0,
				limit : 10
			}

			elgg.trigger_hook('scan:spam', 'framework:approve', params);
		})

	}

	framework.approve.admin.importSFObjects = function(hook, type, params) {
		elgg.action('action/approve/admin/import/sf_objects', {
			data : params,
			success : function(data) {

				if (!data.output.complete) {
					params.offset = data.output.offset;
					params.progress = params.progress + params.limit;

					$('#import-sf-objects-progress').progressbar({
						value : params.progress * 100 / params.count
					})

					elgg.trigger_hook('import:sf_objects', 'framework:approve', params);
				} else {
					elgg.system_message(elgg.echo('hj:approve:admin:import_complete'));
					$('#import-sf-objects-progress').progressbar({
						value : 100
					})
					location.reload();
				}
			}
		})
	}

	framework.approve.admin.importSFUsers = function(hook, type, params) {
		elgg.action('action/approve/admin/import/sf_users', {
			data : params,
			success : function(data) {

				if (!data.output.complete) {
					params.offset = data.output.offset;
					params.progress = params.progress + params.limit;

					$('#import-sf-users-progress').progressbar({
						value : params.progress * 100 / params.count
					})

					elgg.trigger_hook('import:sf_users', 'framework:approve', params);
				} else {
					elgg.system_message(elgg.echo('hj:approve:admin:import_complete'));
					$('#import-sf-users-progress').progressbar({
						value : 100
					})
					location.reload();
				}
			}
		})
	}

	framework.approve.admin.scanSpam = function(hook, type, params) {
		elgg.action('action/approve/admin/scan/spam', {
			data : params,
			success : function(data) {

				if (!data.output.complete) {
					params.offset = data.output.offset;
					params.progress = params.progress + params.limit;

					$('#scan-spam-progress').progressbar({
						value : params.progress * 100 / params.count
					})

					elgg.trigger_hook('scan:spam', 'framework:approve', params);
				} else {
					elgg.system_message(elgg.echo('hj:approve:admin:span_complete'));
					$('#scan-spam-progress').progressbar({
						value : 100
					})
					//location.reload();
				}
			}
		})
	}

	elgg.register_hook_handler('init', 'system', framework.approve.admin.init);
	elgg.register_hook_handler('import:sf_objects', 'framework:approve', framework.approve.admin.importSFObjects);
	elgg.register_hook_handler('import:sf_users', 'framework:approve', framework.approve.admin.importSFUsers);
	elgg.register_hook_handler('scan:spam', 'framework:approve', framework.approve.admin.scanSpam);

<?php if (FALSE) : ?></script><?php endif; ?>