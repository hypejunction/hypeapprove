<?php if (FALSE) : ?>
	<script type="text/javascript">
<?php endif; ?>

elgg.provide('framework');
elgg.provide('framework.approve');

framework.approve.dashboard = function() {

	$('[name="user_guids[]"]')
	.live('change', function(e) {
		e.preventDefault();

		if (this.checked) {
			$(this).closest('.approve-user').find('[name="content_guids[]"]').prop('checked', true);
		} else {
			$(this).closest('.approve-user').find('[name="content_guids[]"]').prop('checked', false);
		}
	})

	$('#approve-select-all-items')
	.live('change', function(e) {
		e.preventDefault();

		if (this.checked) {
			$(this).closest('form').find('[name="user_guids[]"],[name="content_guids[]"]').prop('checked', true);
		} else {
			$(this).closest('form').find('[name="user_guids[]"],[name="content_guids[]"]').prop('checked', false);
		}
	})

	$('[name="search_values[types]"]')
	.live('change', function(e) {
		$form = $(this).closest('form');
		switch ($(this).val()) {
			case 'user' :
				$form.find('.extra-options').hide();
				$form.find('.extra-options-users').show();
				break;

			case 'object' :
				$form.find('.extra-options').hide();
				$form.find('.extra-options-objects').show();
				break;

			default :
				$form.find('.extra-options').hide();
				break;
		}
	})
	$('#approve-list [name="user_action"]')
	.live('change', function(e) {
		var val = $(this).val();
		if (val != 0) {
			$('#approve-list .extras-user-action').show();
			if (val == 'approve/user/set_status') {
				$('#approve-list .extras-user-action-status').show();
			} else if (val == 'approve/supervisor/assign') {
				$('#approve-list .extras-user-action-supervisor').show();
			} else {
				$('#approve-list .extras-user-action-status').hide().find('select').val(0);
				$('#approve-list .extras-user-action-supervisor').hide();
			}
		} else {
			$('#approve-list .extras-user-action').hide();
		}
	})

	$('#approve-list [name="content_action"]')
	.live('change', function(e) {
		var val = $(this).val();
		if (val != 0) {
			$('#approve-list .extras-content-action').show();
			if (val == 'approve/content/set_status') {
				$('#approve-list .extras-content-action-status').show();
			} else {
				$('#approve-list .extras-content-action-status').hide().find('select').val(0);
			}
		} else {
			$('#approve-list .extras-content-action').hide();
		}
	})

}

elgg.register_hook_handler('init', 'system', framework.approve.dashboard);

<?php if (FALSE) : ?></script><?php
endif;
?>
