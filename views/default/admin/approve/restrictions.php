<?php

// Unapproved users

$restrictions = elgg_get_config('policy_user_restrictions');
$action_restrictions = elgg_extract('actions', $restrictions, array());
$page_restrictions = elgg_extract('pages', $restrictions, array());
$event_restrictions = elgg_extract('events', $restrictions, array());

$actions = elgg_get_config('actions');
$pages = elgg_get_config('pagehandler');
$events = elgg_get_config('events');
foreach (array('init', 'boot', 'ready', 'pagesetup', 'all', 'log', 'upgrade') as $e) {
	unset($events[$e]);
}

$form .= elgg_view_title(elgg_echo('hj:approve:user:restrictions'));
$form .= '<p>' . elgg_echo('hj:approve:user:restrictions:intro') . '</p>';
$form .= '<table class="elgg-table-alt">';

$form .= '<thead>';
$form .= '<tr>';
$form .= '<th></th>';
$form .= '<th>' . elgg_echo('hj:approve:status:flagged') . '</th>';
$form .= '<th>' . elgg_echo('hj:approve:status:pending') . '</th>';
$form .= '<th>' . elgg_echo('hj:approve:status:rejected') . '</th>';
$form .= '</tr>';
$form .= '</thead>';

$form .= '<tbody>';

$form .= '<tr><td colspan="4">' . elgg_view_title(elgg_echo('hj:approve:user:restrictions:actions')) . '</td></tr>';

foreach ($actions as $name => $settings) {
	if ($settings['access'] != 'logged_in')
		continue;

	$form .= '<tr>';
	$form .= '<td>' . $name . '</td>';
	$form .= '<td>' . elgg_view('input/checkbox', array(
				'name' => 'params[policy_user_restrictions][actions][flagged][]',
				'value' => $name,
				'checked' => in_array($name, elgg_extract('flagged', $action_restrictions, array()))
			)) . '</td>';
	$form .= '<td>' . elgg_view('input/checkbox', array(
				'name' => 'params[policy_user_restrictions][actions][pending][]',
				'value' => $name,
				'checked' => in_array($name, elgg_extract('pending', $action_restrictions, array()))
			)) . '</td>';
	$form .= '<td>' . elgg_view('input/checkbox', array(
				'name' => 'params[policy_user_restrictions][actions][rejected][]',
				'value' => $name,
				'checked' => in_array($name, elgg_extract('rejected', $action_restrictions, array()))
			)) . '</td>';
	$form .= '</tr>';
}

$form .= '<tr><td colspan="4">' . elgg_view_title(elgg_echo('hj:approve:user:restrictions:pages')) . '</td></tr>';

foreach ($pages as $handler => $callback) {

	$form .= '<tr>';
	$form .= '<td>' . $handler . '</td>';
	$form .= '<td>' . elgg_view('input/checkbox', array(
				'name' => 'params[policy_user_restrictions][pages][flagged][]',
				'value' => $handler,
				'checked' => in_array($handler, elgg_extract('flagged', $page_restrictions, array()))
			)) . '</td>';
	$form .= '<td>' . elgg_view('input/checkbox', array(
				'name' => 'params[policy_user_restrictions][pages][pending][]',
				'value' => $handler,
				'checked' => in_array($handler, elgg_extract('pending', $page_restrictions, array()))
			)) . '</td>';
	$form .= '<td>' . elgg_view('input/checkbox', array(
				'name' => 'params[policy_user_restrictions][pages][rejected][]',
				'value' => $handler,
				'checked' => in_array($handler, elgg_extract('rejected', $page_restrictions, array()))
			)) . '</td>';
	$form .= '</tr>';
}


$form .= '<tr><td colspan="4">' . elgg_view_title(elgg_echo('hj:approve:user:restrictions:events')) . '</td></tr>';

foreach ($events as $event => $options) {

	foreach ($options as $type => $callback) {
		if ($type == 'all')
			continue;

		$form .= '<tr>';
		$form .= "<td>'$event','$type'</td>";
		$form .= '<td>' . elgg_view('input/checkbox', array(
					'name' => 'params[policy_user_restrictions][events][flagged][]',
					'value' => "$event::$type",
					'checked' => in_array("$event::$type", elgg_extract('flagged', $event_restrictions, array()))
				)) . '</td>';
		$form .= '<td>' . elgg_view('input/checkbox', array(
					'name' => 'params[policy_user_restrictions][events][pending][]',
					'value' => "$event::$type",
					'checked' => in_array("$event::$type", elgg_extract('pending', $event_restrictions, array()))
				)) . '</td>';
		$form .= '<td>' . elgg_view('input/checkbox', array(
					'name' => 'params[policy_user_restrictions][events][rejected][]',
					'value' => "$event::$type",
					'checked' => in_array("$event::$type", elgg_extract('rejected', $event_restrictions, array()))
				)) . '</td>';
		$form .= '</tr>';
	}
}

$form .= '</tbody>';

$form .= '</table>';

$form .= '<div class="elgg-foot">';
$form .= elgg_view('input/submit', array(
	'value' => elgg_echo('save'),
	'class' => 'mam'
		));
$form .= '</div>';
echo elgg_view('input/form', array(
	'action' => 'action/hypeApprove/settings/save',
	'body' => $form,
));
