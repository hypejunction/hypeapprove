<?php

// User Reporting
$title = elgg_echo('hj:approve:admin:spam:section:reporting');

$mod = '<div>';
$mod .= '<label>' . elgg_echo('hj:approve:admin:spam:spam_reporting') . '</label>';
$mod .= elgg_view('input/dropdown', array(
	'name' => 'params[spam_reporting]',
	'value' => HYPEAPPROVE_SPAM_REPORTING,
	'options_values' => array(
		0 => elgg_echo('disable'),
		1 => elgg_echo('enable')
	)
		));
$mod .= '</div>';

$mod = '<div class="mam">' . $mod . '</div>';
$form .= elgg_view_module('widget', $title, $mod);


// Akismet
$title = elgg_echo('hj:approve:admin:spam:section:akismet');

$mod = '<div class="elgg-subtext">' . elgg_echo('hj:approve:admin:spam:info:akismet') . '</div>';
$mod .= '<div>';
$mod .= '<label>' . elgg_echo('hj:approve:admin:spam:akismet_key') . '</label>';
$mod .= elgg_view('input/text', array(
	'name' => 'params[akismet_key]',
	'value' => HYPEAPPROVE_AKISMET_KEY
		));
$mod .= '</div>';

$mod .= '<div>';
$mod .= '<label>' . elgg_echo('hj:approve:admin:spam:akismet_fail_count') . '</label>';
$mod .= elgg_view('input/text', array(
	'name' => 'params[akismet_fail_count]',
	'value' => HYPEAPPROVE_AKISMET_FAIL_COUNT
		));
$mod .= '</div>';

$mod .= '<div>';
$mod .= '<label>' . elgg_echo('hj:approve:admin:spam:akismet_fail_count_action') . '</label>';
$mod .= elgg_view('input/dropdown', array(
	'name' => 'params[akismet_fail_count_action]',
	'value' => HYPEAPPROVE_AKISMET_FAIL_COUNT_ACTION,
	'options_values' => array(
		'ban' => elgg_echo('hj:approve:bulk:user:ban'),
		'disable' => elgg_echo('hj:approve:bulk:user:disable'),
		'delete' => elgg_echo('hj:approve:bulk:user:delete'),
	)
		));
$mod .= '</div>';

$mod = '<div class="mam">' . $mod . '</div>';

$form .= elgg_view_module('widget', $title, $mod);


// Stop forum spam
$title = elgg_echo('hj:approve:admin:spam:section:stopforumsspam');
$mod = '<div class="elgg-subtext">' . elgg_echo('hj:approve:admin:spam:info:stopforumspam') . '</div>';
$mod .= '<div>';
$mod .= '<label>' . elgg_echo('hj:approve:admin:spam:stopforumspam_key') . '</label>';
$mod .= elgg_view('input/text', array(
	'name' => 'params[stopforumspam_key]',
	'value' => HYPEAPPROVE_STOPFORUMSPAM_KEY
		));
$mod .= '</div>';

$mod = '<div class="mam">' . $mod . '</div>';

$form .= elgg_view_module('widget', $title, $mod);


// Import hypeSpamFighter object metadata
$spamfighter_objects = elgg_get_entities_from_metadata(array(
	'types' => 'object',
	'metadata_names' => array('spamreport', 'spamsafe', 'spamreport_source'),
	'count' => true
		));

if ($spamfighter_objects) {
	echo elgg_view('framework/approve/admin/import_sf_objects', array(
		'count' => $spamfighter_objects
	));
}


// Import hypeSpamFighter user metadata
$spamfighter_users = elgg_get_entities_from_metadata(array(
	'types' => 'user',
	'metadata_names' => array('spamcount', 'last_ip'),
	'count' => true
		));

if ($spamfighter_users) {
	echo elgg_view('framework/approve/admin/import_sf_users', array(
		'count' => $spamfighter_users
	));
}

$subtype_ids = elgg_get_config('policy_object_spamcheck');
if (count($subtype_ids)) {
	foreach ($subtype_ids as $id) {
		$subtypes[] = get_subtype_from_id($id);
	}
}

if ($subtypes) {
	$ia = elgg_set_ignore_access(true);
	$ha = access_get_show_hidden_status();
	access_show_hidden_entities(true);

	$unscanned = elgg_get_entities_from_metadata(array(
		'types' => 'object',
		'subtypes' => $subtypes,
		'metadata_name_value_pairs' => array(
			'name' => 'approval_status',
			'value' => null,
			'operand' => 'IS NULL'
		),
		'count' => true
			));

	elgg_set_ignore_access($ia);
	access_show_hidden_entities($ha);
}

if ($unscanned) {
	echo elgg_view('framework/approve/admin/scan_spam', array(
		'count' => $unscanned
	));
}

$form .= '<div class="elgg-foot">';
$form .= elgg_view('input/submit', array(
	'value' => elgg_echo('save'),
	'class' => 'mam'
		));
$form .= '</div>';

echo elgg_view('input/form', array(
	'action' => 'action/hypeApprove/settings/save',
	'body' => $form,
));

elgg_load_js('approve.admin.js');