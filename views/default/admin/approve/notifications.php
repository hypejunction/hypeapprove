<?php

$recipients = array('admin', 'editor', 'supervisor');
$notifications = array('queue', 'user_queue', 'user_report', 'akismet', 'workflow');
$notifications_conf = elgg_get_config('policy_notifications');

$form .= '<p>' . elgg_echo('hj:approve:notifications:intro') . '</p>';

$form .= '<table class="elgg-table-alt">';

$form .= '<thead>';
$form .= '<tr>';
$form .= '<th></th>';
foreach ($recipients as $recipient) {
	$form .= '<th>' . elgg_echo("hj:approve:notifications:notify:$recipient") . '</th>';
}
$form .= '</tr>';
$form .= '</thead>';

$form .= '<tbody>';
foreach ($notifications as $notification) {
	$form .= '<tr>';
	$form .= '<td>' . elgg_echo("hj:approve:notifications:$notification") . '</td>';

	foreach ($recipients as $recipient) {
		if ($notification == 'user_queue' && $recipient == 'supervisor') {
			$form .= '<td>n/a</td>';
		} else {
			$form .= '<td>' . elgg_view('input/checkbox', array(
						'name' => "params[policy_notifications][$notification][$recipient]",
						'value' => 1,
						'checked' => (bool)$notifications_conf[$notification][$recipient]
					)) . '</td>';
		}
	}

	$form .= '</tr>';
}
$form .= '</tbody>';

$form .= '</table>';

$form .= '<div>';
$form .= '<label>' . elgg_echo('hj:approve:notifications:include_export') . '</label>';
$form .= elgg_view('input/dropdown', array(
	'name' => 'params[notification_include_export]',
	'value' => HYPEAPPROVE_NOTIF_INCL_EXPORT,
	'options_values' => array(
		true => elgg_echo('Yes'),
		false => elgg_echo('No')
	)
));
$form .= '</div>';

$form .= '<div class="elgg-foot">';
$form .= elgg_view('input/submit', array(
	'value' => elgg_echo('save'),
	'class' => 'mam'
		));
$form .= '</div>';
echo elgg_view('input/form', array(
	'action' => 'action/hypeApprove/settings/save',
	'body' => $form,
));