<?php

$registered_entities = elgg_get_config('registered_entities');

if (empty($registered_entities)) {
	echo elgg_echo('hj:approve:noregisteredentities');
	return;
}


foreach ($registered_entities as $type => $subtypes) {
	switch ($type) {

		case 'object' :
			if (count($subtypes)) {
				foreach ($subtypes as $subtype) {
					$label = elgg_echo("item:$type:$subtype");
					$id = get_subtype_id($type, $subtype);
					if ($id) {
						$object_options[$id] = $label;
					}
				}
			}


			break;

		case 'user' :
			break;

		case 'group' :
			break;
	}
}

$form .= '<table class="elgg-table-alt">';


$form .= '<thead>';
$form .= '<tr>';
$form .= '<th></th>';
$form .= '<th>' . elgg_echo('hj:approve:policy:requires_approval') . '</th>';
$form .= '<th>' . elgg_echo('hj:approve:policy:requires_spamcheck') . '</th>';
$form .= '<th>' . elgg_echo('hj:approve:policy:editor_can_edit') . '</th>';
$form .= '<th>' . elgg_echo('hj:approve:policy:editor_can_approve') . '</th>';
$form .= '<th>' . elgg_echo('hj:approve:policy:supervisor_can_edit') . '</th>';
$form .= '<th>' . elgg_echo('hj:approve:policy:supervisor_can_approve') . '</th>';
$form .= '</tr>';
$form .= '</thead>';


$form .= '<tbody>';

// User privileges
$form .= '<tr><td colspan="7"><h3 class="center mam">' . elgg_echo('hj:approve:policy:users') . '</h3></td></tr>';

$form .= '<tr>';
$form .= '<td>' . elgg_echo('item:user') . '</td>';

$form .= '<td>' . elgg_view('input/checkbox', array(
			'name' => 'params[policy_user_approval]',
			'value' => 1,
			'checked' => (elgg_get_config('policy_user_approval'))
		)) . '</td>';

$form .= '<td>' . elgg_view('input/checkbox', array(
			'name' => 'params[policy_user_spamcheck]',
			'value' => 1,
			'checked' => (elgg_get_config('policy_user_spamcheck'))
		)) . '</td>';

$form .= '<td>' . elgg_view('input/checkbox', array(
			'name' => 'params[policy_user_editor_can_edit]',
			'value' => 1,
			'checked' => (elgg_get_config('policy_user_editor_can_edit'))
		)) . '</td>';

$form .= '<td>' . elgg_view('input/checkbox', array(
			'name' => 'params[policy_user_editor_can_approve]',
			'value' => 1,
			'checked' => elgg_get_config('policy_user_editor_can_approve')
		)) . '</td>';


$form .= '<td>' . elgg_view('input/checkbox', array(
			'name' => 'params[policy_user_supervisor_can_edit]',
			'value' => 1,
			'checked' => (elgg_get_config('policy_user_supervisor_can_edit'))
		)) . '</td>';

$form .= '<td>n/a</td>';
$form .= '</tr>';

// Group privileges
$form .= '<tr><td colspan="7"><h3 class="center mam">' . elgg_echo('hj:approve:policy:groups') . '</h3></td></tr>';

$form .= '<tr>';
$form .= '<td>' . elgg_echo('item:group') . '</td>';

$form .= '<td>' . elgg_view('input/checkbox', array(
			'name' => 'params[policy_group_approval]',
			'value' => 1,
			'checked' => (elgg_get_config('policy_group_approval'))
		)) . '</td>';

$form .= '<td>' . elgg_view('input/checkbox', array(
			'name' => 'params[policy_group_spamcheck]',
			'value' => 1,
			'checked' => (elgg_get_config('policy_group_spamcheck'))
		)) . '</td>';

$form .= '<td>' . elgg_view('input/checkbox', array(
			'name' => 'params[policy_group_editor_can_edit]',
			'value' => 1,
			'checked' => (elgg_get_config('policy_group_editor_can_edit'))
		)) . '</td>';

$form .= '<td>' . elgg_view('input/checkbox', array(
			'name' => 'params[policy_group_editor_can_approve]',
			'value' => 1,
			'checked' => elgg_get_config('policy_group_editor_can_approve')
		)) . '</td>';


$form .= '<td>' . elgg_view('input/checkbox', array(
			'name' => 'params[policy_group_supervisor_can_edit]',
			'value' => 1,
			'checked' => (elgg_get_config('policy_group_supervisor_can_edit'))
		)) . '</td>';

$form .= '<td>' . elgg_view('input/checkbox', array(
			'name' => 'params[policy_group_supervisor_can_approve]',
			'value' => 1,
			'checked' => (elgg_get_config('policy_group_supervisor_can_approve'))
		)) . '</td>';

$form .= '</tr>';

$form .= '<tr><td colspan="7"><h3 class="center mam">' . elgg_echo('hj:approve:policy:objects') . '</h3></td></tr>';

// Object privileges
foreach ($object_options as $subtype_id => $label) {

	$form .= '<tr>';
	$form .= '<td>' . $label . '</td>';

	$form .= '<td>' . elgg_view('input/checkbox', array(
				'name' => 'params[policy_object_approval][]',
				'value' => $subtype_id,
				'checked' => in_array($subtype_id, elgg_get_config('policy_object_approval'))
			)) . '</td>';

	$form .= '<td>' . elgg_view('input/checkbox', array(
				'name' => 'params[policy_object_spamcheck][]',
				'value' => $subtype_id,
				'checked' => in_array($subtype_id, elgg_get_config('policy_object_spamcheck'))
			)) . '</td>';

	$form .= '<td>' . elgg_view('input/checkbox', array(
				'name' => 'params[policy_object_editor_can_edit][]',
				'value' => $subtype_id,
				'checked' => in_array($subtype_id, elgg_get_config('policy_object_editor_can_edit'))
			)) . '</td>';

	$form .= '<td>' . elgg_view('input/checkbox', array(
				'name' => 'params[policy_object_editor_can_approve][]',
				'value' => $subtype_id,
				'checked' => in_array($subtype_id, elgg_get_config('policy_object_editor_can_approve'))
			)) . '</td>';

	$form .= '<td>' . elgg_view('input/checkbox', array(
				'name' => 'params[policy_object_supervisor_can_edit][]',
				'value' => $subtype_id,
				'checked' => in_array($subtype_id, elgg_get_config('policy_object_supervisor_can_edit'))
			)) . '</td>';

	$form .= '<td>' . elgg_view('input/checkbox', array(
				'name' => 'params[policy_object_supervisor_can_approve][]',
				'value' => $subtype_id,
				'checked' => in_array($subtype_id, elgg_get_config('policy_object_supervisor_can_approve'))
			)) . '</td>';

	$form .= '</tr>';
}

// Messages

if (!get_subtype_id('object', 'messages')) {
	add_subtype('object', 'messages');
}

$subtype_id = get_subtype_id('object', 'messages');

$form .= '<tr><td colspan="7"><h3 class="center mam">' . elgg_echo('hj:approve:policy:messages') . '</h3></td></tr>';

$form .= '<tr>';
$form .= '<td>' . elgg_echo('hj:approve:policy:message') . '</td>';

$form .= '<td>' . elgg_view('input/checkbox', array(
			'name' => 'params[policy_object_approval][]',
			'value' => $subtype_id,
			'default' => false,
			'checked' => in_array($subtype_id, elgg_get_config('policy_object_approval'))
		)) . '</td>';

$form .= '<td>n/a</td>';

$form .= '<td>n/a</td>';

$form .= '<td>' . elgg_view('input/checkbox', array(
			'name' => 'params[policy_object_editor_can_approve][]',
			'value' => $subtype_id,
			'default' => false,
			'checked' => in_array($subtype_id, elgg_get_config('policy_object_editor_can_approve'))
		)) . '</td>';

$form .= '<td>n/a</td>';

$form .= '<td>' . elgg_view('input/checkbox', array(
			'name' => 'params[policy_object_supervisor_can_approve][]',
			'value' => $subtype_id,
			'default' => false,
			'checked' => in_array($subtype_id, elgg_get_config('policy_object_supervisor_can_approve'))
		)) . '</td>';

$form .= '</tr>';


// Annotations
$form .= '<tr><td colspan="7"><h3 class="center mam">' . elgg_echo('hj:approve:policy:annotations') . '</h3></td></tr>';

$annotation_names = elgg_get_config('policy_annotations');

foreach ($annotation_names as $aname) {

	$form .= '<tr>';
	$form .= '<td>' . elgg_echo("hj:approve:policy:annotation:$aname") . '</td>';

	$form .= '<td>' . elgg_view('input/checkbox', array(
				'name' => 'params[policy_annotation_approval][]',
				'value' => $aname,
				'checked' => in_array($aname, elgg_get_config('policy_annotation_approval'))
			)) . '</td>';

	$form .= '<td>' . elgg_view('input/checkbox', array(
				'name' => 'params[policy_annotation_spamcheck][]',
				'value' => $aname,
				'checked' => in_array($aname, elgg_get_config('policy_annotation_spamcheck'))
			)) . '</td>';

	$form .= '<td>' . elgg_view('input/checkbox', array(
				'name' => 'params[policy_annotation_editor_can_edit][]',
				'value' => $aname,
				'checked' => in_array($aname, elgg_get_config('policy_annotation_editor_can_edit'))
			)) . '</td>';

	$form .= '<td>' . elgg_view('input/checkbox', array(
				'name' => 'params[policy_annotation_editor_can_approve][]',
				'value' => $aname,
				'checked' => in_array($aname, elgg_get_config('policy_annotation_editor_can_approve'))
			)) . '</td>';

	$form .= '<td>' . elgg_view('input/checkbox', array(
				'name' => 'params[policy_annotation_supervisor_can_edit][]',
				'value' => $aname,
				'checked' => in_array($aname, elgg_get_config('policy_annotation_supervisor_can_edit'))
			)) . '</td>';

	$form .= '<td>' . elgg_view('input/checkbox', array(
				'name' => 'params[policy_annotation_supervisor_can_approve][]',
				'value' => $aname,
				'checked' => in_array($aname, elgg_get_config('policy_annotation_supervisor_can_approve'))
			)) . '</td>';

	$form .= '</tr>';
}

$form .= '</tbody>';
$form .= '</table>';


$form .= '<div class="elgg-foot">';
$form .= elgg_view('input/submit', array(
	'value' => elgg_echo('save'),
	'class' => 'mam'
		));
$form .= '</div>';
echo elgg_view('input/form', array(
	'action' => 'action/hypeApprove/settings/save',
	'body' => $form,
));