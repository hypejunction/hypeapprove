<?php

$editors = hj_approve_get_editors();

if ($editors) {
	echo elgg_view_entity_list($editors, array(
		'full_view' => false
	));
} else {
	echo elgg_autop(elgg_echo('hj:approve:editors:none'));
}