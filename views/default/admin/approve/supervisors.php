<?php

$supervisors = hj_approve_get_supervisors();

if ($supervisors) {
	echo '<table class="elgg-table-alt">';
	echo '<tbody>';

	foreach ($supervisors as $supervisor) {
		echo '<tr>';
		echo '<td>' . elgg_view_entity($supervisor, array(
			'full_view' => false
		)) . '</td>';
		echo '<td>' . elgg_view_entity_list(hj_approve_get_supervised_users($supervisor), array(
			'full_view' => false
		)) . '</td>';
		echo '</tr>';
	}

	echo '</tbody>';
	echo '</table>';
} else {
	echo elgg_autop(elgg_echo('hj:approve:supervisors:none'));
}
