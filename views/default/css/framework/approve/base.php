<?php if (FALSE) : ?><style type="text/css"><?php endif; ?>

	<?php
		$path = elgg_get_site_url() . 'mod/hypeApprove/graphics/';
	?>

	.approve-item-wrapper {
		position: relative;
	}
	.approve-item-wrapper i {
		margin-left:10px;
		font-size:11px;
	}
	.approve-labels {
		margin:5px 0 15px 0;
		color: #666;
		font-size: 12px;
	}
	.approve-userinfo.approve-labels {
		margin:0 0 15px 0;
	}
	.approve-labels .approve-label {
		display:inline-block;
		margin: 0 10px 0 0;
		font-weight: bold;
	}
	.approve-labels .approve-info {
		display:block;
		margin:0 0 3px 0;
	}
	.approve-label-type {
		display: inline-block;
		vertical-align: baseline;
		padding: 2px 10px;
		background: #00529f;
		color: white;
		font-size: 11px;
		margin: 0 10px 0 0;
		font-weight: bold;
	}
	.approve-label-group {
		background: green;
	}
	.approve-label-annotation {
		background: orange;
	}
	.approve-label b {
		margin:0 5px;
		font-size:1.1em;
	}

	.approve-actions {
		padding:15px;
		border:1px solid #e8e8e8;
		background:#f4f4f4;
		position:relative;
	}
	.approve-actions input[type="submit"] {
		font-size:12px;
		padding:4px 14px;
		float:right;
		margin:10px;
	}
	.approve-actions-user, .approve-actions-content {
		font-size: 12px;
		margin: 5px 2% 10px;
		width:46%;
		display:inline-block;
		vertical-align:top;
	}
	.approve-actions .elgg-input-checkboxes > li {
		display: inline-block;
		margin: 0 10px 0 0;
	}
	.approve-select-all {
		text-align:right;
	}

	.elgg-module-approve .elgg-head {
		padding:0;
		margin:0;
		border:0;
	}

	.approve-label:before {
		content: "";
		width: 16px;
		height: 20px;
		display: inline-block;
		opacity: .5;
		vertical-align: middle;
		margin: 0 5px;
	}

	.approve-label.approve-label-notvalidated:before,
	.approve-label.approve-label-validate:before,
	.approve-label.approve-label-validate-revoke:before	{
		background: url(<?php echo $path ?>notvalidated.png) no-repeat 50% 50%;
		background-size: 16px;
	}
	.approve-label.approve-label-banned:before {
		background: url(<?php echo $path ?>banned.png) no-repeat 50% 50%;
		background-size: 16px;
	}
	.approve-label.approve-label-disabled:before {
		background: url(<?php echo $path ?>disabled.png) no-repeat 50% 50%;
		background-size: 16px;
	}
	.approve-label.approve-label-failcount:before {
		background: url(<?php echo $path ?>failcount.png) no-repeat 50% 50%;
		background-size: 16px;
	}
	.approve-label.approve-label-exempt:before {
		background: url(<?php echo $path ?>exempt.png) no-repeat 50% 50%;
		background-size: 16px;
	}
	.approve-label.approve-label-whitelisted:before {
		background: url(<?php echo $path ?>whitelisted.png) no-repeat 50% 50%;
		background-size: 16px;
	}
	.approve-label.approve-label-status:before {
		background: url(<?php echo $path ?>pending.png) no-repeat 50% 50%;
		background-size: 16px;
	}
	.approve-label.approve-label-status-approved:before {
		background: url(<?php echo $path ?>approved.png) no-repeat 50% 50%;
		background-size: 16px;
	}
	.approve-label.approve-label-status-rejected:before {
		background: url(<?php echo $path ?>rejected.png) no-repeat 50% 50%;
		background-size: 16px;
	}
	.approve-label.approve-label-status-flagged:before {
		background: url(<?php echo $path ?>flagged.png) no-repeat 50% 50%;
		background-size: 16px;
	}
	.approve-label.approve-label-reason-sitepolicy:before,
	.approve-label.approve-label-exempt-revoke:before {
		background: url(<?php echo $path ?>sitepolicy.png) no-repeat 50% 50%;
		background-size: 16px;
	}
	.approve-label.approve-label-reason-spamcheck:before {
		background: url(<?php echo $path ?>spamcheck.png) no-repeat 50% 50%;
		background-size: 16px;
	}
	.approve-label.approve-label-trigger-system:before {
		background: url(<?php echo $path ?>system.png) no-repeat 50% 50%;
		background-size: 16px;
	}
	.approve-label.approve-label-trigger-userreport:before {
		background: url(<?php echo $path ?>userreport.png) no-repeat 50% 50%;
		background-size: 16px;
	}
	.approve-label.approve-label-trigger-akismet:before,
	.approve-label.approve-label-trigger-whitelist-revoke:before	{
		background: url(<?php echo $path ?>akismet.png) no-repeat 50% 50%;
		background-size: 16px;
	}
	.approve-label.approve-label-trigger-admin:before {
		background: url(<?php echo $path ?>admin.png) no-repeat 50% 50%;
		background-size: 16px;
	}
	.approve-label.approve-label-visible:before {
		background: url(<?php echo $path ?>visible.png) no-repeat 50% 50%;
		background-size: 16px;
	}
	.approve-label.approve-label-history:before {
		background: url(<?php echo $path ?>history.png) no-repeat 50% 50%;
		background-size: 16px;
	}
	.approve-label.approve-label-message:before {
		background: url(<?php echo $path ?>message.png) no-repeat 50% 50%;
		background-size: 16px;
	}
	.approve-label.approve-label-enabled:before {
		background: url(<?php echo $path ?>enabled.png) no-repeat 50% 50%;
		background-size: 16px;
	}

	.approve-history .elgg-list-annotation li {
		padding: 5px;
		border: 0;
		font-size: 11px;
	}
	<?php if (FALSE) : ?></style><?php endif; ?>