<?php

$annotation = elgg_extract('annotation', $vars);

if (!$annotation)
	return;

$time = elgg_view_friendly_time($annotation->time_created);
$owner = get_entity($annotation->owner_guid);
if (elgg_instanceof($owner, 'site')) {
	$link = elgg_echo('hj:approve:system');
} else {
	if (!elgg_instanceof($owner))
		return;
	$link = elgg_view('output/url', array(
		'text' => $owner->name,
		'href' => $owner->getURL()
			));
}

if ($annotation->value) {
	$msg = elgg_echo('hj:approve:annotation:disable', array(
		$link, $annotation->value, $time
			));
	echo "<span class=\"approve-label approve-label-disabled\">";
	echo $msg;
	echo '</span>';
} else {
	$msg = elgg_echo('hj:approve:annotation:enable', array(
		$link, $time
			));
	echo "<span class=\"approve-label approve-label-enabled\">";
	echo $msg;
	echo '</span>';
}