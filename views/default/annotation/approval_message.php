<?php

$annotation = elgg_extract('annotation', $vars);

if (!$annotation)
	return;

$time = elgg_view_friendly_time($annotation->time_created);
$owner = get_entity($annotation->owner_guid);
if (!elgg_instanceof($owner))
	return;

$link = elgg_view('output/url', array(
	'text' => $owner->name,
	'href' => $owner->getURL()
		));

$msg = elgg_echo('hj:approve:annotation:approval_message', array(
	$link, $annotation->value, $time
		));

echo "<span class=\"approve-label approve-label-message\">";
echo $msg;
echo '</span>';