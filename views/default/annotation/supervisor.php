<?php

$annotation = elgg_extract('annotation', $vars);

if (!$annotation)
	return;

$time = elgg_view_friendly_time($annotation->time_created);
$owner = get_entity($annotation->owner_guid);
if (elgg_instanceof($owner, 'site')) {
	$link = elgg_echo('hj:approve:system');
} else {
	if (!elgg_instanceof($owner))
		return;
	$link = elgg_view('output/url', array(
		'text' => $owner->name,
		'href' => $owner->getURL()
			));
}

$supervisor = get_entity($annotation->value);
$supervisor_link = elgg_view('output/url', array(
	'text' => $supervisor->name,
	'href' => $supervisor->getURL()
		));

$msg = elgg_echo('hj:approve:annotation:supervisor', array(
	$link, $supervisor_link, $time
		));

echo $msg;