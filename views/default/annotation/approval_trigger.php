<?php

$annotation = elgg_extract('annotation', $vars);

if (!$annotation)
	return;

$time = elgg_view_friendly_time($annotation->time_created);
$owner = get_entity($annotation->owner_guid);
if (elgg_instanceof($owner, 'site')) {
	$link = elgg_echo('hj:approve:system');
} else {
	if (!elgg_instanceof($owner))
		return;
	$link = elgg_view('output/url', array(
		'text' => $owner->name,
		'href' => $owner->getURL()
			));
}

$msg = elgg_echo('hj:approve:annotation:approval_trigger', array(
	elgg_echo("hj:approve:trigger:$annotation->value"), $link, $time
		));

echo "<span class=\"approve-label approve-label-trigger approve-label-trigger-$annotation->value\">";
echo $msg;
echo '</span>';