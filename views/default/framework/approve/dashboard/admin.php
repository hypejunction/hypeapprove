<?php

if (!elgg_is_admin_logged_in()) {
	return;
}

elgg_load_js('approve.dashboard.js');
elgg_load_css('approve.base.css');

$limit = get_input('limit', 20);
$offset = get_input('offset', 0);

$filter_context = elgg_extract('filter_context', $vars, 'pending');

$options = array(
	'types' => get_input('types', array('object', 'user', 'group')),
	'limit' => $limit,
	'offset' => $offset,
	'order_by' => "e.owner_guid DESC, e.time_created DESC",
	'count' => true
);

switch ($filter_context) {

	case HYPEAPPROVE_STATUS_PENDING :
	case HYPEAPPROVE_STATUS_FLAGGED :
		
		$pending = HYPEAPPROVE_STATUS_PENDING;
		$flagged = HYPEAPPROVE_STATUS_FLAGGED;

		$options['metadata_name_value_pairs'] = array(
			array(
				'name' => 'approval_status',
				'value' => implode(',', array("'$pending'", "'$flagged'")),
				'operand' => 'IN'
			), array(
				'name' => 'annotation_approval_status',
				'value' => null,
				'operand' => 'IS NOT NULL'
			)
		);
		$options['metadata_name_value_pairs_operator'] = 'OR';
		break;

	case HYPEAPPROVE_STATUS_REJECTED :
		$options['metadata_name_value_pairs'] = array('name' => 'approval_status', 'value' => HYPEAPPROVE_STATUS_REJECTED);
		break;

	case HYPEAPPROVE_STATUS_APPROVED :
		$options['metadata_name_value_pairs'] = array('name' => 'approval_status', 'value' => HYPEAPPROVE_STATUS_APPROVED);
		break;

	case 'search' :
		$options = hj_approve_apply_user_search_values($options);
		break;
}


$ha = access_get_show_hidden_status();
access_show_hidden_entities(true);

$count = elgg_get_entities_from_metadata($options);

if ($count) {
	$options['count'] = false;
	$entities = elgg_get_entities_from_metadata($options);

	echo elgg_view('framework/approve/dashboard/review/list', array(
		'dashboard' => $filter_context,
		'entities' => $entities,
		'count' => $count,
		'limit' => $limit,
		'offset' => $offset
	));
} else {
	echo '<div>' . elgg_echo('hj:approve:dashboard:empty') . '</div>';
}

access_show_hidden_entities($ha);
