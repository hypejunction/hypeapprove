<?php

$search_values = get_input('search_values', array());

$body .= '<div>';
$body .= '<label>' . elgg_echo('hj:approve:search:types') . '</label>';
$body .= elgg_view('input/dropdown', array(
	'name' => 'search_values[types]',
	'value' => elgg_extract('types', $search_values, 'user'),
	'options_values' => array(
		'user' => elgg_echo('item:user'),
		'object' => elgg_echo('hj:approve:content'),
		'group' => elgg_echo('item:group'),
	)
		));
$body .= '</div>';

$body .= '<div>';
$body .= '<label>' . elgg_echo('hj:approve:search:keyword') . '</label>';
$body .= elgg_view('input/text', array(
	'name' => 'search_values[keyword]',
	'value' => elgg_extract('keyword', $search_values, '')
		));
$body .= '</div>';

$body .= '<div>';
$body .= '<label>' . elgg_echo('hj:approve:search:status') . '</label>';
$body .= elgg_view('input/checkboxes', array(
	'name' => 'search_values[status]',
	'value' => elgg_extract('status', $search_values),
	'default' => false,
	'options' => array(
		elgg_echo('hj:approve:status:flagged') => HYPEAPPROVE_STATUS_FLAGGED,
		elgg_echo('hj:approve:status:pending') => HYPEAPPROVE_STATUS_PENDING,
		elgg_echo('hj:approve:status:approved') => HYPEAPPROVE_STATUS_APPROVED,
		elgg_echo('hj:approve:status:rejected') => HYPEAPPROVE_STATUS_REJECTED,
	)
		));
$body .= '</div>';

$body .= '<div>';
$body .= '<label>' . elgg_echo('hj:approve:search:reason') . '</label>';
$body .= elgg_view('input/checkboxes', array(
	'name' => 'search_values[reason]',
	'value' => elgg_extract('reason', $search_values),
	'default' => false,
	'options' => array(
		elgg_echo('hj:approve:reason:sitepolicy') => HYPEAPPROVE_REASON_SITEPOLICY,
		elgg_echo('hj:approve:reason:spamcheck') => HYPEAPPROVE_REASON_SPAMCHECK,
	)
		));
$body .= '</div>';

$body .= '<div>';
$body .= '<label>' . elgg_echo('hj:approve:search:trigger') . '</label>';
$body .= elgg_view('input/checkboxes', array(
	'name' => 'search_values[trigger]',
	'value' => elgg_extract('trigger', $search_values),
	'default' => false,
	'options' => array(
		elgg_echo('hj:approve:trigger:system') => HYPEAPPROVE_TRIGGER_SYSTEM,
		elgg_echo('hj:approve:trigger:akismet') => HYPEAPPROVE_TRIGGER_AKISMET,
		elgg_echo('hj:approve:trigger:userreport') => HYPEAPPROVE_TRIGGER_USERREPORT,
		elgg_echo('hj:approve:trigger:admin') => HYPEAPPROVE_TRIGGER_ADMIN,
	)
		));
$body .= '</div>';

$body .= '<div>';
$body .= '<label>' . elgg_echo('hj:approve:search:enabled') . '</label>';
$body .= elgg_view('input/checkboxes', array(
	'name' => 'search_values[enabled]',
	'value' => elgg_extract('enabled', $search_values),
	'default' => false,
	'options' => array(
		elgg_echo('hj:approve:enabled') => 'yes',
		elgg_echo('hj:approve:disabled') => 'no',
	)
		));
$body .= '</div>';

if (elgg_extract('types', $search_values, 'user') != 'user') {
	$extra_user_class = 'hidden';
}
$body .= "<div class=\"extra-options extra-options-users $extra_user_class\">";

$body .= '<div>';
$body .= '<label>' . elgg_echo('hj:approve:search:banned') . '</label>';
$body .= elgg_view('input/checkboxes', array(
	'name' => 'search_values[banned]',
	'value' => elgg_extract('banned', $search_values),
	'default' => false,
	'options' => array(
		elgg_echo('hj:approve:banned') => 'yes',
		elgg_echo('hj:approve:notbanned') => 'no',
	)
		));
$body .= '</div>';

$body .= '<div>';
$body .= '<label>' . elgg_echo('hj:approve:search:validated') . '</label>';
$body .= elgg_view('input/radio', array(
	'name' => 'search_values[validated]',
	'value' => elgg_extract('validated', $search_values),
	'default' => false,
	'options' => array(
		elgg_echo('hj:approve:validated') => 'yes',
		elgg_echo('hj:approve:validated:not') => 'no',
	)
		));
$body .= '</div>';

$body .= '</div>';

if (elgg_extract('types', $search_values, 'user') != 'object') {
	$extra_object_class = 'hidden';
}
$body .= "<div class=\"extra-options extra-options-objects $extra_object_class\">";

$body .= '<div>';
$body .= '<label>' . elgg_echo('hj:approve:search:subtypes') . '</label>';

$registered = elgg_get_config('registered_entities');

foreach ($registered as $type => $subtypes) {
	if ($type == 'object') {
		foreach ($subtypes as $subtype) {
			$id = get_subtype_id($type, $subtype);
			if ($id) {
				$subtype_global_ids[] = $id;
			}
		}
	}
}

$subtype_global_ids[] = get_subtype_id('object', 'messages');

$user = elgg_get_logged_in_user_entity();
if (hj_approve_is_editor($user)) {
	$policy_can_edit = elgg_get_config('policy_object_editor_can_edit');
	$policy_can_approve = elgg_get_config('policy_object_editor_can_approve');

	$subtype_ids = array_unique(array_merge($policy_can_approve, $policy_can_edit));
} else if (hj_approve_is_supervisor($user)) {

	$policy_can_edit = elgg_get_config('policy_object_supervisor_can_edit');
	$policy_can_approve = elgg_get_config('policy_object_supervisor_can_approve');

	$subtype_ids = array_unique(array_merge($policy_can_approve, $policy_can_edit));
} else if (elgg_is_admin_logged_in()) {

	$subtype_ids = $subtype_global_ids;
} else {
	$subtype_ids = array();
}

$subtype_ids = array_intersect($subtype_ids, $subtype_global_ids);

foreach ($subtype_ids as $id) {
	$subtype = get_subtype_from_id($id);
	$subtype_options[elgg_echo("item:object:$subtype")] = $id;
}

$body .= elgg_view('input/checkboxes', array(
	'name' => 'search_values[subtypes]',
	'value' => elgg_extract('subtypes', $search_values),
	'default' => false,
	'options' => $subtype_options
		));
$body .= '</div>';

$body .= '</div>';

$body .= '<div class="elgg-foot">';
$body .= elgg_view('input/submit', array(
	'value' => elgg_echo('search')
		));
$body .= '</div>';

$form = elgg_view('input/form', array(
	'action' => 'approve/dashboard/search',
	'method' => 'GET',
	'disable_security' => true,
	'body' => $body
		));

echo elgg_view_module('filter', elgg_echo('hj:approve:search'), $form);



