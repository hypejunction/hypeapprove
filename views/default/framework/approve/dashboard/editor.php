<?php

$user = elgg_get_logged_in_user_entity();
$is_editor = hj_approve_is_editor($user);

if (!$is_editor) {
	return;
}

elgg_load_js('approve.dashboard.js');
elgg_load_css('approve.base.css');

$limit = get_input('limit', 20);
$offset = get_input('offset', 0);

$filter_context = elgg_extract('filter_context', $vars, 'pending');

$policy_can_edit = elgg_get_config('policy_object_editor_can_edit');
$policy_can_approve = elgg_get_config('policy_object_editor_can_approve');

if (!sizeof($policy_can_edit) && !sizeof($policy_can_approve)) {
	return;
}

$subtype_ids = array_unique(array_merge($policy_can_approve, $policy_can_edit));
$subtype_ids = implode(',', $subtype_ids);

$options = array(
	'types' => get_input('types', array('object', 'user', 'group')),
	'limit' => $limit,
	'offset' => $offset,
	'order_by' => "e.owner_guid DESC, e.time_created DESC",
	'count' => true,
	'wheres' => array("e.subtype IN ($subtype_ids)")
);

switch ($filter_context) {

	case HYPEAPPROVE_STATUS_PENDING :
	case HYPEAPPROVE_STATUS_FLAGGED :

		$pending = HYPEAPPROVE_STATUS_PENDING;
		$flagged = HYPEAPPROVE_STATUS_FLAGGED;

		$options['metadata_name_value_pairs'] = array(
			array(
				'name' => 'approval_status',
				'value' => implode(',', array("'$pending'", "'$flagged'")),
				'operand' => 'IN'
			), array(
				'name' => 'annotation_approval_status',
				'value' => null,
				'operand' => 'NOT NULL'
			)
		);
		$options['metadata_name_value_pairs_operator'] = 'OR';
		break;

	case HYPEAPPROVE_STATUS_REJECTED :
		$options['metadata_name_value_pairs'] = array('name' => 'approval_status', 'value' => HYPEAPPROVE_STATUS_REJECTED);
		break;

	case HYPEAPPROVE_STATUS_APPROVED :
		$options['metadata_name_value_pairs'] = array('name' => 'approval_status', 'value' => HYPEAPPROVE_STATUS_APPROVED);
		break;

	case 'search' :
		$options = hj_approve_apply_user_search_values($options);
		break;
}

$ha = access_get_show_hidden_status();
access_show_hidden_entities(true);

$count = elgg_get_entities_from_metadata($options);

if ($count) {
	$options['count'] = false;
	$entities = elgg_get_entities_from_metadata($options);

	echo elgg_view('framework/approve/dashboard/review/list', array(
		'entities' => $entities,
		'count' => $count,
		'limit' => $limit,
		'offset' => $offset,
		'dashboard' => $filter_context
	));
} else {
	echo '<div>' . elgg_echo('hj:approve:dashboard:empty') . '</div>';
}

access_show_hidden_entities($ha);