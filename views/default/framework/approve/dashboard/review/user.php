<?php

$user = elgg_extract('user', $vars);
if (!elgg_instanceof($user)) {
	return;
}

$entities = elgg_extract('entities', $vars);

$list = '<ul class="elgg-list approve-list-user-entities">';
foreach ($entities as $entity) {
	if (elgg_instanceof($entity)) {
		$list .= '<li>' . elgg_view('framework/approve/dashboard/review/entity', array('entity' => $entity)) . '</li>';
	} else if ($entity instanceof ElggAnnotation) {
		$list .= '<li>' . elgg_view('framework/approve/dashboard/review/annotation', array('annotation' => $entity)) . '</li>';
	}
}
$list .= '</ul>';

$icon = elgg_view_entity_icon($user, 'small');

$userinfo = '<div class="approve-userinfo approve-labels">';
$userinfo .= '<span class="approve-info">' . elgg_echo('name') . ': ' . '<b>' . $user->name . '</b></span>';
$userinfo .= '<span class="approve-info">' . elgg_echo('username') . ': ' . '<b>' . $user->username . '</b></span>';
$userinfo .= '<span class="approve-info">' . elgg_echo('email') . ': ' . '<b>' . $user->email . '</b></span>';
$last_ip = elgg_get_plugin_user_setting('last_ip', $user->guid, 'hypeApprove');
if ($last_ip) {
	$userinfo .= '<span class="approve-info">' . elgg_echo('hj:approve:last_ip') . ': ' . '<b>' . $last_ip . '</b></span>';
}

$supervisors = hj_approve_get_supervisors($user);
if ($supervisors) {
	foreach ($supervisors as $supervisor) {
		$supervisor_links[] = elgg_view('output/url', array(
			'text' => $supervisor->name,
			'href' => (elgg_is_admin_logged_in()) ? "approve/supervisor/$supervisor->guid" : $supervisor->getURL(),
		));
	}
	$userinfo .= '<span class="approve-info">' . elgg_echo('hj:approve:supervisors') . ': ' . implode(', ', $supervisor_links) . '</span>';
}
if (isset($user->approval_status)) {
	$label_class = "approve-label approve-label-status approve-label-status-$user->approval_status";
	$userinfo .= '<span class="' . $label_class . '">' . elgg_echo("hj:approve:status:$user->approval_status") . '</span>';
}
if (isset($user->approval_reason)) {
	$label_class = "approve-label approve-label-reason approve-label-reason-$user->approval_reason";
	$userinfo .= '<span class="' . $label_class . '">' . elgg_echo("hj:approve:reason:$user->approval_reason") . '</span>';
}
if (isset($user->approval_trigger)) {
	$label_class = "approve-label approve-label-trigger approve-label-trigger-$user->approval_trigger";
	$userinfo .= '<span class="' . $label_class . '">' . elgg_echo("hj:approve:trigger:$user->approval_trigger") . '</span>';
}

if (!$user->isEnabled()) {
	$userinfo .= '<span class="approve-label approve-label-disabled">' . elgg_echo('hj:approve:disabled') . ' (' . elgg_echo($user->disable_reason) . ')' . '</span>';
}

if ($user->isBanned()) {
	$userinfo .= '<span class="approve-label approve-label-banned">' . elgg_echo('hj:approve:banned') . ' (' . elgg_echo($user->ban_reason) . ')' . '</span>';
}

if (!elgg_get_user_validation_status($user->guid)) {
	$userinfo .= '<span class="approve-label approve-label-notvalidated">' . elgg_echo('hj:approve:validated:not') . '</span>';
}

$fail_count = elgg_get_plugin_user_setting('akismet_fail_count', $user->guid, 'hypeApprove');
if ($fail_count > 0) {
	$userinfo .= '<span class="approve-label approve-label-failcount">' . elgg_echo('hj:approve:failcount', array($fail_count)) . '</span>';
}

$exempt = hj_approve_queue_is_exempt($user);
if ($exempt) {
	$userinfo .= '<span class="approve-label approve-label-exempt">' . elgg_echo('hj:approve:is_exempt') . '</span>';
}

$whitelist = hj_approve_spamcheck_is_exempt($user);
if ($whitelist) {
	$userinfo .= '<span class="approve-label approve-label-whitelisted">' . elgg_echo('hj:approve:is_whitelisted') . '</span>';
}

$annotations_options = array(
	'guid' => $user->guid,
	'annotation_names' => array('approval_status', 'approval_reason', 'approval_trigger', 'exempt', 'whitelist', 'validate', 'approval_message', 'ban', 'disable'),
	'limit' => 25,
	'order_by' => 'n_table.time_created DESC',
	'pagination' => false,
	'count' => true
);

$annotations_count = elgg_get_annotations($annotations_options);

if ($annotations_count) {
	$userinfo .= '<span class="approve-label approve-label-history">' . elgg_view('output/url', array(
				'text' => elgg_echo('hj:approve:history') . " ($annotations_count)",
				'href' => "#approve-history-$user->guid",
				'rel' => 'toggle'
			)) . '</span>';

	$history .= "<div id=\"approve-history-$user->guid\" class=\"approve-history hidden\">";
	$annotations_options['count'] = false;
	$history .= elgg_list_annotations($annotations_options);
	$history .= '</div>';
}

$userinfo .= '</div>';

$userinfo .= $history;

$checkbox = elgg_view('input/checkbox', array(
	'default' => false,
	'checked' => false,
	'value' => $user->guid,
	'name' => 'user_guids[]'
		));
$userdetails = elgg_view_image_block($icon, $userinfo . $list);

echo elgg_view_image_block('', $userdetails, array(
	'image_alt' => $checkbox
));
