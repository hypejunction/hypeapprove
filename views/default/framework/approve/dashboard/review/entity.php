<?php

$entity = elgg_extract('entity', $vars);

$checkbox = elgg_view('input/checkbox', array(
	'name' => 'content_guids[]',
	'value' => $entity->guid,
	'default' => false,
		));

$created = elgg_view_friendly_time($entity->time_created);

$description .= elgg_view('output/longtext', array(
	'value' => elgg_get_excerpt($entity->description, 1500),
	'class' => 'elgg-subtext',
		));

$link = elgg_view('output/url', array(
	'text' => ($entity->title) ? $entity->title : (($entity->name) ? $entity->name : elgg_echo('hj:approve::notitle')),
	'href' => "approve/review/$entity->guid",
	'target' => '_blank'
		));

$type = $entity->getType();
$subtype = $entity->getSubtype();

if ($subtype) {
	$type_label = elgg_echo("item:$type:$subtype");
	$type_class = "approve-label-$type approve-label-$type-$subtype";
} else {
	$type_label = elgg_echo("item:$type");
	$type_class = "approve-label-$type";
}

if (isset($entity->fromId)) {
	$from = get_entity($entity->fromId);
	$from_str = elgg_echo('messages:from');
	$user_link = elgg_view('output/url', array(
		'text' => $from->name,
		'href' => $from->getURL()
	));

	$label_class = "approve-label approve-label-from";
	$labels .= '<span class="' . $label_class . '">' . $from_str . ': ' . $user_link . '</span>';
}

if (isset($entity->approval_status)) {
	$label_class = "approve-label approve-label-status approve-label-status-$entity->approval_status";
	$labels .= '<span class="' . $label_class . '">' . elgg_echo("hj:approve:status:$entity->approval_status") . '</span>';
}
if (isset($entity->approval_reason)) {
	$label_class = "approve-label approve-label-reason approve-label-reason-$entity->approval_reason";
	$labels .= '<span class="' . $label_class . '">' . elgg_echo("hj:approve:reason:$entity->approval_reason") . '</span>';
}
if (isset($entity->approval_trigger)) {
	$label_class = "approve-label approve-label-trigger approve-label-trigger-$entity->approval_trigger";
	$labels .= '<span class="' . $label_class . '">' . elgg_echo("hj:approve:trigger:$entity->approval_trigger") . '</span>';
}
if (!$entity->isEnabled()) {
	$labels .= '<span class="approve-label approve-label-disabled">' . elgg_echo('hj:approve:disabled') . ' (' . elgg_echo($entity->disable_reason) . ')' . '</span>';
}

if (!is_null($entity->original_access_id)) {
	$labels .= '<span class="approve-label approve-label-visible">' . get_readable_access_level($entity->access_id) . " &#9656; " . get_readable_access_level($entity->original_access_id) . '</span>';
}

$annotations_options = array(
	'guid' => $entity->guid,
	'annotation_names' => array('approval_status', 'approval_reason', 'approval_trigger', 'exempt', 'whitelist', 'validate', 'approval_message', 'ban', 'disable'),
	'limit' => 25,
	'order_by' => 'n_table.time_created DESC',
	'pagination' => false,
	'count' => true
);

$annotations_count = elgg_get_annotations($annotations_options);

if ($annotations_count) {
	$labels .= '<span class="approve-label approve-label-history">' . elgg_view('output/url', array(
				'text' => elgg_echo('hj:approve:history') . " ($annotations_count)",
				'href' => "#approve-history-$entity->guid",
				'rel' => 'toggle'
			)) . '</span>';

	$history .= "<div id=\"approve-history-$entity->guid\" class=\"approve-history hidden\">";
	$annotations_options['count'] = false;
	$history .= elgg_list_annotations($annotations_options);
	$history .= '</div>';
}

$block = <<<___END
	<div class="approve-item-wrapper">
		<span class="approve-label-type $type_class">$type_label</span><strong>$link</strong><i>$created</i>
		<div class="approve-labels">
			$labels
		</div>
		$history
		$description
	</div>
___END;

echo elgg_view_image_block($checkbox, $block);
