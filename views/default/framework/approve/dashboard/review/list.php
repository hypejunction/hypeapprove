<?php

$limit = elgg_extract('limit', $vars);
$offset = elgg_extract('offset', $vars);
$count = elgg_extract('count', $vars);
$entities = elgg_extract('entities', $vars);
$dashboard = elgg_extract('dashboard', $vars);

$pagination = elgg_view('navigation/pagination', array(
	'limit' => $limit,
	'offset' => $offset,
	'count' => $count,
		));

foreach ($entities as $entity) {
	if (!elgg_instanceof($entity, 'user')) {
		$users[$entity->owner_guid][] = $entity;
	} else {
		if (!isset($users[$entity->guid])) {
			$users[$entity->guid] = array();
		}
	}
}

$body = '<ul class="elgg-list approve-list-users">';
foreach ($users as $guid => $user_entities) {
	$user = get_entity($guid);
	if (isset($user->annotation_approval_status)) {
		$annotation_approval_status = unserialize($user->annotation_approval_status);
		foreach ($annotation_approval_status as $id => $status) {
			if ($status == $dashboard || $dashboard == 'search')
				$user_entities[] = elgg_get_annotation_from_id($id);
		}
	} else {
		$annotation_approval_status = array();
	}
	if ($dashboard == HYPEAPPROVE_STATUS_APPROVED) {
		$annotation_approval_trigger = unserialize($user->annotation_approval_trigger);
		foreach ($annotation_approval_trigger as $id => $trigger) {
			if (!array_key_exists($id, $annotation_approval_status)) {
				$user_entities[] = elgg_get_annotation_from_id($id);
			}
		}
	}
	$body .= '<li class="elgg-item approve-user">' . elgg_view('framework/approve/dashboard/review/user', array(
				'user' => $user,
				'entities' => $user_entities
			)) . '</li>';
}
$body .= '</ul>';


$toggle = '<div class="approve-select-all">';
$toggle .= '<label>' . elgg_view('input/checkbox', array(
			'default' => false,
			'value' => false,
			'id' => 'approve-select-all-items'
		)) . elgg_echo('hj:approve:bulk:select') . '</label>';
$toggle .= '</div>';

$body = $toggle . $body . $toggle;

$actions .= '<div class="approve-actions clearfix">';

$actions_options = array(elgg_echo('hj:approve:bulk:select:action'));

if (hj_approve_can_approve(elgg_get_logged_in_user_entity(), null, 'user')) {
	$actions_options_approve = array(
		'approve/user/exempt' => elgg_echo('hj:approve:bulk:user:exempt'),
		'approve/user/exempt_revoke' => elgg_echo('hj:approve:bulk:user:exempt_revoke'),
		'approve/user/whitelist' => elgg_echo('hj:approve:bulk:user:whitelist'),
		'approve/user/whitelist_revoke' => elgg_echo('hj:approve:bulk:user:whitelist_revoke'),
		'approve/user/set_status' => elgg_echo('hj:approve:bulk:user:set_status'),
	);

	$actions_options = array_merge($actions_options, $actions_options_approve);
}

if (hj_approve_can_edit(elgg_get_logged_in_user_entity(), null, 'user')) {
	$actions_options_edit = array(
		'approve/supervisor/assign' => elgg_echo('hj:approve:bulk:user:assign_supervisor'),
		'approve/user/validate' => elgg_echo('hj:approve:bulk:user:validate'),
		'approve/user/ban' => elgg_echo('hj:approve:bulk:user:ban'),
		'approve/user/unban' => elgg_echo('hj:approve:bulk:user:unban'),
		'approve/user/disable' => elgg_echo('hj:approve:bulk:user:disable'),
		'approve/user/enable' => elgg_echo('hj:approve:bulk:user:enable'),
		'approve/user/delete' => elgg_echo('hj:approve:bulk:user:delete'),
	);
	$actions_options = array_merge($actions_options, $actions_options_edit);
}

if (count($actions_options) > 1) {
	$actions .= '<div class="approve-actions-user">';
	$actions .= '<label>' . elgg_echo('hj:approve:bulk:selected:user') . '</label>';
	$actions .= elgg_view('input/dropdown', array(
		'name' => 'user_action',
		'options_values' => $actions_options
	));

	$actions .= '<div class="extras-user-action hidden">';

	$actions .= '<div class="extras-user-action-status mbs mts hidden">';
	$actions .= elgg_view('input/dropdown', array(
		'name' => 'status_users',
		'options_values' => array(
			0 => elgg_echo('hj:approve:bulk:select:status'),
			HYPEAPPROVE_STATUS_FLAGGED => elgg_echo('hj:approve:status:flagged'),
			HYPEAPPROVE_STATUS_PENDING => elgg_echo('hj:approve:status:pending'),
			HYPEAPPROVE_STATUS_APPROVED => elgg_echo('hj:approve:status:approved'),
			HYPEAPPROVE_STATUS_REJECTED => elgg_echo('hj:approve:status:rejected')
		)
	));
	$actions .= '</div>';

	$actions .= '<div class="extras-user-action-supervisor mbs mts hidden">';
	$actions .= '<label>' . elgg_echo('hj:approve:bulk:select:supervisor') . '</label>';
	$actions .= elgg_view('input/userpicker');
	$actions .= '</div>';

	$actions .= '<div class="mbs mts">';
	$actions .= '<label>' . elgg_view('input/checkbox', array(
				'name' => 'notify_users',
				'value' => true,
				'checked' => false,
				'default' => false
			)) . elgg_echo('hj:approve:bulk:notify_users') . '</label>';
	$actions .= '</div>';

	$actions .= '<div class="mbs mts">';
	$actions .= '<label>' . elgg_echo('hj:approve:bulk:message') . '</label>';
	$actions .= elgg_view('input/text', array(
		'name' => 'notify_users_message',
	));
	$actions .= '</div>';

	$actions .= '<div class="mbs mts">';
	$actions .= '<label>' . elgg_echo('hj:approve:bulk:approval_message') . '</label>';
	$actions .= elgg_view('input/text', array(
		'name' => 'approval_message',
	));
	$actions .= '</div>';
	$actions .= '</div>';
	$actions .= '</div>';
}

$actions_options = array(
	0 => elgg_echo('hj:approve:bulk:select:action'),
	'approve/content/set_status' => elgg_echo('hj:approve:bulk:content:set_status'),
	'approve/content/disable' => elgg_echo('hj:approve:bulk:content:disable'),
	'approve/content/enable' => elgg_echo('hj:approve:bulk:content:enable'),
	'approve/content/delete' => elgg_echo('hj:approve:bulk:content:delete'),
);


$actions .= '<div class="approve-actions-content">';
$actions .= '<label>' . elgg_echo('hj:approve:bulk:selected:content') . '</label>';
$actions .= elgg_view('input/dropdown', array(
	'name' => 'content_action',
	'options_values' => $actions_options
		));

$actions .= '<div class="extras-content-action hidden">';

$actions .= '<div class="extras-content-action-status mbs mts hidden">';
$actions .= elgg_view('input/dropdown', array(
	'name' => 'status_content',
	'options_values' => array(
		0 => elgg_echo('hj:approve:bulk:select:status'),
		HYPEAPPROVE_STATUS_FLAGGED => elgg_echo('hj:approve:status:flagged'),
		HYPEAPPROVE_STATUS_PENDING => elgg_echo('hj:approve:status:pending'),
		HYPEAPPROVE_STATUS_APPROVED => elgg_echo('hj:approve:status:approved'),
		HYPEAPPROVE_STATUS_REJECTED => elgg_echo('hj:approve:status:rejected')
	)
		));
$actions .= '</div>';

$actions .= '<div class="mbs mts">';
$actions .= '<label>' . elgg_view('input/checkbox', array(
			'name' => 'notify_owners',
			'value' => true,
			'checked' => false,
			'default' => false
		)) . elgg_echo('hj:approve:bulk:notify_owners') . '</label>';
$actions .= '</div>';

$actions .= '<div class="mbs mts">';
$actions .= '<label>' . elgg_echo('hj:approve:bulk:message') . '</label>';
$actions .= elgg_view('input/text', array(
	'name' => 'notify_owners_message',
		));
$actions .= '</div>';

$actions .= '<div class="mbs mts">';
$actions .= '<label>' . elgg_echo('hj:approve:bulk:approval_message') . '</label>';
$actions .= elgg_view('input/text', array(
	'name' => 'content_approval_message',
		));
$actions .= '</div>';
$actions .= '</div>';
$actions .= '</div>';

$actions .= elgg_view('input/submit', array(
	'value' => elgg_echo('save')
		));

$actions .= '</div>';

echo elgg_view('input/form', array(
	'id' => 'approve-list',
	'action' => 'action/approve/bulk',
	'body' => elgg_view_module('approve', '', $body, array(
		'footer' => $actions
	))
));

echo $pagination;