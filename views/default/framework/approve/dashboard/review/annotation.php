<?php

$annotation = elgg_extract('annotation', $vars);

$checkbox = elgg_view('input/checkbox', array(
	'name' => 'annotation_ids[]',
	'value' => $annotation->id,
	'default' => false,
		));

$created = elgg_view_friendly_time($annotation->time_created);

$description .= elgg_view('output/longtext', array(
	'value' => elgg_get_excerpt($annotation->value, 1500),
	'class' => 'elgg-subtext',
		));

$link = elgg_view('output/url', array(
	'text' => elgg_echo('hj:approve::notitle'),
	'href' => "approve/review/annotation/$annotation->id",
	'target' => '_blank'
		));


$type_label = elgg_echo("hj:approve:policy:annotation:$annotation->name");
$type_class = "approve-label-annotation approve-label-annotation-$annotation->name";

$entity = $annotation->getEntity();
$owner = $annotation->getOwnerEntity();

$approval_status = (isset($owner->annotation_approval_status)) ? unserialize($owner->annotation_approval_status) : array();
$current_status = elgg_extract($annotation->id, $approval_status, null);

$approval_reason = (isset($owner->annotation_approval_reason)) ? unserialize($owner->annotation_approval_reason) : array();
$current_reason = elgg_extract($annotation->id, $approval_reason, null);

$approval_trigger = (isset($owner->annotation_approval_trigger)) ? unserialize($owner->annotation_approval_trigger) : array();
$current_trigger = elgg_extract($annotation->id, $approval_trigger, null);

$original_access = (isset($owner->annotation_original_access)) ? unserialize($owner->annotation_original_access) : array();
$original_access_id = elgg_extract($annotation->id, $original_access, false);


if (isset($current_status)) {
	$label_class = "approve-label approve-label-status approve-label-status-$current_status";
	$labels .= '<span class="' . $label_class . '">' . elgg_echo("hj:approve:status:$current_status") . '</span>';
}
if (isset($current_reason)) {
	$label_class = "approve-label approve-label-reason approve-label-reason-$current_reason";
	$labels .= '<span class="' . $label_class . '">' . elgg_echo("hj:approve:reason:$current_reason") . '</span>';
}
if (isset($current_trigger)) {
	$label_class = "approve-label approve-label-trigger approve-label-trigger-$current_trigger";
	$labels .= '<span class="' . $label_class . '">' . elgg_echo("hj:approve:trigger:$current_trigger") . '</span>';
}
if ($annotation->enabled == 'no') {
	$labels .= '<span class="approve-label approve-label-disabled">' . elgg_echo('hj:approve:disabled') . '</span>';
}

if ($original_access_id !== false) {
	$labels .= '<span class="approve-label approve-label-visible">' . get_readable_access_level($annotation->access_id) . " &#9656; " . get_readable_access_level($original_access_id) . '</span>';
}

$block = <<<___END
	<div class="approve-item-wrapper">
		<span class="approve-label-type $type_class">$type_label</span><strong>$link</strong><i>$created</i>
		<div class="approve-labels">
			$labels
		</div>
		$description
	</div>
___END;

echo elgg_view_image_block($checkbox, $block);
