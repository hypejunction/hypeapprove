<?php

$filter_context = elgg_extract('filter_context', $vars, 'pending');

$tabs = array(
	HYPEAPPROVE_STATUS_PENDING => array(
		'text' => elgg_echo('hj:approve:dashboard:pending'),
		'href' => 'approve/dashboard/pending',
		'selected' => ($filter_context == 'pending'),
		'priority' => 100,
	),
	HYPEAPPROVE_STATUS_REJECTED => array(
		'text' => elgg_echo('hj:approve:dashboard:rejected'),
		'href' => 'approve/dashboard/rejected',
		'selected' => ($filter_context == 'rejected'),
		'priority' => 200,
	),
	HYPEAPPROVE_STATUS_APPROVED => array(
		'text' => elgg_echo('hj:approve:dashboard:approved'),
		'href' => 'approve/dashboard/approved',
		'selected' => ($filter_context == 'approved'),
		'priority' => 300,
	),
);

if ($filter_context == 'search') {
	$tabs['search'] = array(
		'text' => elgg_echo('hj:approve:dashboard:search'),
		'href' => 'approve/dashboard/search',
		'selected' => true,
		'priority' => 400,
	);
}

foreach ($tabs as $name => $tab) {
	if ($tab) {
		$tab['name'] = $name;
		elgg_register_menu_item('filter', $tab);
	}
}

echo elgg_view_menu('filter', array('sort_by' => 'priority', 'class' => 'elgg-menu-hz'));
