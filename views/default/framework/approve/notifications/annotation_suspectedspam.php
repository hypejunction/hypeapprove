<?php

$annotation = elgg_extract('annotation', $vars);

if (!($annotation instanceof ElggAnnotation)) {
	return true;
}

$owner = get_entity($annotation->owner_guid);

$head = elgg_echo('hj:approve:suspectedspam:email:head', array(
	$owner->name, $owner->username, elgg_echo("hj:approve:policy:annotation:$annotation->name")
));

$body = "<blockquote>" . $annotation->value . "</blockquote>";
$footer = elgg_echo('hj:approve:suspectedspam:email:footer', array(
	elgg_normalize_url("approve/review/annotation/$annotation->id")
));

echo elgg_view('output/longtext', array(
	'value' => elgg_view_module('message', $head, $body, array('footer' => $footer))
));
