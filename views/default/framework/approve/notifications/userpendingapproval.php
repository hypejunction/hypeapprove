<?php

$entity = elgg_extract('entity', $vars);

if (!elgg_instanceof($entity, 'user')) {
	return true;
}

$head = elgg_echo('hj:approve:userpendingapproval:email:head');

$body = elgg_view('export/entity', array('entity' => $entity));

$footer = elgg_echo('hj:approve:userpendingapproval:email:footer', array(
	elgg_normalize_url("approve/review/$entity->guid")
));

echo elgg_view('output/longtext', array(
	'value' => elgg_view_module('message', $head, $body, array('footer' => $footer))
));
