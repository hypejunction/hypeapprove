<?php

$entity = elgg_extract('entity', $vars);

if (!elgg_instanceof($entity)) {
	return true;
}

$owner = $entity->getOwnerEntity();

$head = elgg_echo('hj:approve:suspectedspam:email:head', array(
	$owner->name, $owner->username, $entity->title
		));

if (HYPEAPPROVE_NOTIF_INCL_EXPORT) {
	$body = '<blockquote>';
	$body .= elgg_view('export/entity', array(
		'entity' => $entity,
	));
	$body .= '</blockquote>';
} else {
	$body = "<blockquote>" . $entity->description . "</blockquote>";
}

$footer = elgg_echo('hj:approve:suspectedspam:email:footer', array(
	elgg_normalize_url("approve/review/$entity->guid")
		));

echo elgg_view('output/longtext', array(
	'value' => elgg_view_module('message', $head, $body, array('footer' => $footer))
));
