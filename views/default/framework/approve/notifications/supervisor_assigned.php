<?php

$entity = elgg_extract('entity', $vars);

if (!elgg_instanceof($entity)) {
	return;
}

$setter = elgg_extract('setter', $vars);
$supervisor = elgg_extract('supervisor', $vars);
$note = elgg_extract('note', $vars);

$head = elgg_echo('hj:approve:supervisor_assigned:email:head', array(
	$setter->name, $supervisor->name
));

if ($note) {
	$body = elgg_echo('hj:approve:supervisor_assigned:email:note');
	$body .= "<blockquote>" . $note . "</blockquote>";
}

$footer = elgg_echo('hj:approve:supervisor_assigned:email:footer', array(
	$supervisor->name, elgg_normalize_url("messages/compose?send_to=$supervisor->guid")
));

echo elgg_view('output/longtext', array(
	'value' => elgg_view_module('message', $head, $body, array('footer' => $footer))
));
