<?php

$entity = elgg_extract('entity', $vars);

if (!elgg_instanceof($entity)) {
	return;
}

$setter = elgg_extract('setter', $vars);
$previous_status = elgg_extract('previous_status', $vars);
$status = elgg_extract('status', $vars);
$note = elgg_extract('note', $vars);

$link = elgg_view('output/url', array(
	'text' => ($entity->title) ? $entity->title : (($entity->name) ? $entity->name : elgg_echo('hj:approve::notitle')),
	'href' => $entity->getURL(),
	'target' => '_blank'
		));

if ($previous_status) {
	$head = elgg_echo('hj:approve:contentapprovalstatus:email:head', array(
		$setter->name, $link, elgg_echo("hj:approve:status:$previous_status"), elgg_echo("hj:approve:status:$status")
	));
} else {
	$head = elgg_echo('hj:approve:contentapprovalstatusnoprev:email:head', array(
		$setter->name, $link, elgg_echo("hj:approve:status:$status")
	));
}

if ($note) {
	$body = elgg_echo('hj:approve:contentapprovalstatus:email:note');
	$body .= "<blockquote>" . $note . "</blockquote>";
}

$footer = elgg_echo('hj:approve:contentapprovalstatus:email:footer', array(
	$setter->name, elgg_normalize_url("messages/compose?send_to=$setter->guid")
));

echo elgg_view('output/longtext', array(
	'value' => elgg_view_module('message', $head, $body, array('footer' => $footer))
));
