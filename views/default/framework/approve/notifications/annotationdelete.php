<?php

$annotation = elgg_extract('annotation', $vars);

if (!($annotation instanceof ElggAnnotation)) {
	return;
}

$setter = elgg_extract('setter', $vars);
$note = elgg_extract('note', $vars);

$title = elgg_echo("hj:approve:annotaiton:$annotation->name");

$head = elgg_echo('hj:approve:content:delete:email:head', array(
	$setter->name, $title
));

if ($note) {
	$body = elgg_echo('hj:approve:content:delete:email:note');
	$body .= "<blockquote>" . $note . "</blockquote>";
}

$footer = elgg_echo('hj:approve:content:delete:email:footer', array(
	$setter->name, elgg_normalize_url("messages/compose?send_to=$setter->guid")
));

echo elgg_view('output/longtext', array(
	'value' => elgg_view_module('message', $head, $body, array('footer' => $footer))
));
