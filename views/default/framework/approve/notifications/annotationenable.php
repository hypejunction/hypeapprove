<?php

$annotation = elgg_extract('annotation', $vars);

if (!($annotation instanceof ElggAnnotation)) {
	return;
}

$setter = elgg_extract('setter', $vars);
$note = elgg_extract('note', $vars);

$title = elgg_echo("hj:approve:annotaiton:$annotation->name");
$link = elgg_view('output/url', array(
	'text' => $title,
	'href' => $annotation->getURL(),
	'target' => '_blank'
		));

$head = elgg_echo('hj:approve:content:enable:email:head', array(
	$setter->name, $link
));

if ($note) {
	$body = elgg_echo('hj:approve:content:enable:email:note');
	$body .= "<blockquote>" . $note . "</blockquote>";
}

$footer = elgg_echo('hj:approve:content:enable:email:footer', array(
	$setter->name, elgg_normalize_url("messages/compose?send_to=$setter->guid")
));

echo elgg_view('output/longtext', array(
	'value' => elgg_view_module('message', $head, $body, array('footer' => $footer))
));
