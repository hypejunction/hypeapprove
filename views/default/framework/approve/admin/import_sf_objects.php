<?php

elgg_load_js('approve.admin.js');

$title = elgg_echo('hj:approve:admin:import:sf_objects');

$body = '<p class="mam">' . elgg_echo('hj:approve:admin:import_stats', array($vars['count'])) . '</p>';
$body .= '<strong class="mam">' . elgg_echo('hj:approve:admin:import_warning') . '</strong>';
$body .= elgg_view('output/url', array(
	'id' => 'hj-approve-admin-import-sf-objects',
	'text' => elgg_echo('hj:approve:admin:import_start'),
	'class' => 'elgg-button elgg-button-action float mam',
	'rel' => elgg_echo('hj:approve:admin:import_confirmation'),
	'data-count' => $vars['count']
));
$body .= '<div id="import-sf-objects-progress" class="mam"></div>';

echo elgg_view_module('widget', $title, $body);