<?php

if (!HYPEAPPROVE_AKISMET_KEY) {
	return;
}

elgg_load_js('approve.admin.js');

$title = elgg_echo('hj:approve:admin:scan:spam');

$body = '<p class="mam">' . elgg_echo('hj:approve:admin:scan_stats', array($vars['count'])) . '</p>';
$body .= elgg_view('output/url', array(
	'id' => 'hj-approve-admin-scan-spam',
	'text' => elgg_echo('hj:approve:admin:scan_start'),
	'class' => 'elgg-button elgg-button-action float mam',
	'data-count' => $vars['count']
));
$body .= '<div id="scan-spam-progress" class="mam"></div>';

echo elgg_view_module('widget', $title, $body);