<?php

/* hypeApprove
 *
 * Content Approval and Moderation Tools for Elgg
 * @package hypeJunction
 * @subpackage hypeApprove
 *
 * @author Ismayil Khayredinov <ismayil.khayredinov@gmail.com>
 * @copyright Copyright (c) 2011-2013, Ismayil Khayredinov
 */

define('HYPEAPPROVE_RELEASE', 1372771756);

define('HYPEAPPROVE_STATUS_REJECTED', 'rejected');
define('HYPEAPPROVE_STATUS_PENDING', 'pending');
define('HYPEAPPROVE_STATUS_APPROVED', 'approved');
define('HYPEAPPROVE_STATUS_FLAGGED', 'flagged');
define('HYPEAPPROVE_STATUS_EXEMPT', 'exempt');

define('HYPEAPPROVE_REASON_SPAMCHECK', 'spamcheck');
define('HYPEAPPROVE_REASON_SITEPOLICY', 'sitepolicy');

define('HYPEAPPROVE_TRIGGER_SYSTEM', 'system');
define('HYPEAPPROVE_TRIGGER_AKISMET', 'akismet');
define('HYPEAPPROVE_TRIGGER_USERREPORT', 'userreport');
define('HYPEAPPROVE_TRIGGER_ADMIN', 'admin');

define('HYPEAPPROVE_SPAM_REPORTING', elgg_get_plugin_setting('spam_reporting', 'hypeApprove'));

define('HYPEAPPROVE_AKISMET_KEY', elgg_get_plugin_setting('akismet_key', 'hypeApprove'));
define('HYPEAPPROVE_AKISMET_FAIL_COUNT', elgg_get_plugin_setting('akismet_fail_count', 'hypeApprove'));
define('HYPEAPPROVE_AKISMET_FAIL_COUNT_ACTION', elgg_get_plugin_setting('akismet_fail_count_action', 'hypeApprove'));
define('HYPEAPPROVE_STOPFORUMSPAM_KEY', elgg_get_plugin_setting('stopforumspam_key', 'hypeApprove'));

define('HYPEAPPROVE_NOTIF_INCL_EXPORT', elgg_get_plugin_setting('notification_include_export', 'hypeApprove'));

define('ACCESS_SUPERVISORS', -5);
define('ACCESS_EDITORS', -7);
define('ACCESS_APPROVAL_WORKFLOW', -9);

elgg_register_event_handler('init', 'system', 'hj_approve_init');

function hj_approve_init() {

	$path = elgg_get_plugins_path() . 'hypeApprove/';

	elgg_register_library('approve:vendor:akismet', $path . 'vendors/akismet/Akismet.class.php');
	elgg_register_library('approve:vendor:stopforumspam', $path . 'vendors/stopforumspam/stopforumspam.php');

	$libraries = array(
		'base',
		'events',
		'assets',
		'page_handlers',
		'actions',
		'views',
		'menus',
		'hooks'
	);

	foreach ($libraries as $lib) {
		$libpath = "{$path}lib/{$lib}.php";
		if (file_exists($libpath)) {
			elgg_register_library("approve:library:$lib", $libpath);
			elgg_load_library("approve:library:$lib");
		}
	}

}
