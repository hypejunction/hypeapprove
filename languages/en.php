<?php

$english = array(

	'noaccess' => 'You need to login to view this content or the content has been removed or the content is pending approval or you do not have permission to view it.',
	
	'menu:page:header:approve' => 'Content Moderation',
	'admin:approve' => 'Content Moderation',
	'admin:approve:policy' => 'Moderation Policies',

	'admin:approve:notifications' => 'Notification Settings',
	'admin:approve:spam' => 'Spam Protection',
	'admin:approve:restrictions' => 'User Restrictions',
	'admin:approve:editors' => 'Editors',
	'admin:approve:supervisors' => 'Supervisors',

	'hj:approve:admin:policy' => 'Policy',
	'hj:approve:admin:spam' => 'Spam Protection',
	'hj:approve:admin:restrictions' => 'User Restrictions',
	'hj:approve:admin:notifications' => 'Notification Settings',
	'hj:approve:admin:editors' => 'Editors',
	'hj:approve:admin:supervisors' => 'Supervisors',

	'hj:approve:editor' => 'Editor',
	'hj:approve:editors' => 'Editors',
	'hj:approve:supervisor' => 'Supervisor',
	'hj:approve:supervisors' => 'Supervisors',

	'hj:approve:editors:none' => 'You have not yet assigned any editors',
	'hj:approve:supervisors:none' => 'You have not yet assigned any supervisors',
	
	'hj:approve:user:restrictions' => 'Restrictions',
	'hj:approve:user:restrictions:intro' => 'To restrict user access to listed actions, pages and events,
		check to boxes corresponding to the user\'s approval status. E.g. to prevent rejected users from loggin in,
		check to box in the Rejected column in the \'login\',\'user\' event row.',

	'hj:approve:user:restrictions:actions' => 'Actions',
	'hj:approve:user:restrictions:pages' => 'Page Handlers',
	'hj:approve:user:restrictions:events' => 'Events',

	'hj:approve:user:restrictions:action' => 'Sorry, your account is under review and your ability to perform this operation has been restricted',
	'hj:approve:user:restrictions:event' => 'Sorry, your account is under review and your ability to perform this operation has been restricted',
	'hj:approve:user:restrictions:page' => 'Sorry, your account is under review and your ability to view this page has been restricted',

	'hj:approve:noregisteredentities' => 'There are no content types registered on your site',

	'hj:approve:policy:users' => 'User entity policies',
	'hj:approve:policy:groups' => 'Group entity policies',
	'hj:approve:policy:objects' => 'Object entity policies',
	'hj:approve:policy:messages' => 'Messaging policies',
	'hj:approve:policy:message' => 'User-to-user messages',
	'hj:approve:policy:annotations' => 'Annotation policies',

	'hj:approve:policy:requires_approval' => 'Queue for manual approval',
	'hj:approve:policy:requires_spamcheck' => 'Perform a spam check',

	'hj:approve:policy:editor_can_edit' => 'Editors can edit, disable and delete',
	'hj:approve:policy:editor_can_approve' => 'Editors can approve and reject',
	
	'hj:approve:policy:supervisor_can_edit' => 'Supervisors can edit, disable and delete',
	'hj:approve:policy:supervisor_can_approve' => 'Supervisors can approve and reject',

	'hj:approve:admin:spam:section:akismet' => 'Akismet Settings',
	'hj:approve:admin:spam:info:akismet' => 'Akismet is a hosted web service that saves you time by automatically detecting comment and trackback spam. You can register and obtain a key at http://akismet.com/',
	'hj:approve:admin:spam:akismet_key' => 'Akismet Key',
	'hj:approve:admin:spam:akismet_key_notice' => 'You have configured hypeApprove to check content against spam. Please add your Akismet key for this feature to actually work',
	'hj:approve:admin:spam:akismet_fail_count' => 'Number of items flagged by AKISMET as spam before the user is automatically banned, disabled or deleted',
	'hj:approve:admin:spam:akismet_fail_count_action' => 'An action to perform on the user account when the above limit is reached',

	'hj:approve:admin:spam:section:reporting' => 'Spam Reporting',
	'hj:approve:admin:spam:spam_reporting' => 'Allow users to report spam',
	
	'hj:approve:admin:spam:section:stopforumsspam' => 'Stop Forum Spam Settings',
	'hj:approve:admin:spam:info:stopforumspam' => 'Stop Forum Spam is a database of known spammer emails, usernames and domains. You can reduce the number of bogus registrations by checking the corresponding box on the Policy page and entering your key here. A key can be obtained from http://stopforumspam.com',
	'hj:approve:admin:spam:stopforumspam_key_notice' => 'You have configured hypeApprove to check new user registrations against spam. Please add your StopForumSpam key for this feature to actually work',
	'hj:approve:admin:spam:stopforumspam_key' => 'Stop Forum Spam Key',
	
	'hj:approve:admin:import:sf_objects' => 'Import hypeSpamFighter object data',
	'hj:approve:admin:import:sf_users' => 'Import hypeSpamFighter user data',
	'hj:approve:admin:import:ha_users' => 'Import hypeApprove 1.7 user data',
	'hj:approve:admin:import_stats' => 'Items to import: %s',
	'hj:approve:admin:import_warning' => 'This operation is irreversible',
	'hj:approve:admin:import_start' => 'Start import',
	'hj:approve:admin:import_confirmation' => 'This operation is irreversible. Are you sure you want to continue?',
	'hj:approve:admin:import_complete' => 'Import is complete. Refreshing the page...',

	'hj:approve:admin:scan:spam' => 'Scan site content for spam',
	'hj:approve:admin:scan_stats' => '%s items were not previously scanned for spam',
	'hj:approve:admin:scan_start' => 'Scan now',
	'hj:approve:admin:scan_complete' => 'Scan complete',
	
	'hj:approve:register:stopforumspam:spamemail' => 'Unfotunately, you can not use this email address to register on our site. It has been recorded as a spam source in the StopForumSpam database',
	'hj:approve:register:stopforumspam:spamusername' => 'Unfotunately, you can not use this username to register on our site. It has been recorded as a spam source in the StopForumSpam database',

	'hj:approve:markedasspam' => 'Item has been identified as spam and will not be visible on the site until reviewed and approved by our staff',

	'hj:approve:suspectedspam:source:default' => 'system flag',
	'hj:approve:suspectedspam:source:akismet' => 'Akismet',
	'hj:approve:suspectedspam:source:userreport' => 'user report',
	
	'hj:approve:suspectedspam:email:subject' => 'New suspected spam (%s)',
	'hj:approve:suspectedspam:email:head' => '%s (%s) added a new item %s suspected to be spam.',
	'hj:approve:suspectedspam:email:footer' => 'The items was flagged for further review. You can review and approve this item here: %s',

	'hj:approve:pendingapproval' => 'Item is pending approval. It will appear on the site once it has been reviewed and approved by our staff.',
	'hj:approve:userpendingapproval' => 'Account is pending approval. Your ability to perform certain actions may be limited until approved',
	'hj:approve:userflagged' => 'Account is under review for possible violation of site policies. Your ability to perform certain actions may be limited',
	'hj:approve:userrejected' => 'Your profile has been reviewed and rejected. Your ability to perform certain actions may be limited.',

	'hj:approve:pendingapproval:email:subject' => 'New item pending approval',
	'hj:approve:pendingapproval:email:head' => '%s (%s) added a new item %s that is pending approval.',
	'hj:approve:pendingapproval:email:footer' => 'The item can be reviewed here: %s',

	'hj:approve:userpendingapproval:email:subject' => 'New user account pending approval',
	'hj:approve:userpendingapproval:email:head' => 'An account with the following details has been registered and is pending approval. The account may not yet be enabled if its pending email validation et al',
	'hj:approve:userpendingapproval:email:footer' => 'The details of the registration can be reviewed here: %s',

	'hj:approve:userapprovalstatus:email:subject' => 'Account approval status update',
	'hj:approve:userapprovalstatus:email:head' => '%s updated the status of your account from <b>%s</b> to <b>%s</b>',
	'hj:approve:userapprovalstatusnoprev:email:head' => '%s updated the status of your account to <b>%s</b>',
	'hj:approve:userapprovalstatus:email:note' => 'The following note was included for your information:',
	'hj:approve:userapprovalstatus:email:footer' => 'You can contact %s at %s',

	'hj:approve:contentapprovalstatus:email:subject' => 'Content approval status update',
	'hj:approve:contentapprovalstatus:email:head' => '%s updated the status of %s from <b>%s</b> to <b>%s</b>',
	'hj:approve:contentapprovalstatusnoprev:email:head' => '%s updated the status of %s to <b>%s</b>',
	'hj:approve:contentapprovalstatus:email:note' => 'The following note was included for your information:',
	'hj:approve:contentapprovalstatus:email:footer' => 'You can contact %s at %s',

	'hj:approve:exempt:email:subject' => 'Account exempt from approval workflow',
	'hj:approve:exempt:email:head' => '%s updated your account status and the content you post will no longer be queued for manual approval',
	'hj:approve:exempt:email:note' => 'The following note was included for your information:',
	'hj:approve:exempt:email:footer' => 'You can contact %s at %s',

	'hj:approve:exempt_revoke:email:subject' => 'Exemption from approval workflow revoked',
	'hj:approve:exempt_revoke:email:head' => '%s updated your account status and the content you post will now be queued for manual approval',
	'hj:approve:exempt_revoke:email:note' => 'The following note was included for your information:',
	'hj:approve:exempt_revoke:email:footer' => 'You can contact %s at %s',

	'hj:approve:ban:email:subject' => 'Your account was suspended',
	'hj:approve:ban:email:head' => '%s suspended your account',
	'hj:approve:ban:email:note' => 'The following note was included for your information:',
	'hj:approve:ban:email:footer' => 'You can contact site administrator at %s',

	'hj:approve:unban:email:subject' => 'Your account is now active',
	'hj:approve:unban:email:head' => '%s lifted the suspension of your account',
	'hj:approve:unban:email:note' => 'The following note was included for your information:',
	'hj:approve:unban:email:footer' => 'You can contact %s at %s',

	'hj:approve:disable:email:subject' => 'Your account has been deactivated',
	'hj:approve:disable:email:head' => '%s has deactivated your account',
	'hj:approve:disable:email:note' => 'The following note was included for your information:',
	'hj:approve:disable:email:footer' => 'You can contact site administrator at %s',

	'hj:approve:enable:email:subject' => 'Your account has been activated',
	'hj:approve:enable:email:head' => '%s has activated your account',
	'hj:approve:enable:email:note' => 'The following note was included for your information:',
	'hj:approve:enable:email:footer' => 'You can contact %s at %s',

	'hj:approve:delete:email:subject' => 'Your account has been deleted',
	'hj:approve:delete:email:head' => '%s has deleted your account',
	'hj:approve:delete:email:note' => 'The following note was included for your information:',
	'hj:approve:delete:email:footer' => 'You can contact the site administrator at %s',

	'hj:approve:content:disable:email:subject' => 'Your item has been disabled',
	'hj:approve:content:disable:email:head' => '%s has disabled your items %s',
	'hj:approve:content:disable:email:note' => 'The following note was included for your information:',
	'hj:approve:content:disable:email:footer' => 'You can contact site administrator at %s',

	'hj:approve:content:enable:email:subject' => 'Your item has been enabled',
	'hj:approve:content:enable:email:head' => '%s has enabled your item %s',
	'hj:approve:content:enable:email:note' => 'The following note was included for your information:',
	'hj:approve:content:enable:email:footer' => 'You can contact %s at %s',

	'hj:approve:content:delete:email:subject' => 'Your items has been deleted',
	'hj:approve:content:delete:email:head' => '%s has deleted your items',
	'hj:approve:content:delete:email:note' => 'The following note was included for your information:',
	'hj:approve:content:delete:email:footer' => 'You can contact the site administrator at %s',

	'hj:approve:supervisor_assigned_user:email:subject' => 'You have been assigned a user for supervision',
	'hj:approve:supervisor_assigned_user:email:head' => '%s has assigned %s to you for supervision',
	'hj:approve:supervisor_assigned_user:email:note' => 'The following note was included for your information:',
	'hj:approve:supervisor_assigned_user:email:footer' => 'You can view %s\'s profile here: %s',

	'hj:approve:supervisor_assigned:email:subject' => 'You have been assigned a supervisor',
	'hj:approve:supervisor_assigned:email:head' => '%s has assigned %s as a supervisor to your account',
	'hj:approve:supervisor_assigned:email:note' => 'The following note was included for your information:',
	'hj:approve:supervisor_assigned:email:footer' => 'You can contact %s at %s',

	'hj:approve:dashboard' => 'Content Approval Dashboard',
	'hj:approve:dashboard:empty' => 'There are no items on this dashboard',
	'hj:approve:dashboard:pending' => 'Pending Approval',
	'hj:approve:dashboard:rejected' => 'Rejected',
	'hj:approve:dashboard:approved' => 'Approved',
	'hj:approve:dashboard:spam' => 'Suspected Spam',
	'hj:approve:dashboard:ham' => 'Reviewed Spam (Ham)',

	'hj:approve:disabled' => 'Disabled',
	'hj:approve:enabled' => 'Enabled',
	'hj:approve:banned' => 'Banned',
	'hj:approve:notbanned' => 'Not Banned',
	'hj:approve:validated:not' => 'Not validated',
	'hj:approve:validated' => 'Validated',
	'hj:approve:failcount' => '%s spam flags',
	'hj:approve:last_ip' => 'Last login IP',
	'hj:approve:is_exempt' => 'Exempt from approval workflow',
	'hj:approve:is_whitelisted' => 'Whitelisted',
	'hj:approve:history' => 'History',

	'hj:approve:bulk:select' => 'Toggle all',
	'hj:approve:bulk:select:action' => 'Do nothing',
	'hj:approve:bulk:select:status' => 'Select new status...',
	'hj:approve:bulk:select:supervisor' => 'Supervisors',
	'hj:approve:bulk:selected:user' => 'With selected users:',
	'hj:approve:bulk:selected:content' => 'With selected content:',

	'hj:approve:bulk:user:exempt' => 'Exempt from approval workflow',
	'hj:approve:bulk:user:exempt_revoke' => 'Add to approval workflow',
	'hj:approve:bulk:user:whitelist' => 'Whitelist (exempt from spam checks)',
	'hj:approve:bulk:user:whitelist_revoke' => 'Remove from whitelist',
	'hj:approve:bulk:user:assign_supervisor' => 'Assign supervisor(s)',
	'hj:approve:bulk:user:validate' => 'Validate',
	'hj:approve:bulk:user:set_status' => 'Change status',
	'hj:approve:bulk:user:ban' => 'Ban user',
	'hj:approve:bulk:user:unban' => 'Unban user',
	'hj:approve:bulk:user:disable' => 'Disable user account',
	'hj:approve:bulk:user:enable' => 'Enable user account',
	'hj:approve:bulk:user:delete' => 'Delete user account and all user content',

	'hj:approve:bulk:content:set_status' => 'Change status',
	'hj:approve:bulk:content:disable' => 'Disable',
	'hj:approve:bulk:content:enable' => 'Enable',
	'hj:approve:bulk:content:delete' => 'Delete',

	'hj:approve:bulk:notify_users' => 'Notify users',
	'hj:approve:bulk:notify_owners' => 'Notify owners',
	'hj:approve:bulk:message' => 'Add a note to the notifications',
	'hj:approve:bulk:approval_message' => 'Add a note to approval workflow history',

	'hj:approve:access_id_private' => 'You can not change the access level of this item before it\'s reviewed and approved',

	'hj:approve::notitle' => 'untitled',

	'hj:approve:status' => 'Approval Status',
	'hj:approve:status:pending' => 'Pending',
	'hj:approve:status:approved' => 'Approved',
	'hj:approve:status:rejected' => 'Rejected',
	'hj:approve:status:flagged' => 'Needs Review',
	'hj:approve:status:exempt' => 'Exempt',

	'hj:approve:reason:spamcheck' => 'Spam Check',
	'hj:approve:reason:sitepolicy' => 'Approval Workflow',

	'hj:approve:trigger:userreport' => 'User Report',
	'hj:approve:trigger:system' => 'System Report',
	'hj:approve:trigger:akismet' => 'Spam Database',
	'hj:approve:trigger:admin' => 'Admin Review',

	'hj:approve:error:nouser' => 'Error: user not found - %s',
	'hj:approve:error:nouser' => 'Error: item not found - %s',
	'hj:approve:error:canedit' => 'Error: insufficient privileges - %s',
	'hj:approve:error:unknown' => 'Error: unknown - %s',
	
	'hj:approve:success:exempt' => '%s of %s users were exempted from the approval workflow',
	'hj:approve:success:exempt_revoke' => '%s of %s user were added to the approval workflow',
	'hj:approve:success:whitelist' => '%s of %s users were whitelisted',
	'hj:approve:success:whitelist_revoke' => '%s of %s users were removed from the white list',
	'hj:approve:success:validate' => '%s of %s user accounts were validated',
	'hj:approve:success:set_status' => 'Status was changed for %s of %s users',
	'hj:approve:success:ban' => '%s of %s users were banned',
	'hj:approve:success:unban' => '%s of %s were unbanned',
	'hj:approve:success:disable' => '%s of %s user accounts were disabled',
	'hj:approve:success:enable' => '%s of %s user accounts were enabled',
	'hj:approve:success:delete' => '%s of %s user accounts were deleted',
	'hj:approve:success:assign_supervisor' => '%s was assigned as supervisor to %s of %s users',

	'hj:approve:success:content:set_status' => 'Status was changed for %s of %s items',
	'hj:approve:success:content:disable' => '%s of %s items were disabled',
	'hj:approve:success:content:enable' => '%s of %s items were enabled',
	'hj:approve:success:content:delete' => '%s of %s items were deleted',

	'hj:approve:error:already_exempt' => '%s accounts were already exempt',
	'hj:approve:error:already_validated' => '%s accounts required no validation',
	'hj:approve:error:already_banned' => '%s users already banned',
	'hj:approve:error:notbanned' => '%s users are not banned',
	'hj:approve:error:already_disabled' => '%s users already disabled',
	'hj:approve:error:notdisabled' => '%s users not disabled',
	'hj:approve:error:already_supervisor' => '%s users were assigned previously',

	'hj:approve:error:content:already_disabled' => '%s items were already disabled',
	'hj:approve:error:content:notdisabled' => '%s items were not disabled',

	'hj:approve:system' => 'the system',
	
	'hj:approve:annotation:approval_status' => 'Status set to <b>%s</b> by %s <i>%s</i>',
	'hj:approve:annotation:approval_reason' => 'Review reason set to <b>%s</b> by %s <i>%s</i>',
	'hj:approve:annotation:approval_trigger' => 'Review stage set to <b>%s</b> by %s <i>%s</i>',
	'hj:approve:annotation:exempt' => 'Exempted from approval workflow by %s <i>%s</i>',
	'hj:approve:annotation:exempt_revoke' => 'Added to approval workflow by %s <i>%s</i>',
	'hj:approve:annotation:whitelist' => 'Whitelisted by %s <i>%s</i>',
	'hj:approve:annotation:whitelist_revoke' => 'Removed from whitelist by %s <i>%s</i>',
	'hj:approve:annotation:validate' => 'Validated by %s <i>%s</i>',
	'hj:approve:annotation:approval_message' => '%s added a note "%s" <i>%s</i>',
	'hj:approve:annotation:ban' => 'Banned by %s with a note "%s" <i>%s</i>',
	'hj:approve:annotation:unban' => 'Unbanned by %s <i>%s</i>',
	'hj:approve:annotation:disable' => 'Disabled by %s with a note "%s" <i>%s</i>',
	'hj:approve:annotation:enable' => 'Enabled by %s <i>%s</i>',
	'hj:approve:annotation:supervisor' => '%s assigned %s as supervisor <i>%s</i>',

	'hj:approve:report:spam' => 'Report this',
	'hj:approve:report:user' => 'Report user',
	'hj:approve:report:message' => 'Please add a message to the administrator (optional)',
	'hj:approve:report:spam:in_workflow' => 'Thank you for your report. This item has been reported previously, and we are currently reviewing it',
	'hj:approve:report:spam:success' => 'Thank you. Your report has been saved. We will review this item shortly',
	'hj:approve:report:spam:error' => 'Sorry, something went wrong',

	'hj:approve:make_editor' => 'Make editor',
	'hj:approve:revoke_editor' => 'Revoke editor privileges',
	'hj:approve:make_editor:success' => 'Editor privileges were assigned successfully',
	'hj:approve:make_editor:error' => 'Editor privileges could not be assigned',
	'hj:approve:revoke_editor:success' => 'Editor privileges were revoked successfully',
	'hj:approve:revoke_editor:error' => 'Editor privileges could not be revoked',

	'hj:approve:supervisor:manage' => 'Manage supervised users',
	'hj:approve:supervisor:revoke' => 'Revoke all supervisory privileges',
	'hj:approve:supervisor:revoke:success' => 'All supervisory privileges were revoked successfully',
	'hj:approve:supervisor:revoke:error' => 'Supervisory privileges could not be revoked',
	'hj:approve:supervisor:notfound' => 'Supervisor was not found',
	'hj:approve:supervisor:supervised_users' => 'Supervised users',
	'hj:approve:supervisor:supervised_users:add' => '%s users added to the supervised users list',
	'hj:approve:supervisor:supervised_users:delete' => '%s users were removed from the supervised users list',
	'hj:approve:supervisor:supervised_groups' => 'Supervised groups',
	'hj:approve:supervisor:supervised_groups:members' => 'Members of the supervised groups',
	'hj:approve:supervisor:supervised_groups:info' => 'By adding a group to the supervised groups list, you are actually adding all members of that group to the supervisor\'s list of supervised users',
	'hj:approve:supervisor:supervised_groups:add' => '%s groups added to the supervised groups list',
	'hj:approve:supervisor:supervised_groups:delete' => '%s groups were removed from the supervised groups list',
	'hj:approve:supervisor:supervised_users_groups:error' => '%s relationships could not be established. Possibly due to conflicts in the hierarchy of user roles',

	'hj:approve:topbar:approve' => 'Content Approval Dashboard',

	'hj:approve:review' => 'Review content',

	'hj:approve:notifications:intro' => 'You can specify, who should be sent a notification once one of the following events occur',
	'hj:approve:notifications:queue' => 'Content item is queued for approval',
	'hj:approve:notifications:user_queue' => 'User account is queued for approva',
	'hj:approve:notifications:user_report' => 'User reported content or user as spam',
	'hj:approve:notifications:akismet' => 'Akismet flagged a content item as spam',
	'hj:approve:notifications:workflow' => 'Approval status of an item or user has been changed',

	'hj:approve:notifications:notify:admin' => 'Notify administrators',
	'hj:approve:notifications:notify:editor' => 'Notify editors',
	'hj:approve:notifications:notify:supervisor' => 'Notify supervisors',
	
	'hj:approve:notifications:include_export' => 'Include entity export into notifications sent to editors/supervisors',

	'hj:approve:dashboard:search' => 'Search',
	'hj:approve:search' => 'Search Dashboard',
	'hj:approve:search:types' => 'Find',
	'hj:approve:search:keyword' => 'by keyword',
	'hj:approve:search:status' => 'by status',
	'hj:approve:search:reason' => 'by reason',
	'hj:approve:search:trigger' => 'by stage',
	'hj:approve:search:enabled' => 'by enabled flag',
	'hj:approve:search:banned' => 'by banned flag',
	'hj:approve:search:validated' => 'by validation',
	'hj:approve:search:subtypes' => 'by content type',
	
	'hj:approve:content' => 'Content items',

	'hj:approve:admin_notice:supervisor_relationship' => 'Please remember that users in admin, editor and supervisor roles can not be added to the other user\'s supervised users list.',
	'hj:approve:admin_notice:supervisor_relationship_reverse' => 'Please remember that users in admin and editor roles can not be supervisors',

	'hj:approve:under_review' => 'This item is currently being reviewed by the administrator and is not available',

	'hj:approve:policy:annotation:generic_comment' => 'Comment',
	'hj:approve:policy:annotation:messageboard' => 'Message board post',
	'hj:approve:policy:annotation:group_topic_post' => 'Group topic post',
		);

add_translation("en", $english);
?>
