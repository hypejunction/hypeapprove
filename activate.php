<?php

if (is_null(elgg_get_plugin_setting('queue_notify', 'hypeApprove'))) {
	elgg_set_plugin_setting('queue_notify', true, 'hypeApprove');
}

if (is_null(elgg_get_plugin_setting('spam_reporting', 'hypeApprove'))) {
	elgg_set_plugin_setting('spam_reporting', true, 'hypeApprove');
}

if (is_null(elgg_get_plugin_setting('akismet_key', 'hypeApprove'))) {
	elgg_set_plugin_setting('akismet_key', false, 'hypeApprove');
}

if (is_null(elgg_get_plugin_setting('akismet_notify', 'hypeApprove'))) {
	elgg_set_plugin_setting('akismet_notify', true, 'hypeApprove');
}

if (is_null(elgg_get_plugin_setting('akismet_fail_count', 'hypeApprove'))) {
	elgg_set_plugin_setting('akismet_fail_count', 100, 'hypeApprove');
}

if (is_null(elgg_get_plugin_setting('akismet_fail_count_action', 'hypeApprove'))) {
	elgg_set_plugin_setting('akismet_fail_count_action', 'ban', 'hypeApprove');
}

if (is_null(elgg_get_plugin_setting('stopforumspam_key', 'hypeApprove'))) {
	elgg_set_plugin_setting('stopforumspam_key', false, 'hypeApprove');
}